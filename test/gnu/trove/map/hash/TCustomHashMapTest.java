package gnu.trove.map.hash;

import gnu.trove.impl.hash.DHashObjObjMap;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

import java.util.*;


/**
 *
 */
public class TCustomHashMapTest extends TestCase {


    static class CharArrayHashMap<V> extends DHashObjObjMap<char[], V> {

        public CharArrayHashMap() {}

        protected int keyHashCode( @NotNull char[] obj ) {
            return Arrays.hashCode( obj );
        }

        protected boolean keysEquals( @NotNull char[] o1, char[] o2 ) {
            return Arrays.equals( o1, o2 );
        }
    }

    // Example from Trove overview doc
    public void testArray() {
        char[] foo = new char[]{ 'a', 'b', 'c' };
        char[] bar = new char[]{ 'a', 'b', 'c' };

        Map<char[], String> map = new CharArrayHashMap<String>();
        map.put( foo, "yay" );
        assertTrue( map.containsKey( foo ) );
        assertTrue( map.containsKey( bar ) );
        assertEquals( "yay", map.get( foo ) );
        assertEquals( "yay", map.get( bar ) );

        Set<char[]> keys = map.keySet();
        assertTrue( keys.contains( foo ) );
        assertTrue( keys.contains( bar ) );
    }


    static class ByteArrayHashMap<V> extends DHashObjObjMap<byte[], V> {

        public ByteArrayHashMap() {}

        // Copied from Arrays.hashCode, but applied only to the first four bytes
        public int keyHashCode( @NotNull byte[] obj ) {
            int h = 1;
            for ( int i = 0; i < 4; i++ ) h = 31 * h + obj[ i ];
            return h;
        }

        public boolean keysEquals( @NotNull byte[] o1, byte[] o2 ) {
            return Arrays.equals( o1, o2 );
        }
    }

    private static byte[] random( int n, Random rnd ) {
        byte[] ba = new byte[ n ];
        for ( int i = 0; i < ba.length; i++ ) {
            ba[ i ] = ( byte ) rnd.nextInt();
        }
        return ba;
    }

    public void testBug4706479() throws Exception {
        Random rnd = new Random( 1234 );
        DHashObjObjMap<byte[], Integer> map = new ByteArrayHashMap<Integer>();
        List<byte[]> list = new ArrayList<byte[]>();
        List<Integer> expected = new ArrayList<Integer>();

        for ( int i = 0; i < 1000; i++ ) {
            byte[] ba = random( 16, rnd );
            list.add( ba );

            Integer obj = i;
            expected.add( obj );
            map.put( ba, obj );
        }

        assertEquals( list.size(), map.size() );

        // Make sure all the arrays are found in the map
        for( byte[] array : map.keySet() ) {
            boolean found_it = false;
            for( byte[] test : list ) {
                if ( Arrays.equals( test, array ) ) {
                    found_it = true;
                    break;
                }
            }

            assertTrue( "Unable to find: " + Arrays.toString( array ), found_it );
        }
        // Make sure all the Integers are found in the map
        for( Integer obj : map.values() ) {
            assertTrue( "Unable to find: " + obj, expected.contains( obj ) );
        }

        for ( int i = 0; i < expected.size(); i++ ) {
            assertEquals( expected.get( i ), map.get( list.get( i ) ) );
        }

        // Remove items
        for ( int i = 0; i < list.size(); i++ ) {
            assertEquals( expected.get( i ), map.remove( list.get( i ) ) );
        }

        assertEquals( 0, map.size() );
        assertTrue( map.isEmpty() );

        for ( byte[] aList : list ) {
            assertNull( map.get( aList ) );
        }
    }


}
