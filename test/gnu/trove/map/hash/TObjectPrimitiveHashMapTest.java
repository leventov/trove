///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2006, Eric D. Friedman All Rights Reserved.
// Copyright (c) 2009, Rob Eden All Rights Reserved.
// Copyright (c) 2009, Jeff Randall All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////

package gnu.trove.map.hash;


import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.map.*;
import gnu.trove.impl.hash.*;
import junit.framework.TestCase;

import java.util.*;



/**
 *
 */
public class TObjectPrimitiveHashMapTest extends TestCase {
    
    public static List<Integer> asList( int[] vals ) {
        List<Integer> list = new ArrayList<Integer>();
        for ( int v : vals ) list.add( v );
        return list;
    }

    public static List<Long> asList( long[] vals ) {
        List<Long> list = new ArrayList<Long>();
        for ( long v : vals ) list.add( v );
        return list;
    }

    public TObjectPrimitiveHashMapTest( String name ) {
        super( name );
    }


    public void testConstructors() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }

        HashObjIntMap<String> capacity =
                new DHashObjIntMap<String>( 20 );
        for ( int i = 0; i < element_count; i++ ) {
            capacity.put( keys[i], vals[i] );
        }
        assertEquals( map, capacity );

        HashObjIntMap<String> cap_and_factor =
                new DHashObjIntMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            cap_and_factor.put( keys[i], vals[i] );
        }
        assertEquals( map, cap_and_factor );

        HashObjIntMap<String> fully_specified =
                new DHashObjIntMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            fully_specified.put( keys[i], vals[i] );
        }
        assertEquals( map, fully_specified );

        HashObjIntMap<String> copy =
                new DHashObjIntMap<String>( fully_specified );
        assertEquals( map, copy );
    }


    public void testContainsKey() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( "Key should be present: " + keys[i] + ", map: " + map,
                    map.containsKey( keys[i] ) );
        }

        String key = "1138";
        assertFalse( "Key should not be present: " + key + ", map: " + map,
                map.containsKey( key ) );
    }


    public void testContainsValue() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( "Value should be present: " + vals[i] + ", map: " + map,
                    map.containsValue( vals[i] ) );
        }

        int val = 1138;
        assertFalse( "Key should not be present: " + val + ", map: " + map,
                map.containsValue( val ) );
    }


    public void testPutIfAbsent() {
        HashObjIntMap<String> map = new DHashObjIntMap<String>();

        map.put( "One", 1 );
        map.put( "Two", 2 );
        map.put( "Three", 3 );

        assertEquals( 1, map.putIfAbsent( "One", 2 ) );
        assertEquals( 1, map.getInt( "One" ) );
        assertEquals( 0, map.putIfAbsent( "Nine", 9 ) );
        assertEquals( 9, map.getInt( "Nine" ) );
    }


    public void testRemove() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            if ( i % 2 == 1 ) {
                assertEquals( "Remove should have modified map: " + keys[i] + ", map: " + map,
                        vals[i], map.removeAsInt( keys[i] ) );
            }
        }

        for ( int i = 0; i < element_count; i++ ) {
            if ( i % 2 == 1 ) {
                assertTrue( "Removed key still in map: " + keys[i] + ", map: " + map,
                        map.getInt( keys[i] ) == map.getNoEntryValue() );
            } else {
                assertTrue( "Key should still be in map: " + keys[i] + ", map: " + map,
                        map.getInt( keys[i] ) == vals[i] );
            }
        }
    }


    public void testPutAllMap() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> control = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            control.put( keys[i], vals[i] );
        }

        ObjIntMap<String> map = new DHashObjIntMap<String>();

        Map<String, Integer> source = new HashMap<String, Integer>();
        for ( int i = 0; i < element_count; i++ ) {
            source.put( keys[i], vals[i] );
        }

        map.putAll( source );
        assertEquals( control, map );
    }


    public void testPutAll() throws Exception {
        HashObjIntMap<String> t = new DHashObjIntMap<String>();
        HashObjIntMap<String> m = new DHashObjIntMap<String>();
        m.put( "one", 2 );
        m.put( "two", 4 );
        m.put( "three", 6 );

        t.put( "four", 5 );
        assertEquals( 1, t.size() );

        t.putAll( m );
        assertEquals( 4, t.size() );
        assertEquals( 4, t.getInt( "two" ) );
    }


    public void testClear() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        map.clear();
        assertTrue( map.isEmpty() );
        assertEquals( 0, map.size() );

        assertEquals( map.getNoEntryValue(), map.getInt( keys[5] ) );
    }


    @SuppressWarnings({"ToArrayCallWithZeroLengthArrayArgument"})
    public void testKeySet() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Set<String> keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        Object[] keys_obj_array = keyset.toArray();
        int count = 0;
        Iterator<String> iter = keyset.iterator();
        while ( iter.hasNext() ) {
            String key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_obj_array[count], key );
            count++;
        }

        String[] keys_array = keyset.toArray( new String[0] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            String key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }

        keys_array = keyset.toArray( new String[keyset.size()] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            String key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }

        keys_array = keyset.toArray( new String[keyset.size() * 2] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            String key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }
        assertNull( keys_array[keyset.size()] );

        Set<String> other = new HashSet<String>( keyset );
        assertFalse( keyset.retainAll( other ) );
        other.remove( keys[5] );
        assertTrue( keyset.retainAll( other ) );
        assertFalse( keyset.contains( keys[5] ) );
        assertFalse( map.containsKey( keys[5] ) );

        keyset.clear();
        assertTrue( keyset.isEmpty() );


    }


    public void testKeySetAdds() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Set<String> keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        try {
            keyset.add( "explosions!" );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            Set<String> test = new HashSet<String>();
            test.add( "explosions!" );
            keyset.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }
    }


    public void testKeys() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        // No argument
        Object[] keys_object_array = map.keySet().toArray();
        assertEquals( element_count, keys_object_array.length );
        List<Object> keys_object_list = Arrays.asList( keys_object_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_object_list.contains( keys[i] ) );
        }

        // Zero length array
        String[] keys_string_array = map.keySet().toArray( new String[ 0 ] );
        assertEquals( element_count, keys_string_array.length );
        List<String> keys_string_list = Arrays.asList( keys_string_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_string_list.contains( keys[i] ) );
        }

        // appropriate length array
        keys_string_array = map.keySet().toArray( new String[map.size()] );
        assertEquals( element_count, keys_string_array.length );
        keys_string_list = Arrays.asList( keys_string_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_string_list.contains( keys[i] ) );
        }

        // longer array
        keys_string_array = map.keySet().toArray( new String[element_count * 2] );
        assertEquals( element_count * 2, keys_string_array.length );
        keys_string_list = Arrays.asList( keys_string_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_string_list.contains( keys[i] ) );
        }
        assertNull( keys_string_array[element_count] );
    }


    public void testValueCollectionToArray() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        int[] values_array = collection.toIntArray();
        int count = 0;
        IntIterator iter = collection.iterator();
        while ( iter.hasNext() ) {
            int value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new int[0] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            int value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new int[collection.size()] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            int value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new int[collection.size() * 2] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            int value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }
        assertEquals( collection.getNoEntryValue(), values_array[collection.size()] );
        assertEquals( map.getNoEntryValue(), values_array[collection.size()] );

        List<Integer> other = new ArrayList<Integer>( collection );
        assertFalse( collection.retainAll( other ) );
        other.remove( Integer.valueOf(vals[5]) );
        assertTrue( collection.retainAll( other ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );

        collection.clear();
        assertTrue( collection.isEmpty() );
    }


    public void testValueCollectionAdds() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        try {
            collection.add( 1138 );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            Set<Integer> test = new HashSet<Integer>();
            test.add( Integer.valueOf( 1138 ) );
            collection.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            List<Integer> test = new ArrayList<Integer>();
            test.add( 1138 );
            collection.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            collection.addAll( asList( vals ) );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }
    }


    public void testValueCollectionContainsAll() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<Integer> java_list = new ArrayList<Integer>();
        for ( int value : vals ) {
            java_list.add( value );
        }
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.containsAll( java_list ) );
        java_list.add( Integer.valueOf( 1138 ) );
        assertFalse( "collection: " + collection + ", should not contain all in list: " +
                     java_list, collection.containsAll( java_list ) );

        // ClassCastException is OK
//        List<Number> number_list = new ArrayList<Number>();
//        for ( int value : vals ) {
//            if ( value == 5 ) {
//                number_list.add( Long.valueOf( value ) );
//            } else {
//                number_list.add( Integer.valueOf( value ) );
//            }
//        }
//        assertFalse( "collection: " + collection + ", should not contain all in list: " +
//                     java_list, collection.containsAll( number_list ) );

        List<Integer> other = new ArrayList<Integer>( collection );
        assertTrue( "collection: " + collection + ", should contain all in other: " +
                    other, collection.containsAll( other ) );
        other.add( 1138 );
        assertFalse( "collection: " + collection + ", should not contain all in other: " +
                     other, collection.containsAll( other ) );

        assertTrue( "collection: " + collection + ", should contain all in array: " +
                    Arrays.toString( vals ), collection.containsAll( asList( vals ) ) );
        int[] other_array = new int[vals.length + 1];
        other_array[other_array.length - 1] = 1138;
        assertFalse( "collection: " + collection + ", should not contain all in array: " +
                     Arrays.toString( vals ), collection.containsAll( asList( other_array ) ) );
    }


    public void testValueCollectionRetainAllCollection() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<Integer> java_list = new ArrayList<Integer>();
        for ( int value : vals ) {
            java_list.add( value );
        }
        assertFalse( "collection: " + collection + ", should contain all in list: " +
                     java_list, collection.retainAll( java_list ) );

        java_list.remove( 5 );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.retainAll( java_list ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.containsAll( java_list ) );
    }


    public void testValueCollectionRetainAllTCollection() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        try {
            assertFalse( "collection: " + collection + ", should be unmodified.",
                    collection.retainAll( collection ) );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

        List<Integer> other = new ArrayList<Integer>( collection );
        assertFalse( "collection: " + collection + ", should be unmodified. other: " +
                     other, collection.retainAll( other ) );

        other.remove( Integer.valueOf(vals[5]) );
        assertTrue( "collection: " + collection + ", should be modified. other: " +
                    other, collection.retainAll( other ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );
        assertTrue( "collection: " + collection + ", should contain all in other: " +
                    other, collection.containsAll( other ) );
    }


    public void testValueCollectionRetainAllArray() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        assertFalse( "collection: " + collection + ", should be unmodified. array: " +
                     Arrays.toString( vals ), collection.retainAll( asList( vals ) ) );

        int[] other = new int[element_count - 1];
        for ( int i = 0; i < element_count; i++ ) {
            if ( i < 5 ) {
                other[i] = i + 1;
            }
            if ( i > 5 ) {
                other[i - 1] = i + 1;
            }
        }
        assertTrue( "collection: " + collection + ", should be modified. array: " +
                    Arrays.toString( other ), collection.retainAll( asList( other ) ) );
    }


    public void testValueCollectionRemoveAllCollection() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<Integer> java_list = new ArrayList<Integer>();
        assertFalse( "collection: " + collection + ", should contain all in list: " +
                     java_list, collection.removeAll( java_list ) );

        java_list.add( vals[5] );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.removeAll( java_list ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );

        java_list = new ArrayList<Integer>();
        for ( int value : vals ) {
            java_list.add( value );
        }
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.removeAll( java_list ) );
        assertTrue( collection.isEmpty() );
    }


    public void testValueCollectionRemoveAllTCollection() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<Integer> other = new ArrayList<Integer>();
        assertFalse( "collection: " + collection + ", should be unmodified.",
                collection.removeAll( other ) );

        other = new ArrayList<Integer>( collection );
        other.remove( Integer.valueOf(vals[5]) );
        assertTrue( "collection: " + collection + ", should be modified. other: " +
                    other, collection.removeAll( other ) );
        assertEquals( 1, collection.size() );
        for ( int i = 0; i < element_count; i++ ) {
            if ( i == 5 ) {
                assertTrue( collection.contains( vals[i] ) );
                assertTrue( map.containsKey( keys[i] ) );
                assertTrue( map.containsValue( vals[i] ) );
            } else {
                assertFalse( collection.contains( vals[i] ) );
                assertFalse( map.containsKey( keys[i] ) );
                assertFalse( map.containsValue( vals[i] ) );
            }
        }

        assertFalse( "collection: " + collection + ", should be unmodified. other: " +
                     other, collection.removeAll( other ) );
        try {
            assertTrue( "collection: " + collection + ", should be modified. other: " +
                        other, collection.removeAll( collection ) );
            assertTrue( collection.isEmpty() );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

    }


    public void testValueCollectionRemoveAllArray() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map = new DHashObjIntMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        int[] other = {1138};
        assertFalse( "collection: " + collection + ", should be unmodified. array: " +
                     Arrays.toString( vals ), collection.removeAll( asList( other ) ) );

        other = new int[element_count - 1];
        for ( int i = 0; i < element_count; i++ ) {
            if ( i < 5 ) {
                other[i] = i + 1;
            }
            if ( i > 5 ) {
                other[i - 1] = i + 1;
            }
        }
        assertTrue( "collection: " + collection + ", should be modified. array: " +
                    Arrays.toString( other ), collection.removeAll( asList( other ) ) );
        assertEquals( 1, collection.size() );
        for ( int i = 0; i < element_count; i++ ) {
            if ( i == 5 ) {
                assertTrue( collection.contains( vals[i] ) );
                assertTrue( map.containsKey( keys[i] ) );
                assertTrue( map.containsValue( vals[i] ) );
            } else {
                assertFalse( collection.contains( vals[i] ) );
                assertFalse( map.containsKey( keys[i] ) );
                assertFalse( map.containsValue( vals[i] ) );
            }
        }
    }


    public void testValues() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        // No argument
        int[] values_array = map.values().toIntArray();
        assertEquals( element_count, values_array.length );
        List<Integer> values_list = asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }

        // Zero length array
        values_array = map.values().toArray( new int[ 0 ] );
        assertEquals( element_count, values_array.length );
        values_list = asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }

        // appropriate length array
        values_array = map.values().toArray( new int[ map.size() ] );
        assertEquals( element_count, values_array.length );
        values_list = asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }

        // longer array
        values_array = map.values().toArray( new int[ element_count * 2 ] );
        assertEquals( element_count * 2, values_array.length );
        values_list = asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }
        assertEquals( map.getNoEntryValue(), values_array[element_count] );
    }


    public void testIterator() {
        HashObjIntMap<String> map = new DHashObjIntMap<String>();

        IntValueMapIterator<String> iterator = map.mapIterator();
        assertFalse( iterator.hasNext() );

        map.put( "one", 1 );
        map.put( "two", 2 );

        iterator = map.mapIterator();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();

        String first_key = iterator.key();
        assertNotNull( "key was null", first_key );
        assertTrue( "invalid key: " + first_key,
                first_key.equals( "one" ) || first_key.equals( "two" ) );
        if ( first_key.equals( "one" ) ) {
            assertEquals( 1, iterator.intValue() );
        } else {
            assertEquals( 2, iterator.intValue() );
        }

        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        String second_key = iterator.key();
        assertNotNull( "key was null", second_key );
        assertTrue( "invalid key: " + second_key,
                second_key.equals( "one" ) || second_key.equals( "two" ) );
        if ( second_key.equals( "one" ) ) {
            assertEquals( 1, iterator.intValue() );
        } else {
            assertEquals( 2, iterator.intValue() );
        }
        assertFalse( first_key + ", " + second_key, first_key.equals( second_key ) );

        assertFalse( iterator.hasNext() );

        // New Iterator
        iterator = map.mapIterator();
        iterator.tryAdvance();
        first_key = iterator.key();
        iterator.setValue( 1138 );
        assertEquals( 1138, iterator.intValue() );
        assertEquals( 1138, map.getInt( first_key ) );
    }


    public void testIteratorRemoval() {
        HashObjIntMap<String> map = new DHashObjIntMap<String>();

        map.put( "one", 1 );
        map.put( "two", 2 );
        map.put( "three", 3 );
        map.put( "four", 4 );
        map.put( "five", 5 );
        map.put( "six", 6 );
        map.put( "seven", 7 );
        map.put( "eight", 8 );
        map.put( "nine", 9 );
        map.put( "ten", 10 );

        IntValueMapIterator<String> iterator = map.mapIterator();
        while ( map.size() > 5 && iterator.hasNext() ) {
            iterator.tryAdvance();
            String key = iterator.key();
            iterator.remove();
            assertFalse( "map still contains key: " + key, map.containsKey( key ) );
        }

        assertEquals( 5, map.size() );
        iterator = map.mapIterator();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertFalse( iterator.hasNext() );

        iterator = map.mapIterator();
        int elements = map.size();
        while ( iterator.hasNext() ) {
            iterator.tryAdvance();
            String key = iterator.key();
            iterator.remove();
            assertFalse( "map still contains key: " + key, map.containsKey( key ) );
            elements--;
        }
        assertEquals( 0, elements );

        assertEquals( 0, map.size() );
        assertTrue( map.isEmpty() );
    }


    public void testIteratorRemoval2() {
        int element_count = 10000;
        int remaining = element_count / 2;

        HashObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );

        for ( int pass = 0; pass < 10; pass++ ) {
            Random r = new Random();
            for ( int i = 0; i <= element_count; i++ ) {
                map.put( String.valueOf( r.nextInt() ), i );
            }

            IntValueMapIterator iterator = map.mapIterator();
            while ( map.size() > remaining && iterator.hasNext() ) {
                iterator.tryAdvance();
                iterator.remove();
            }
        }
    }


    public void testAdjustOrPutValue() {
        HashObjIntMap<String> map = new DHashObjIntMap<String>();

        map.put( "one", 1 );

        long new_value = map.adjustOrPutValue( "one", 1, 100 );
        assertEquals( 2, new_value );
        assertEquals( 2, map.getInt( "one" ) );

        new_value = map.adjustOrPutValue( "one", 5, 100 );
        assertEquals( 7, new_value );
        assertEquals( 7, map.getInt( "one" ) );

        new_value = map.adjustOrPutValue( "one", -3, 100 );
        assertEquals( 4, new_value );
        assertEquals( 4, map.getInt( "one" ) );

        new_value = map.adjustOrPutValue( "two", 1, 100 );
        assertEquals( 100, new_value );
        assertTrue( map.containsKey( "two" ) );
        assertEquals( 100, map.getInt( "two" ) );

        new_value = map.adjustOrPutValue( "two", 1, 100 );
        assertEquals( 101, new_value );
        assertEquals( 101, map.getInt( "two" ) );
    }


    public void testForEachKey() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements Predicate<String> {

            List<String> built = new ArrayList<String>();


            public boolean test( String value ) {
                built.add( value );
                return true;
            }


            List<String> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.keySet().testWhile( foreach );
        List<String> built = foreach.getBuilt();
        List<String> keys_list = Arrays.asList( map.keySet().toArray( new String[map.size()] ) );
        assertEquals( keys_list, built );

        Collections.sort( built );
        Collections.sort( keys_list );
        assertEquals( keys_list, built );


        class ForEachFalse implements Predicate<String> {

            List<String> built = new ArrayList<String>();


            public boolean test( String value ) {
                built.add( value );
                return false;
            }


            List<String> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.keySet().testWhile( foreach_false );
        built = foreach_false.getBuilt();
        keys_list = Arrays.asList( map.keySet().toArray( new String[ map.size() ] ) );
        assertEquals( 1, built.size() );
        assertEquals( keys_list.get( 0 ), built.get( 0 ) );
    }


    public void testForEachValue() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements IntConsumer {

            List<Integer> built = new ArrayList<Integer>();


            public void accept( int value ) {
                built.add( value );
            }


            List<Integer> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.values().forEach( foreach );
        List<Integer> built = foreach.getBuilt();
        List<Integer> values = new ArrayList<Integer>( map.values() );
        assertEquals( values, built );


        class ForEachFalse implements IntPredicate {

            List<Integer> built = new ArrayList<Integer>();


            public boolean test( int value ) {
                built.add( value );
                return false;
            }


            List<Integer> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.values().testWhile( foreach_false );
        built = foreach_false.getBuilt();
        values = new ArrayList<Integer>( map.values() );
        assertEquals( 1, built.size() );
        assertEquals( values.get( 0 ), built.get( 0 ) );
    }


    public void testForEachEntry() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements ObjIntConsumer<String> {

            ObjIntMap<String> built = new DHashObjIntMap<String>();


            public void accept( String key, int value ) {
                built.put( key, value );
            }


            ObjIntMap<String> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.forEach( foreach );
        ObjIntMap<String> built = foreach.getBuilt();
        assertEquals( map, built );


        class ForEachFalse implements ObjIntPredicate<String> {

            ObjIntMap<String> built = new DHashObjIntMap<String>();


            public boolean test( String key, int value ) {
                built.put( key, value );
                return false;
            }


            ObjIntMap<String> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.testWhile( foreach_false );
        built = foreach_false.getBuilt();
        assertEquals( 1, built.size() );
        assertTrue( map.containsKey( built.keySet().toArray()[0] ) );
        assertTrue( map.containsValue( built.values().toIntArray()[0] ) );
    }


    public void testRetain() {
        HashObjIntMap<String> map = new DHashObjIntMap<String>();

        map.put( "one", 1 );
        map.put( "two", 2 );
        map.put( "three", 3 );
        map.put( "four", 4 );
        map.put( "five", 5 );
        map.put( "six", 6 );
        map.put( "seven", 7 );
        map.put( "eight", 8 );
        map.put( "nine", 9 );
        map.put( "ten", 10 );

        map.removeIf( new ObjIntPredicate<String>() {
            public boolean test( String a, int b ) {
                return !( b > 5 && b <= 8 );

            }
        } );

        assertEquals( 3, map.size() );
        assertFalse( map.containsKey( "one" ) );
        assertFalse( map.containsKey( "two" ) );
        assertFalse( map.containsKey( "three" ) );
        assertFalse( map.containsKey( "four" ) );
        assertFalse( map.containsKey( "five" ) );
        assertTrue( map.containsKey( "six" ) );
        assertTrue( map.containsKey( "seven" ) );
        assertTrue( map.containsKey( "eight" ) );
        assertFalse( map.containsKey( "nine" ) );
        assertFalse( map.containsKey( "ten" ) );

        map.put( "eleven", 11 );
        map.put( "twelve", 12 );
        map.put( "thirteen", 13 );
        map.put( "fourteen", 14 );
        map.put( "fifteen", 15 );
        map.put( "sixteen", 16 );
        map.put( "seventeen", 17 );
        map.put( "eighteen", 18 );
        map.put( "nineteen", 19 );
        map.put( "twenty", 20 );


        map.removeIf( new ObjIntPredicate<String>() {
            public boolean test( String a, int b ) {
                return b <= 15;
            }
        } );

        assertEquals( 5, map.size() );
        assertFalse( map.containsKey( "one" ) );
        assertFalse( map.containsKey( "two" ) );
        assertFalse( map.containsKey( "three" ) );
        assertFalse( map.containsKey( "four" ) );
        assertFalse( map.containsKey( "five" ) );
        assertFalse( map.containsKey( "six" ) );
        assertFalse( map.containsKey( "seven" ) );
        assertFalse( map.containsKey( "eight" ) );
        assertFalse( map.containsKey( "nine" ) );
        assertFalse( map.containsKey( "ten" ) );
        assertFalse( map.containsKey( "eleven" ) );
        assertFalse( map.containsKey( "twelve" ) );
        assertFalse( map.containsKey( "thirteen" ) );
        assertFalse( map.containsKey( "fourteen" ) );
        assertFalse( map.containsKey( "fifteen" ) );
        assertTrue( map.containsKey( "sixteen" ) );
        assertTrue( map.containsKey( "seventeen" ) );
        assertTrue( map.containsKey( "eighteen" ) );
        assertTrue( map.containsKey( "nineteen" ) );
        assertTrue( map.containsKey( "twenty" ) );


        map.removeIf( new ObjIntPredicate<String>() {
            public boolean test( String a, int b ) {
                return true;
            }
        } );

        assertEquals( 0, map.size() );
    }


    public void testTransformValues() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        map.replaceAll( new ObjIntToIntFunction<String>() {
            public int applyAsInt( String a, int b ) {
                return b * b;
            }
        });

        for ( int i = 0; i < element_count; i++ ) {
            int expected = vals[i] * vals[i];
            assertEquals( expected, map.getInt( keys[i] ) );
        }
    }


    public void testEquals() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = Integer.toString( i + 1 );
            vals[i] = i + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        HashObjIntMap<String> fully_specified =
                new DHashObjIntMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            fully_specified.put( keys[i], vals[i] );
        }
        assertEquals( map, fully_specified );

        assertFalse( "shouldn't equal random object", map.equals( new Object() ) );

        int value = 11010110;     // I thought I saw a two!
        assertEquals( map.getNoEntryValue(), map.put( null, value ) );
        assertEquals( map.getNoEntryValue(), fully_specified.put( null, value ) );
        assertEquals( value, map.getInt( null ) );
        assertEquals( value, fully_specified.getInt( null ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );

        assertEquals( map.getNoEntryValue(), fully_specified.put( "non-null-key", value ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );

        int no_value = map.getNoEntryValue();
        assertEquals( map.getNoEntryValue(), map.put( "non-null-key", no_value ) );
        assertEquals( value, fully_specified.put( "non-null-key", no_value ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );
        
        assertEquals( no_value, map.put( "non-null-key", value ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );


        assertEquals( map.getNoEntryValue(), fully_specified.put( "blargh", value ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
    }


    public void testHashCode() {
        int element_count = 20;
        String[] keys = new String[element_count];
        int[] vals = new int[element_count];
        int counter = 0;

        ObjIntMap<String> map =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = Integer.toString( counter + 1 );
            vals[i] = counter + 1;
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );
        assertEquals( map.hashCode(), map.hashCode() );

        Map<String, ObjIntMap<String>> string_tmap_map =
                new HashMap<String, ObjIntMap<String>>();
        string_tmap_map.put( "first", map );
        string_tmap_map.put( "second", map );
        assertSame( map, string_tmap_map.get( "first" ) );
        assertSame( map, string_tmap_map.get( "second" ) );

        ObjIntMap<String> map2 =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = Integer.toString( counter + 1 );
            vals[i] = counter + 1;
            map2.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map2.size() );
        assertEquals( map2.hashCode(), map2.hashCode() );

        ObjIntMap<String> map3 =
                new DHashObjIntMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = Integer.toString( counter + 1 );
            vals[i] = counter + 1;
            map3.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map3.size() );
        assertEquals( map3.hashCode(), map3.hashCode() );

        assertFalse( map.hashCode() == map2.hashCode() );
        assertFalse( map.hashCode() == map3.hashCode() );
        assertFalse( map2.hashCode() == map3.hashCode() );

        Map<ObjIntMap<String>, String> tmap_string_map =
                new HashMap<ObjIntMap<String>, String>();
        tmap_string_map.put( map, "map1" );
        tmap_string_map.put( map2, "map2" );
        tmap_string_map.put( map3, "map3" );
        assertEquals( "map1", tmap_string_map.get( map ) );
        assertEquals( "map2", tmap_string_map.get( map2 ) );
        assertEquals( "map3", tmap_string_map.get( map3 ) );
    }

    public void testToString() {
        HashObjIntMap<String> m = new DHashObjIntMap<String>();
        m.put( "One", 11 );
        m.put( "Two", 22 );

        String to_string = m.toString();
        assertTrue( to_string, to_string.equals( "{One=11, Two=22}" )
                               || to_string.equals( "{Two=22, One=11}" ) );
    }

    // What?
//    public void testBug3232758() {
//        HashObjIntMap<String> map = new DHashObjIntMap<String>( 1, 3 );
//        map.put( "1009", 0 );
//        map.put( "1007", 1 );
//        map.put( "1006", 2 );
//        map.put( "1005", 3 );
//        map.put( "1004", 4 );
//        map.put( "1002", 5 );
//        map.put( "1001", 6 );
//
//        for ( Object o : map.keySet() ) {
//            map.remove( o );
//        }
//    }
}
