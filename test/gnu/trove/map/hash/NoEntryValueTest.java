package gnu.trove.map.hash;

import gnu.trove.function.ObjIntToIntFunction;
import gnu.trove.impl.hash.DHashObjIntMap;
import gnu.trove.map.ObjIntMap;
import junit.framework.TestCase;

import java.util.Arrays;


/**
 * Test for a number of bugs related to noEntryValue, including 3432402
 */
public class NoEntryValueTest extends TestCase {
    public void testAdjustToNoEntry() {
        ObjIntMap<String> map = new DHashObjIntMap<String>();
        
        assertEquals( 0, map.getNoEntryValue() );
        assertEquals( 0, map.getInt( "NotInThere" ) );
        
        map.put( "Value", 1 );
        assertEquals( 1, map.size() );
        assertEquals( 1, map.getInt( "Value" ) );
        assertTrue( map.containsKey( "Value" ) );
        assertTrue( map.containsValue( 1 ) );
        assertTrue( Arrays.equals( new int[] { 1 }, map.values().toIntArray() ) );
        
        map.computeIfPresent( "Value", new ObjIntToIntFunction<String>() {
            public int applyAsInt( String a, int b ) {
                return b - 1;
            }
        } );
        assertEquals( 1, map.size() );
        assertEquals( 0, map.getInt( "Value" ) );
        assertTrue( map.containsKey( "Value" ) );
        assertTrue( map.containsValue( 0 ) );
        assertTrue( Arrays.equals( new int[] { 0 }, map.values().toIntArray() ) );
    }
}
