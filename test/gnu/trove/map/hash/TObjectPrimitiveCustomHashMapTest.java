package gnu.trove.map.hash;

import gnu.trove.impl.hash.DHashObjIntMap;
import gnu.trove.map.ObjIntMap;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Set;


/**
 *
 */
public class TObjectPrimitiveCustomHashMapTest extends TestCase {

    static class CharArrayIntHashMap extends DHashObjIntMap<char[]> {

        public CharArrayIntHashMap() {}

        protected int keyHashCode( @NotNull char[] obj ) {
            return Arrays.hashCode( obj );
        }

        protected boolean keysEquals( @NotNull char[] o1, char[] o2 ) {
            return Arrays.equals( o1, o2 );
        }
    }

    // Example from Trove overview doc
    public void testArray() {
        char[] foo = new char[] { 'a', 'b', 'c' };
        char[] bar = new char[] { 'a', 'b', 'c' };

        assertFalse( foo.hashCode() == bar.hashCode() );
        //noinspection ArrayEquals
        assertFalse( foo.equals( bar ) );

        ObjIntMap<char[]> map = new CharArrayIntHashMap( );
        map.put( foo, 12 );
        assertTrue( map.containsKey( foo ) );
        assertTrue( map.containsKey( bar ) );
        assertEquals( 12, map.getInt( foo ) );
        assertEquals( 12, map.getInt( bar ) );

        Set<char[]> keys = map.keySet();
        assertTrue( keys.contains( foo ) );
        assertTrue( keys.contains( bar ) );
    }

}
