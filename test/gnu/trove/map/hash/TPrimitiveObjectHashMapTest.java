///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2006, Eric D. Friedman All Rights Reserved.
// Copyright (c) 2009, Rob Eden All Rights Reserved.
// Copyright (c) 2009, Jeff Randall All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////

package gnu.trove.map.hash;


import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.map.*;
import gnu.trove.set.*;
import gnu.trove.impl.hash.*;
import junit.framework.TestCase;

import java.util.*;
import java.lang.CharSequence;

import static gnu.trove.map.hash.TObjectPrimitiveHashMapTest.asList;


/**
 *
 */
public class TPrimitiveObjectHashMapTest extends TestCase {

    public TPrimitiveObjectHashMapTest( String name ) {
        super( name );
    }


    public void testBug2975214() {
        HashIntObjMap<Integer> map = new DHashIntObjMap<Integer>( 5 );
        for ( int i = 0; i < 9; i++ ) {
            System.out.println( "Pass: " + i );
            map.put( i, new Integer( i ) );
            map.remove( i );
        }
    }


    public void testConstructors() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }

        IntObjMap<String> capacity =
                new DHashIntObjMap<String>( 20 );
        for ( int i = 0; i < element_count; i++ ) {
            capacity.put( keys[i], vals[i] );
        }
        assertEquals( map, capacity );

        IntObjMap<String> cap_and_factor =
                new DHashIntObjMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            cap_and_factor.put( keys[i], vals[i] );
        }
        assertEquals( map, cap_and_factor );

        IntObjMap<String> fully_specified =
                new DHashIntObjMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            fully_specified.put( keys[i], vals[i] );
        }
        assertEquals( map, fully_specified );

        IntObjMap<String> copy =
                new DHashIntObjMap<String>( map );
        assertEquals( map, copy );
    }


    public void testContainsKey() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( "Key should be present: " + keys[i] + ", map: " + map,
                    map.containsKey( keys[i] ) );
        }

        int key = 1138;
        assertFalse( "Key should not be present: " + key + ", map: " + map,
                map.containsKey( key ) );
    }


    public void testContainsValue() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( "Value should be present: " + vals[i] + ", map: " + map,
                    map.containsValue( vals[i] ) );
        }

        String val = "1138";
        assertFalse( "Key should not be present: " + val + ", map: " + map,
                map.containsValue( val ) );

        assertFalse( "Random object should not be present in map: " + map,
                map.containsValue( new Object() ) );

        // test with null value
        int key = 11010110;
        map.put( key, null );
        assertTrue( map.containsKey( key ) );
        assertTrue( map.containsValue( null ) );
        assertNull( map.get( key ) );
    }


    public void testPutIfAbsent() {
        IntObjMap<String> map = new DHashIntObjMap<String>();

        map.put( 1, "One" );
        map.put( 2, "Two" );
        map.put( 3, "Three" );

        assertEquals( "One", map.putIfAbsent( 1, "Two" ) );
        assertEquals( "One", map.get( 1 ) );
        assertEquals( null, map.putIfAbsent( 9, "Nine") );
        assertEquals( "Nine", map.get( 9 ) );
    }


    public void testRemove() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }

        for ( int i = 0; i < element_count; i++ ) {
            if ( i % 2 == 1 ) {
                assertEquals( "Remove should have modified map: " + keys[i] + ", map: " + map,
                        vals[i], map.remove( keys[i] ) );
            }
        }

        for ( int i = 0; i < element_count; i++ ) {
            if ( i % 2 == 1 ) {
                assertTrue( "Removed key still in map: " + keys[i] + ", map: " + map,
                        map.get( keys[i] ) == null );
            } else {
                assertTrue( "Key should still be in map: " + keys[i] + ", map: " + map,
                        map.get( keys[i] ).equals( vals[i] ) );
            }
        }
    }


    public void testPutAllMap() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> control = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            control.put( keys[i], vals[i] );
        }

        IntObjMap<String> map = new DHashIntObjMap<String>();

        Map<Integer, String> source = new HashMap<Integer, String>();
        for ( int i = 0; i < element_count; i++ ) {
            source.put( keys[i], vals[i] );
        }

        map.putAll( source );
        assertEquals( control, map );
    }


    public void testPutAll() throws Exception {
        IntObjMap<String> t = new DHashIntObjMap<String>();
        IntObjMap<String> m = new DHashIntObjMap<String>();
        m.put( 2, "one" );
        m.put( 4, "two" );
        m.put( 6, "three" );

        t.put( 5, "four" );
        assertEquals( 1, t.size() );

        t.putAll( m );
        assertEquals( 4, t.size() );
        assertEquals( "two", t.get( 4 ) );
    }


    public void testClear() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        map.clear();
        assertTrue( map.isEmpty() );
        assertEquals( 0, map.size() );

        assertNull( map.get( keys[5] ) );
    }


    public void testKeySet() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        int[] keys_array = keyset.toIntArray();
        int count = 0;
        IntIterator iter = keyset.iterator();
        while ( iter.hasNext() ) {
            int key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }

        //noinspection ToArrayCallWithZeroLengthArrayArgument
        keys_array = keyset.toArray( new int[0] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            int key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }

        keys_array = keyset.toArray( new int[keyset.size()] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            int key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }

        keys_array = keyset.toArray( new int[keyset.size() * 2] );
        count = 0;
        iter = keyset.iterator();
        while ( iter.hasNext() ) {
            int key = iter.next();
            assertTrue( keyset.contains( key ) );
            assertEquals( keys_array[count], key );
            count++;
        }
        assertEquals( keyset.getNoEntryValue(), keys_array[keyset.size()] );

        IntSet other = new DHashIntSet( keyset );
        assertFalse( keyset.retainAll( other ) );
        other.remove( keys[5] );
        assertTrue( keyset.retainAll( other ) );
        assertFalse( keyset.contains( keys[5] ) );
        assertFalse( map.containsKey( keys[5] ) );

        keyset.clear();
        assertTrue( keyset.isEmpty() );
    }


    public void testKeySetAdds() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        try {
            keyset.add( 1138 );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            Set<Integer> test = new HashSet<Integer>();
            test.add( Integer.valueOf( 1138 ) );
            keyset.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            IntSet test = new DHashIntSet();
            test.add( 1138 );
            keyset.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }
        
        try {
            int[] test = { 1138 };
            keyset.addAll( asList( test ) );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }
    }


    public void testKeySetContainsAllCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        Collection<Integer> test_collection = new HashSet<Integer>();
        for ( int i = 0; i < element_count; i++ ) {
            test_collection.add( keys[i] );
        }
        assertTrue( keyset.containsAll( test_collection ) ) ;

        test_collection.remove( Integer.valueOf( keys[5] ) );
        assertTrue( "should contain all. keyset: " + keyset + ", " + test_collection,
                keyset.containsAll( test_collection ) );

        test_collection.add( Integer.valueOf( 1138 ) );
        assertFalse( "should not contain all. keyset: " + keyset + ", " + test_collection,
                keyset.containsAll( test_collection ) );
    }


    public void testKeySetContainsAllTCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        IntCollection test_collection = new DHashIntSet();
        for ( int i = 0; i < element_count; i++ ) {
            test_collection.add( keys[i] );
        }
        assertTrue( keyset.containsAll( test_collection ) ) ;
        assertTrue( keyset.equals( keyset ) );

        test_collection.remove( Integer.valueOf( keys[5] ) );
        assertTrue( "should contain all. keyset: " + keyset + ", " + test_collection,
                keyset.containsAll( test_collection ) );

        test_collection.add( 1138 );
        assertFalse( "should not contain all. keyset: " + keyset + ", " + test_collection,
                keyset.containsAll( test_collection ) );
    }


    public void testKeySetContainsAllArray() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        assertTrue( "should contain all. keyset: " + keyset + ", " + Arrays.toString( keys ),
                keyset.containsAll( asList( keys ) ) );

        int[] other_array = new int[ keys.length + 1 ];
        System.arraycopy( keys, 0, other_array, 0, keys.length );
        other_array[other_array.length - 1] = 1138;
        assertFalse( "should not contain all. keyset: " + keyset + ", " + Arrays.toString( other_array ),
                keyset.containsAll( asList(other_array) ) );
    }


    public void testKeySetRetainAllCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        Collection<Integer> test_collection = new HashSet<Integer>();
        for ( int i = 0; i < element_count; i++ ) {
            test_collection.add( keys[i] );
        }
        keyset.retainAll( test_collection );
        assertFalse( keyset.isEmpty() );
        assertFalse( map.isEmpty() );

        // Reset map
        for ( int i = 0; i < element_count; i++ ) {
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        test_collection.remove( Integer.valueOf( keys[5] ) );
        keyset.retainAll( test_collection );
        assertEquals( element_count - 1, keyset.size() );
        assertEquals( element_count - 1, map.size() );
        assertFalse( keyset.contains( keys[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        

        // Reset map
        for ( int i = 0; i < element_count; i++ ) {
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        test_collection.add( Integer.valueOf( 1138 ) );
        keyset.retainAll( test_collection );
        
    }


    public void testKeySetRetainAllTCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        try {
            assertFalse( "keyset: " + keyset + ", should be unmodified.",
                    keyset.retainAll( keyset ) );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

        List<Integer> other = new ArrayList<Integer>( keyset );
        assertFalse( "keyset: " + keyset + ", should be unmodified. other: " +
                     other, keyset.retainAll( other ) );

        other.remove( Integer.valueOf(keys[5]) );
        assertTrue( "keyset: " + keyset + ", should be modified. other: " +
                    other, keyset.retainAll( other ) );
        assertFalse( keyset.contains( keys[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );
        assertTrue( "keyset: " + keyset + ", should contain all in other: " +
                    other, keyset.containsAll( other ) );
    }


    public void testKeySetRetainAllArray() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        assertFalse( "keyset: " + keyset + ", should be unmodified. array: " +
                     Arrays.toString( keys ), keyset.retainAll( asList( keys ) ) );

        int[] other = new int[element_count - 1];
        for ( int i = 0; i < element_count; i++ ) {
            if ( i < 5 ) {
                other[i] = i + 1;
            }
            if ( i > 5 ) {
                other[i - 1] = i + 1;
            }
        }
        assertTrue( "keyset: " + keyset + ", should be modified. array: " +
                    Arrays.toString( other ), keyset.retainAll( asList( other ) ) );
    }


    public void testKeySetRemoveAllCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        List<Integer> java_list = new ArrayList<Integer>();
        assertFalse( "collection: " + keyset + ", should contain all in list: " +
                     java_list, keyset.removeAll( java_list ) );

        java_list.add( keys[5] );
        assertTrue( "collection: " + keyset + ", should contain all in list: " +
                    java_list, keyset.removeAll( java_list ) );
        assertFalse( keyset.contains( keys[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );

        java_list = new ArrayList<Integer>();
        for ( int key : keys ) {
            java_list.add( key );
        }
        assertTrue( "collection: " + keyset + ", should contain all in list: " +
                    java_list, keyset.removeAll( java_list ) );
        assertTrue( keyset.isEmpty() );
    }


    public void testKeySetRemoveAllTCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        List<Integer> other = new ArrayList<Integer>();
        assertFalse( "collection: " + keyset + ", should be unmodified.",
                keyset.removeAll( other ) );

        other = new ArrayList<Integer>( keyset );
        other.remove( Integer.valueOf(keys[5]) );
        assertTrue( "collection: " + keyset + ", should be modified. other: " +
                    other, keyset.removeAll( other ) );
        assertEquals( 1, keyset.size() );
        for ( int i = 0; i < element_count; i++ ) {
            if ( i == 5 ) {
                assertTrue( keyset.contains( keys[i] ) );
                assertTrue( map.containsKey( keys[i] ) );
                assertTrue( map.containsValue( vals[i] ) );
            } else {
                assertFalse( keyset.contains( keys[i] ) );
                assertFalse( map.containsKey( keys[i] ) );
                assertFalse( map.containsValue( vals[i] ) );
            }
        }

        assertFalse( "collection: " + keyset + ", should be unmodified. other: " +
                     other, keyset.removeAll( other ) );
        try {
            assertTrue( "collection: " + keyset + ", should be modified. other: " +
                        other, keyset.removeAll( keyset ) );
            assertTrue( keyset.isEmpty() );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

    }


    public void testKeySetRemoveAllArray() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntCollection keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        int[] other = {1138};
        assertFalse( "collection: " + keyset + ", should be unmodified. array: " +
                     Arrays.toString( vals ), keyset.removeAll( asList( other ) ) );

        other = new int[element_count - 1];
        for ( int i = 0; i < element_count; i++ ) {
            if ( i < 5 ) {
                other[i] = i + 1;
            }
            if ( i > 5 ) {
                other[i - 1] = i + 1;
            }
        }
        assertTrue( "collection: " + keyset + ", should be modified. array: " +
                    Arrays.toString( other ), keyset.removeAll( asList( other ) ) );
        assertEquals( 1, keyset.size() );
        for ( int i = 0; i < element_count; i++ ) {
            if ( i == 5 ) {
                assertTrue( keyset.contains( keys[i] ) );
                assertTrue( map.containsKey( keys[i] ) );
                assertTrue( map.containsValue( vals[i] ) );
            } else {
                assertFalse( keyset.contains( keys[i] ) );
                assertFalse( map.containsKey( keys[i] ) );
                assertFalse( map.containsValue( vals[i] ) );
            }
        }
    }


    public void testKeySetEqual() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        IntSet other = new DHashIntSet();
        other.addAll( asList( keys ) );

        assertTrue( "sets incorrectly not equal: " + keyset + ", " + other,
                keyset.equals( other ) );

        int[] mismatched = {72, 49, 53, 1024, 999};
        IntSet unequal = new DHashIntSet();
        unequal.addAll( asList( mismatched ) );

        assertFalse( "sets incorrectly equal: " + keyset + ", " + unequal,
                keyset.equals( unequal ) );

        // Change length, different code branch
        unequal.add( 1 );
        assertFalse( "sets incorrectly equal: " + keyset + ", " + unequal,
                keyset.equals( unequal ) );

        //noinspection ObjectEqualsNull
        assertFalse( keyset.equals( null ) );
    }


    public void testKeySetHashCode() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        IntSet keyset = map.keySet();
        for ( int i = 0; i < keyset.size(); i++ ) {
            assertTrue( keyset.contains( keys[i] ) );
        }
        assertFalse( keyset.isEmpty() );

        assertEquals( keyset.hashCode(), keyset.hashCode() );

        IntSet other = new DHashIntSet( Sequences.asIntSequence( keys ) );
        other.add( 1138 );
        assertTrue( keyset.hashCode() != other.hashCode() );
    }


    public void testKeys() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        // No argument
        int[] keys_array = map.keySet().toIntArray();
        assertEquals( element_count, keys_array.length );
        List<Integer> keys_list = asList( keys_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_list.contains( keys[i] ) );
        }

        // Zero length array
        keys_array = map.keySet().toArray( new int[0] );
        assertEquals( element_count, keys_array.length );
        keys_list = asList( keys_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_list.contains( keys[i] ) );
        }

        // appropriate length array
        keys_array = map.keySet().toArray( new int[map.size()] );
        assertEquals( element_count, keys_array.length );
        keys_list = asList( keys_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_list.contains( keys[i] ) );
        }

        // longer array
        keys_array = map.keySet().toArray( new int[element_count * 2] );
        assertEquals( element_count * 2, keys_array.length );
        keys_list = asList( keys_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( keys_list.contains( keys[i] ) );
        }
        assertEquals( map.getNoEntryKey(), keys_array[element_count] );
    }


    @SuppressWarnings({"ToArrayCallWithZeroLengthArrayArgument"})
    public void testValueCollectionToArray() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        Object[] values_array = collection.toArray();
        int count = 0;
        Iterator<String> iter = collection.iterator();
        while ( iter.hasNext() ) {
            String value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new String[0] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            String value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new String[collection.size()] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            String value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }

        values_array = collection.toArray( new String[collection.size() * 2] );
        count = 0;
        iter = collection.iterator();
        while ( iter.hasNext() ) {
            String value = iter.next();
            assertTrue( collection.contains( value ) );
            assertEquals( values_array[count], value );
            count++;
        }
        assertNull( values_array[collection.size()] );
        assertNull( values_array[collection.size()] );

        Collection<String> other = new ArrayList<String>( collection );
        assertFalse( collection.retainAll( other ) );
        other.remove( vals[5] );
        assertTrue( collection.retainAll( other ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );

        collection.clear();
        assertTrue( collection.isEmpty() );
    }


    public void testValueCollectionAdds() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        try {
            collection.add( "1138" );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            Set<String> test = new HashSet<String>();
            test.add( "1138" );
            collection.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            Collection<String> test = new ArrayList<String>();
            test.add( "1138" );
            collection.addAll( test );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }

        try {
            collection.addAll( Arrays.asList( vals ) );
            fail( "Expected UnsupportedOperationException" );
        }
        catch ( UnsupportedOperationException ex ) {
            // Expected
        }
    }


    public void testValueCollectionContainsAll() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<String> java_list = new ArrayList<String>();
        java_list.addAll( Arrays.asList( vals ) );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.containsAll( java_list ) );
        java_list.add( String.valueOf( 1138 ) );
        assertFalse( "collection: " + collection + ", should not contain all in list: " +
                     java_list, collection.containsAll( java_list ) );

        List<CharSequence> number_list = new ArrayList<CharSequence>();
        for ( String value : vals ) {
            if ( value.equals( "5" ) ) {
                number_list.add( new StringBuilder().append( value ) );
            } else {
                number_list.add( String.valueOf( value ) );
            }
        }
        assertFalse( "collection: " + collection + ", should not contain all in list: " +
                     java_list, collection.containsAll( number_list ) );

        Collection<String> other = new ArrayList<String>( collection );
        assertTrue( "collection: " + collection + ", should contain all in other: " +
                    other, collection.containsAll( other ) );
        other.add( "1138" );
        assertFalse( "collection: " + collection + ", should not contain all in other: " +
                     other, collection.containsAll( other ) );
    }


    public void testValueCollectionRetainAllCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<String> java_list = new ArrayList<String>();
        java_list.addAll( Arrays.asList( vals ) );
        assertFalse( "collection: " + collection + ", should contain all in list: " +
                     java_list, collection.retainAll( java_list ) );

        java_list.remove( 5 );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.retainAll( java_list ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.containsAll( java_list ) );
    }


    public void testValueCollectionRetainAllTCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );
        try {
            assertFalse( "collection: " + collection + ", should be unmodified.",
                    collection.retainAll( collection ) );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

        Collection<String> other = new ArrayList<String>( collection );
        assertFalse( "collection: " + collection + ", should be unmodified. other: " +
                     other, collection.retainAll( other ) );

        other.remove( vals[5] );
        assertTrue( "collection: " + collection + ", should be modified. other: " +
                    other, collection.retainAll( other ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );
        assertTrue( "collection: " + collection + ", should contain all in other: " +
                    other, collection.containsAll( other ) );
    }


    public void testValueCollectionRemoveAllCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        List<String> java_list = new ArrayList<String>();
        assertFalse( "collection: " + collection + ", should contain all in list: " +
                     java_list, collection.removeAll( java_list ) );

        java_list.add( vals[5] );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.removeAll( java_list ) );
        assertFalse( collection.contains( vals[5] ) );
        assertFalse( map.containsKey( keys[5] ) );
        assertFalse( map.containsValue( vals[5] ) );

        java_list = new ArrayList<String>();
        java_list.addAll( Arrays.asList( vals ) );
        assertTrue( "collection: " + collection + ", should contain all in list: " +
                    java_list, collection.removeAll( java_list ) );
        assertTrue( collection.isEmpty() );
    }


    public void testValueCollectionRemoveAllTCollection() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        Collection<String> collection = map.values();
        for ( int i = 0; i < collection.size(); i++ ) {
            assertTrue( collection.contains( vals[i] ) );
        }
        assertFalse( collection.isEmpty() );

        Collection<String> other = new ArrayList<String>();
        assertFalse( "collection: " + collection + ", should be unmodified.",
                collection.removeAll( other ) );

        other = new ArrayList<String>( collection );
        other.remove( vals[5] );
        assertTrue( "collection: " + collection + ", should be modified. other: " +
                    other, collection.removeAll( other ) );
        assertEquals( 1, collection.size() );
        for ( int i = 0; i < element_count; i++ ) {
            if ( i == 5 ) {
                assertTrue( collection.contains( vals[i] ) );
                assertTrue( map.containsKey( keys[i] ) );
                assertTrue( map.containsValue( vals[i] ) );
            } else {
                assertFalse( collection.contains( vals[i] ) );
                assertFalse( map.containsKey( keys[i] ) );
                assertFalse( map.containsValue( vals[i] ) );
            }
        }

        assertFalse( "collection: " + collection + ", should be unmodified. other: " +
                     other, collection.removeAll( other ) );
        try {
            assertTrue( "collection: " + collection + ", should be modified. other: " +
                        other, collection.removeAll( collection ) );
            assertTrue( collection.isEmpty() );
        } catch ( IllegalArgumentException e ) {
            // OK
        }

    }


    public void testValues() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map = new DHashIntObjMap<String>();
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        // No argument
        Object[] values_object_array = map.values().toArray();
        assertEquals( element_count, values_object_array.length );
        List<Object> values_object_list = Arrays.asList( values_object_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_object_list.contains( vals[i] ) );
        }

        // Zero length array
        String[] values_array = map.values().toArray( new String[ 0 ] );
        assertEquals( element_count, values_array.length );
        List<String> values_list = Arrays.asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }

        // appropriate length array
        values_array = map.values().toArray( new String[ map.size() ] );
        assertEquals( element_count, values_array.length );
        values_list = Arrays.asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }

        // longer array
        values_array = map.values().toArray( new String[ element_count * 2 ] );
        assertEquals( element_count * 2, values_array.length );
        values_list = Arrays.asList( values_array );
        for ( int i = 0; i < element_count; i++ ) {
            assertTrue( values_list.contains( vals[i] ) );
        }
        assertEquals( null, values_array[element_count] );
    }


    public void testIterator() {
        HashIntObjMap<String> map = new DHashIntObjMap<String>();

        IntKeyMapIterator<String> iterator = map.mapIterator();
        assertFalse( iterator.hasNext() );

        map.put( 1, "one" );
        map.put( 2, "two" );

        iterator = map.mapIterator();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();

        int first_key = iterator.key();
        assertNotNull( "key was null", first_key );
        assertTrue( "invalid key: " + first_key, first_key == 1 || first_key == 2 );
        if ( first_key == 1 ) {
            assertEquals( "one", iterator.value() );
        } else {
            assertEquals( "two", iterator.value() );
        }

        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        int second_key = iterator.key();
        assertNotNull( "key was null", second_key );
        assertTrue( "invalid key: " + second_key, second_key == 1 || second_key == 2 );
        if ( second_key == 1 ) {
            assertEquals( "one", iterator.value() );
        } else {
            assertEquals( "two", iterator.value() );
        }
        assertFalse( first_key + ", " + second_key, first_key == second_key );

        assertFalse( iterator.hasNext() );

        // New Iterator
        iterator = map.mapIterator();
        iterator.tryAdvance();
        first_key = iterator.key();
        iterator.setValue( "1138" );
        assertEquals( "1138", iterator.value() );
        assertEquals( "1138", map.get( first_key ) );
    }


    public void testIteratorRemoval() {
        HashIntObjMap<String> map = new DHashIntObjMap<String>();

        map.put( 1, "one" );
        map.put( 2, "two" );
        map.put( 3, "three" );
        map.put( 4, "four" );
        map.put( 5, "five" );
        map.put( 6, "six" );
        map.put( 7, "seven" );
        map.put( 8, "eight" );
        map.put( 9, "nine" );
        map.put( 10, "ten" );

        IntKeyMapIterator<String> iterator = map.mapIterator();
        while ( map.size() > 5 && iterator.hasNext() ) {
            iterator.tryAdvance();
            int key = iterator.key();
            iterator.remove();
            assertFalse( "map still contains key: " + key, map.containsKey( key ) );
        }

        assertEquals( 5, map.size() );
        iterator = map.mapIterator();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertTrue( iterator.hasNext() );
        iterator.tryAdvance();
        assertFalse( iterator.hasNext() );

        iterator = map.mapIterator();
        int elements = map.size();
        while ( iterator.hasNext() ) {
            iterator.tryAdvance();
            int key = iterator.key();
            iterator.remove();
            assertFalse( "map still contains key: " + key, map.containsKey( key ) );
            elements--;
        }
        assertEquals( 0, elements );

        assertEquals( 0, map.size() );
        assertTrue( map.isEmpty() );
    }


    public void testIteratorRemoval2() {
        int element_count = 10000;
        int remaining = element_count / 2;

        HashIntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );

        for ( int pass = 0; pass < 10; pass++ ) {
            Random r = new Random();
            for ( int i = 0; i <= element_count; i++ ) {
                map.put( Integer.valueOf( r.nextInt() ), String.valueOf( i ) );
            }

            IntKeyMapIterator iterator = map.mapIterator();
            while ( map.size() > remaining && iterator.hasNext() ) {
                iterator.tryAdvance();
                iterator.remove();
            }
        }
    }


    public void testForEachKey() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements IntConsumer {

            List<Integer> built = new ArrayList<Integer>();


            public void accept( int value ) {
                built.add( value );
            }


            List<Integer> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.keySet().forEach( foreach );
        List<Integer> built = foreach.getBuilt();
        List<Integer> keys_list = asList( map.keySet().toArray( new int[ map.size() ] ) );
        assertEquals( keys_list, built );


        class ForEachFalse implements IntPredicate {

            List<Integer> built = new ArrayList<Integer>();


            public boolean test( int value ) {
                built.add( value );
                return false;
            }


            List<Integer> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.keySet().testWhile( foreach_false );
        built = foreach_false.getBuilt();
        keys_list = asList( map.keySet().toArray( new int[ map.size() ] ) );
        assertEquals( 1, built.size() );
        assertEquals( keys_list.get( 0 ), built.get( 0 ) );
    }


    public void testForEachValue() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements Predicate<String> {

            List<String> built = new ArrayList<String>();


            public boolean test( String value ) {
                built.add( value );
                return true;
            }


            List<String> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.values().testWhile( foreach );
        List<String> built = foreach.getBuilt();
        List<String> values = Arrays.asList( map.values().toArray( new String[ 0 ] ) );
        assertEquals( values, built );

        Collections.sort( built );
        Collections.sort( values );
        assertEquals( values, built );


        class ForEachFalse implements Predicate<String> {

            List<String> built = new ArrayList<String>();


            public boolean test( String value ) {
                built.add( value );
                return false;
            }


            List<String> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.values().testWhile( foreach_false );
        built = foreach_false.getBuilt();
        values = Arrays.asList( map.values().toArray( new String[ 0 ] ) );
        assertEquals( 1, built.size() );
        assertEquals( values.get( 0 ), built.get( 0 ) );
    }


    public void testForEachEntry() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        class ForEach implements IntObjPredicate<String> {

            IntObjMap<String> built = new DHashIntObjMap<String>();


            public boolean test( int key, String value ) {
                built.put( key, value );
                return true;
            }


            IntObjMap<String> getBuilt() {
                return built;
            }
        }

        ForEach foreach = new ForEach();
        map.testWhile( foreach );
        IntObjMap<String> built = foreach.getBuilt();
        assertEquals( map, built );


        class ForEachFalse implements IntObjPredicate<String> {

            IntObjMap<String> built = new DHashIntObjMap<String>();


            public boolean test( int key, String value ) {
                built.put( key, value );
                return false;
            }


            IntObjMap<String> getBuilt() {
                return built;
            }
        }

        ForEachFalse foreach_false = new ForEachFalse();
        map.testWhile( foreach_false );
        built = foreach_false.getBuilt();
        assertEquals( 1, built.size() );
        assertTrue( map.containsKey( built.keySet().toIntArray()[0] ) );
        assertTrue( map.containsValue( built.values().toArray()[0] ) );
    }


    public void testRetain() {
        HashIntObjMap<String> map = new DHashIntObjMap<String>();

        map.put( 1, "one" );
        map.put( 2, "two" );
        map.put( 3, "three" );
        map.put( 4, "four" );
        map.put( 5, "five" );
        map.put( 6, "six" );
        map.put( 7, "seven" );
        map.put( 8, "eight" );
        map.put( 9, "nine" );
        map.put( 10, "ten" );

        map.removeIf( new IntObjPredicate<String>() {
            public boolean test( int a, String b ) {
                return !( a > 5 && a <= 8 );
            }
        } );

        assertEquals( 3, map.size() );
        assertFalse( map.containsValue( "one" ) );
        assertFalse( map.containsValue( "two" ) );
        assertFalse( map.containsValue( "three" ) );
        assertFalse( map.containsValue( "four" ) );
        assertFalse( map.containsValue( "five" ) );
        assertTrue( map.containsValue( "six" ) );
        assertTrue( map.containsValue( "seven" ) );
        assertTrue( map.containsValue( "eight" ) );
        assertFalse( map.containsValue( "nine" ) );
        assertFalse( map.containsValue( "ten" ) );

        map.put( 11, "eleven" );
        map.put( 12, "twelve" );
        map.put( 13, "thirteen" );
        map.put( 14, "fourteen" );
        map.put( 15, "fifteen" );
        map.put( 16, "sixteen" );
        map.put( 17, "seventeen" );
        map.put( 18, "eighteen" );
        map.put( 19, "nineteen" );
        map.put( 20, "twenty" );


        map.removeIf( new IntObjPredicate<String>() {
            public boolean test( int a, String b ) {
                return a <= 15;
            }
        } );

        assertEquals( 5, map.size() );
        assertFalse( map.containsValue( "one" ) );
        assertFalse( map.containsValue( "two" ) );
        assertFalse( map.containsValue( "three" ) );
        assertFalse( map.containsValue( "four" ) );
        assertFalse( map.containsValue( "five" ) );
        assertFalse( map.containsValue( "six" ) );
        assertFalse( map.containsValue( "seven" ) );
        assertFalse( map.containsValue( "eight" ) );
        assertFalse( map.containsValue( "nine" ) );
        assertFalse( map.containsValue( "ten" ) );
        assertFalse( map.containsValue( "eleven" ) );
        assertFalse( map.containsValue( "twelve" ) );
        assertFalse( map.containsValue( "thirteen" ) );
        assertFalse( map.containsValue( "fourteen" ) );
        assertFalse( map.containsValue( "fifteen" ) );
        assertTrue( map.containsValue( "sixteen" ) );
        assertTrue( map.containsValue( "seventeen" ) );
        assertTrue( map.containsValue( "eighteen" ) );
        assertTrue( map.containsValue( "nineteen" ) );
        assertTrue( map.containsValue( "twenty" ) );


        map.removeIf( new IntObjPredicate<String>() {
            public boolean test( int a, String b ) {
                return true;
            }
        } );

        assertEquals( 0, map.size() );
    }


    public void testTransformValues() {
       int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        map.replaceAll( new BiFunction<Integer, String, String>() {
            public String apply( Integer a, String b ) {
                return b + "/" + b;
            }
        });

        for ( int i = 0; i < element_count; i++ ) {
            String expected = vals[i] + "/" + vals[i];
            assertEquals( expected, map.get( keys[i] ) );
        }
    }


    public void testEquals() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++ ) {
            keys[i] = i + 1;
            vals[i] = Integer.toString( i + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );

        HashIntObjMap<String> fully_specified =
                new DHashIntObjMap<String>( 20, 0.75f );
        for ( int i = 0; i < element_count; i++ ) {
            fully_specified.put( keys[i], vals[i] );
        }
        assertEquals( map, fully_specified );

        assertFalse( "shouldn't equal random object", map.equals( new Object() ) );

        int key = 11010110;     // I thought I saw a two!
        assertNull( map.put( key, null ) );
        assertNull( fully_specified.put( key, null ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertTrue( "incorrectly not-equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );

        assertNull( fully_specified.put( key, "non-null-value" ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                fully_specified.equals( map ) );

        assertNull( fully_specified.put( key + 1, "blargh" ) );
        assertFalse( "incorrectly equal map: " + map + "\nfully_specified: " + fully_specified,
                map.equals( fully_specified ) );
    }


    public void testHashCode() {
        int element_count = 20;
        int[] keys = new int[element_count];
        String[] vals = new String[element_count];
        int counter = 0;

        IntObjMap<String> map =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = counter + 1;
            vals[i] = Integer.toString( counter + 1 );
            map.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map.size() );
        assertEquals( map.hashCode(), map.hashCode() );

        Map<String, IntObjMap<String>> string_tmap_map =
                new HashMap<String, IntObjMap<String>>();
        string_tmap_map.put( "first", map );
        string_tmap_map.put( "second", map );
        assertSame( map, string_tmap_map.get( "first" ) );
        assertSame( map, string_tmap_map.get( "second" ) );

        IntObjMap<String> map2 =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = counter + 1;
            vals[i] = Integer.toString( counter + 1 );
            map2.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map2.size() );
        assertEquals( map2.hashCode(), map2.hashCode() );

        IntObjMap<String> map3 =
                new DHashIntObjMap<String>( element_count, 0.5f );
        for ( int i = 0; i < element_count; i++, counter++ ) {
            keys[i] = counter + 1;
            vals[i] = Integer.toString( counter + 1 );
            map3.put( keys[i], vals[i] );
        }
        assertEquals( element_count, map3.size() );
        assertEquals( map3.hashCode(), map3.hashCode() );

        assertFalse( "hashcodes are unlikely equal.  map: " + map + " (" + map.hashCode() +
                     ")\nmap2: " + map2 + " (" + map2.hashCode() + ")",
                map.hashCode() == map2.hashCode() );
        assertFalse( "hashcodes are unlikely equal.  map: " + map + " (" + map.hashCode() +
                     ")\nmap3: " + map3 + " (" + map3.hashCode() + ")",
                map.hashCode() == map3.hashCode() );
        assertFalse( "hashcodes are unlikely equal.  map2: " + map2 + " (" + map2.hashCode() + 
                     ")\nmap3: " + map3 + " (" + map3.hashCode() + ")",
                map2.hashCode() == map3.hashCode() );

        Map<IntObjMap<String>, String> tmap_string_map =
                new HashMap<IntObjMap<String>, String>();
        tmap_string_map.put( map, "map1" );
        tmap_string_map.put( map2, "map2" );
        tmap_string_map.put( map3, "map3" );
        assertEquals( "map1", tmap_string_map.get( map ) );
        assertEquals( "map2", tmap_string_map.get( map2 ) );
        assertEquals( "map3", tmap_string_map.get( map3 ) );
    }


    public void testToString() {
        HashIntObjMap<String> m = new DHashIntObjMap<String>();
        m.put( 11, "One" );
        m.put( 22, "Two" );

        String to_string = m.toString();
        assertTrue( to_string, to_string.equals( "{11=One, 22=Two}" )
                               || to_string.equals( "{22=Two, 11=One}" ) );
    }


    /**
     * Test for tracking issue #1204014. +0.0 and -0.0 have different bit patterns, but
     * should be counted the same as keys in a map. Checks for doubles and floats.
     */
    public void testFloatZeroHashing() {
        HashDoubleObjMap<String> po_double_map = new DHashDoubleObjMap<String>();
        HashDoubleIntMap pp_double_map = new DHashDoubleIntMap();
        HashFloatObjMap<String> po_float_map = new DHashFloatObjMap<String>();
        HashFloatIntMap pp_float_map = new DHashFloatIntMap();

        final double zero_double = 0.0;
        final double negative_zero_double = -zero_double;
        final float zero_float = 0.0f;
        final float negative_zero_float = -zero_float;

        // Sanity check... make sure I'm really creating two different values.
        final String zero_bits_double =
                Long.toBinaryString( Double.doubleToLongBits( zero_double ) );
        final String negative_zero_bits_double =
                Long.toBinaryString( Double.doubleToLongBits( negative_zero_double ) );
        assertFalse( zero_bits_double + " == " + negative_zero_bits_double,
                zero_bits_double.equals( negative_zero_bits_double ) );

        final String zero_bits_float =
                Integer.toBinaryString( Float.floatToIntBits( zero_float ) );
        final String negative_zero_bits_float =
                Integer.toBinaryString( Float.floatToIntBits( negative_zero_float ) );
        assertFalse( zero_bits_float + " == " + negative_zero_bits_float,
                zero_bits_float.equals( negative_zero_bits_float ) );


        po_double_map.put( zero_double, "Zero" );
        po_double_map.put( negative_zero_double, "Negative Zero" );

        pp_double_map.put( zero_double, 0 );
        pp_double_map.put( negative_zero_double, -1 );

        po_float_map.put( zero_float, "Zero" );
        po_float_map.put( negative_zero_float, "Negative Zero" );

        pp_float_map.put( zero_float, 0 );
        pp_float_map.put( negative_zero_float, -1 );


        assertEquals( 1, po_double_map.size() );
        assertEquals( po_double_map.get( zero_double ), "Negative Zero" );
        assertEquals( po_double_map.get( negative_zero_double ), "Negative Zero" );

        assertEquals( 1, pp_double_map.size() );
        assertEquals( pp_double_map.get( zero_double ), -1 );
        assertEquals( pp_double_map.get( negative_zero_double ), -1 );

        assertEquals( 1, po_float_map.size() );
        assertEquals( po_float_map.get( zero_float ), "Negative Zero" );
        assertEquals( po_float_map.get( negative_zero_float ), "Negative Zero" );

        assertEquals( 1, pp_float_map.size() );
        assertEquals( pp_float_map.get( zero_float ), -1 );
        assertEquals( pp_float_map.get( negative_zero_float ), -1 );


        po_double_map.put( zero_double, "Zero" );
        pp_double_map.put( zero_double, 0 );
        po_float_map.put( zero_float, "Zero" );
        pp_float_map.put( zero_float, 0 );


        assertEquals( 1, po_double_map.size() );
        assertEquals( po_double_map.get( zero_double ), "Zero" );
        assertEquals( po_double_map.get( negative_zero_double ), "Zero" );

        assertEquals( 1, pp_double_map.size() );
        assertEquals( pp_double_map.get( zero_double ), 0 );
        assertEquals( pp_double_map.get( negative_zero_double ), 0 );

        assertEquals( 1, po_float_map.size() );
        assertEquals( po_float_map.get( zero_float ), "Zero" );
        assertEquals( po_float_map.get( negative_zero_float ), "Zero" );

        assertEquals( 1, pp_float_map.size() );
        assertEquals( pp_float_map.get( zero_float ), 0 );
        assertEquals( pp_float_map.get( negative_zero_float ), 0 );
    }


    // Testing issue from:
    //    https://sourceforge.net/projects/trove4j/forums/forum/121845/topic/4969032
    public void testForumIssue_20120124() {
        HashIntObjMap<String> string_map = new DHashIntObjMap<String>();
        string_map.put( 1, "one" );
        string_map.put( 2, "two" );
        string_map.put( 3, "three" );

        int count = 0;
        for ( String string : string_map.values().toArray( new String [0] ) ) {
            count++;
        }

        assertEquals( 3, count );
    }
}
