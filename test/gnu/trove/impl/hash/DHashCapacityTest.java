// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.impl.hash.DHashByteSet;
import gnu.trove.set.hash.HashByteSet;
import junit.framework.TestCase;

public class DHashCapacityTest extends TestCase {
    public static void testExtremeCapacity() {
        HashByteSet set = new DHashByteSet( Integer.MAX_VALUE );
        assertEquals( DHash.MAX_CAPACITY, set.capacity() );
    }
}
