package gnu.trove.impl.hash;

import gnu.trove.impl.hash.DHashObjLongMap;
import gnu.trove.map.hash.HashObjLongMap;
import junit.framework.TestCase;


/**
 *
 */
public class ObjDHashTest extends TestCase {
    // Test case bug bug ID 3067307
    public static void testBug3067307() {
        HashObjLongMap<String> testHash = new DHashObjLongMap<String>();
        final int c = 1000;
        for ( long i = 1; i < c; i++ ) {
            final String data = "test-" + i;
            testHash.put( data, i );
            testHash.remove( data );
        }
    }

    // Test case bug bug ID 3067307
    public static void testBug3067307_noAutoCompact() {
        HashObjLongMap<String> testHash = new DHashObjLongMap<String>(1001);
        final int c = 1000;
        for ( long i = 1; i < c; i++ ) {
            final String data = "test-" + i;
            testHash.put( data, i );
            testHash.remove( data );
        }
    }
}
