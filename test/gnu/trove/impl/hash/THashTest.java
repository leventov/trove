package gnu.trove.impl.hash;

import junit.framework.TestCase;
import gnu.trove.map.IntLongMap;


/**
 * tests that need access to internals of DHash or DHashObjSet
 */
public class THashTest extends TestCase {

    public THashTest( String name ) {
        super( name );
    }


    public void setUp() throws Exception {
        super.setUp();
    }


    public void tearDown() throws Exception {
        super.tearDown();
    }


    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    public void testNormalLoad() throws Exception {
        DHashObjSet<Integer> set = new DHashObjSet<Integer>( 11, 0.5f );
        assertEquals( 23, set.capacity() );
        for ( int i = 0; i <= 13; i++ ) {
            set.add( i );
        }
        assertTrue( set.capacity() > 23 );
    }


    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    public void testMaxLoad() throws Exception {
        DHashObjSet<Integer> set = new DHashObjSet<Integer>( 16, 0.99f );
        assertEquals( 19, set.capacity() );
        for ( int i = 0; i <= 15; i++ ) {
            set.add( i );
        }
        assertEquals( 19, set.capacity() );
    }




    public void testReusesRemovedSlotsOnCollision() {
        DHashObjSet<Object> set = new DHashObjSet<Object>( 11, 0.5f );

        class Foo {

            public int hashCode() {
                return 4;
            }
        }

        Foo f1 = new Foo();
        Foo f2 = new Foo();
        Foo f3 = new Foo();
        set.add( f1 );

        int idx = set.insertionIndex( f2 ).get();
        set.add( f2 );
        assertEquals( f2, set.set[idx] );
        set.remove( f2 );
        assertEquals( ObjDHash.REMOVED, set.set[idx] );
        assertEquals( idx, set.insertionIndex( f3 ).get() );
        set.add( f3 );
        assertEquals( f3, set.set[idx] );
    }


    public void testCompact() throws Exception {
        DHashObjObjMap<Integer,Integer> map = new DHashObjObjMap<Integer,Integer>();
        
        Integer[] data = new Integer[1000];

        for (int i = 0; i < 1000; i++) {
            data[i] = i;
            map.put(data[i], data[i]);
        }
        int initialCapacity = map.capacity();
        assertTrue(initialCapacity > 1000);
        for (int i = 0; i < 1000; i+=2) {
            map.remove(data[i]);
        }
        assertEquals(500, map.size());
        // rehash if removed strictly > size
        assertEquals( map.capacity(), initialCapacity );
        map.shrink();
        assertEquals(500, map.size());
        assertTrue(map.capacity() < initialCapacity);
    }


    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    public void testTPHashMapConstructors() {

        int size = 20;

        DHashObjObjMap cap_and_factor = new DHashObjObjMap( size, 0.75f );
        assertTrue( "capacity not sufficient: " + size + ", " + cap_and_factor.capacity(),
                size <= cap_and_factor.capacity() );
        assertEquals( 0.75f, cap_and_factor.loadFactor() );
    }


    public void testTPrimitivePrimitveHashMapConstructors() {

        int size = 20;

        IntLongMap cap_and_factor = new DHashIntLongMap( size, 0.75f );
        StatesDHash cap_and_factor_hash = (StatesDHash ) cap_and_factor;
        assertTrue( "capacity not sufficient: " + size + ", " + cap_and_factor_hash.capacity(),
                size <= cap_and_factor_hash.capacity() );
        assertEquals( 0.75f, cap_and_factor_hash.loadFactor() );
    }

}
