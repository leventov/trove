package gnu.trove.set.hash;

import gnu.trove.impl.hash.DHashObjSet;
import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Set;


/**
 *
 */
public class TCustomHashSetTest extends TestCase {

    static class CharArrayHashSet extends DHashObjSet<char[]> {

        public CharArrayHashSet() {}

        protected int elementHashCode( @NotNull char[] obj ) {
            return Arrays.hashCode( obj );
        }

        protected boolean elementsEquals( @NotNull char[] e1, char[] e2 ) {
            return Arrays.equals( e1, e2 );
        }
    }

    public void testArray() {
        char[] foo = new char[] { 'a', 'b', 'c' };
        char[] bar = new char[] { 'a', 'b', 'c' };

        Set<char[]> set = new CharArrayHashSet();
        set.add( foo );
        assertTrue( set.contains( foo ) );
        assertTrue( set.contains( bar ) );

        set.remove( bar );

        assertTrue( set.isEmpty() );
    }

}
