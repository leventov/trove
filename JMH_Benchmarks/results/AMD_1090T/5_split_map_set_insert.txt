# Measurement Section
# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_charSet_0_5
# Warmup Iteration   1 (1s in 1 thread): 26,418 usec/op
# Warmup Iteration   2 (1s in 1 thread): 25,838 usec/op
# Warmup Iteration   3 (1s in 1 thread): 25,950 usec/op
Iteration   1 (100ms in 1 thread): 24,940 usec/op
Iteration   2 (100ms in 1 thread): 25,185 usec/op
Iteration   3 (100ms in 1 thread): 25,757 usec/op
Iteration   4 (100ms in 1 thread): 25,330 usec/op
Iteration   5 (100ms in 1 thread): 25,859 usec/op
Iteration   6 (100ms in 1 thread): 24,600 usec/op
Iteration   7 (100ms in 1 thread): 25,881 usec/op
Iteration   8 (100ms in 1 thread): 24,704 usec/op
Iteration   9 (100ms in 1 thread): 25,566 usec/op
Iteration  10 (100ms in 1 thread): 25,444 usec/op
Iteration  11 (100ms in 1 thread): 25,950 usec/op
Iteration  12 (100ms in 1 thread): 25,627 usec/op
Iteration  13 (100ms in 1 thread): 26,032 usec/op
Iteration  14 (100ms in 1 thread): 25,355 usec/op
Iteration  15 (100ms in 1 thread): 24,725 usec/op
Iteration  16 (100ms in 1 thread): 25,499 usec/op
Iteration  17 (100ms in 1 thread): 25,486 usec/op
Iteration  18 (100ms in 1 thread): 25,477 usec/op
Iteration  19 (100ms in 1 thread): 25,772 usec/op
Iteration  20 (100ms in 1 thread): 25,366 usec/op

Run result "construction_charSet_0_5": 25,428 ±(95%) 0,196 ±(99%) 0,268 usec/op
Run statistics "construction_charSet_0_5": min = 24,600, avg = 25,428, max = 26,032, stdev = 0,420
Run confidence intervals "construction_charSet_0_5": 95% [25,231, 25,624], 99% [25,159, 25,696]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_charSet_0_8
# Warmup Iteration   1 (1s in 1 thread): 30,402 usec/op
# Warmup Iteration   2 (1s in 1 thread): 31,063 usec/op
# Warmup Iteration   3 (1s in 1 thread): 30,515 usec/op
Iteration   1 (100ms in 1 thread): 30,052 usec/op
Iteration   2 (100ms in 1 thread): 29,856 usec/op
Iteration   3 (100ms in 1 thread): 30,065 usec/op
Iteration   4 (100ms in 1 thread): 30,801 usec/op
Iteration   5 (100ms in 1 thread): 30,504 usec/op
Iteration   6 (100ms in 1 thread): 29,927 usec/op
Iteration   7 (100ms in 1 thread): 30,129 usec/op
Iteration   8 (100ms in 1 thread): 30,083 usec/op
Iteration   9 (100ms in 1 thread): 30,728 usec/op
Iteration  10 (100ms in 1 thread): 29,993 usec/op
Iteration  11 (100ms in 1 thread): 30,275 usec/op
Iteration  12 (100ms in 1 thread): 31,280 usec/op
Iteration  13 (100ms in 1 thread): 30,297 usec/op
Iteration  14 (100ms in 1 thread): 30,828 usec/op
Iteration  15 (100ms in 1 thread): 30,498 usec/op
Iteration  16 (100ms in 1 thread): 30,651 usec/op
Iteration  17 (100ms in 1 thread): 30,798 usec/op
Iteration  18 (100ms in 1 thread): 31,044 usec/op
Iteration  19 (100ms in 1 thread): 29,714 usec/op
Iteration  20 (100ms in 1 thread): 30,164 usec/op

Run result "construction_charSet_0_8": 30,384 ±(95%) 0,202 ±(99%) 0,275 usec/op
Run statistics "construction_charSet_0_8": min = 29,714, avg = 30,384, max = 31,280, stdev = 0,431
Run confidence intervals "construction_charSet_0_8": 95% [30,183, 30,586], 99% [30,109, 30,660]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_doubleSet_0_5
# Warmup Iteration   1 (1s in 1 thread): 33,191 usec/op
# Warmup Iteration   2 (1s in 1 thread): 31,723 usec/op
# Warmup Iteration   3 (1s in 1 thread): 31,414 usec/op
Iteration   1 (100ms in 1 thread): 31,285 usec/op
Iteration   2 (100ms in 1 thread): 31,826 usec/op
Iteration   3 (100ms in 1 thread): 31,788 usec/op
Iteration   4 (100ms in 1 thread): 31,841 usec/op
Iteration   5 (100ms in 1 thread): 32,594 usec/op
Iteration   6 (100ms in 1 thread): 31,786 usec/op
Iteration   7 (100ms in 1 thread): 32,456 usec/op
Iteration   8 (100ms in 1 thread): 31,414 usec/op
Iteration   9 (100ms in 1 thread): 32,242 usec/op
Iteration  10 (100ms in 1 thread): 32,794 usec/op
Iteration  11 (100ms in 1 thread): 32,560 usec/op
Iteration  12 (100ms in 1 thread): 32,421 usec/op
Iteration  13 (100ms in 1 thread): 32,112 usec/op
Iteration  14 (100ms in 1 thread): 33,112 usec/op
Iteration  15 (100ms in 1 thread): 33,055 usec/op
Iteration  16 (100ms in 1 thread): 32,326 usec/op
Iteration  17 (100ms in 1 thread): 32,964 usec/op
Iteration  18 (100ms in 1 thread): 32,692 usec/op
Iteration  19 (100ms in 1 thread): 32,391 usec/op
Iteration  20 (100ms in 1 thread): 32,171 usec/op

Run result "construction_doubleSet_0_5": 32,292 ±(95%) 0,242 ±(99%) 0,331 usec/op
Run statistics "construction_doubleSet_0_5": min = 31,285, avg = 32,292, max = 33,112, stdev = 0,518
Run confidence intervals "construction_doubleSet_0_5": 95% [32,049, 32,534], 99% [31,960, 32,623]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_doubleSet_0_8
# Warmup Iteration   1 (1s in 1 thread): 43,093 usec/op
# Warmup Iteration   2 (1s in 1 thread): 41,937 usec/op
# Warmup Iteration   3 (1s in 1 thread): 41,391 usec/op
Iteration   1 (100ms in 1 thread): 42,005 usec/op
Iteration   2 (100ms in 1 thread): 43,036 usec/op
Iteration   3 (100ms in 1 thread): 42,817 usec/op
Iteration   4 (100ms in 1 thread): 42,988 usec/op
Iteration   5 (100ms in 1 thread): 43,190 usec/op
Iteration   6 (100ms in 1 thread): 41,968 usec/op
Iteration   7 (100ms in 1 thread): 42,673 usec/op
Iteration   8 (100ms in 1 thread): 42,839 usec/op
Iteration   9 (100ms in 1 thread): 42,421 usec/op
Iteration  10 (100ms in 1 thread): 43,311 usec/op
Iteration  11 (100ms in 1 thread): 42,283 usec/op
Iteration  12 (100ms in 1 thread): 42,142 usec/op
Iteration  13 (100ms in 1 thread): 42,950 usec/op
Iteration  14 (100ms in 1 thread): 42,328 usec/op
Iteration  15 (100ms in 1 thread): 42,282 usec/op
Iteration  16 (100ms in 1 thread): 42,575 usec/op
Iteration  17 (100ms in 1 thread): 42,117 usec/op
Iteration  18 (100ms in 1 thread): 42,589 usec/op
Iteration  19 (100ms in 1 thread): 41,407 usec/op
Iteration  20 (100ms in 1 thread): 42,441 usec/op

Run result "construction_doubleSet_0_8": 42,518 ±(95%) 0,221 ±(99%) 0,302 usec/op
Run statistics "construction_doubleSet_0_8": min = 41,407, avg = 42,518, max = 43,311, stdev = 0,473
Run confidence intervals "construction_doubleSet_0_8": 95% [42,297, 42,739], 99% [42,216, 42,820]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_intSet_0_5
# Warmup Iteration   1 (1s in 1 thread): 30,711 usec/op
# Warmup Iteration   2 (1s in 1 thread): 30,416 usec/op
# Warmup Iteration   3 (1s in 1 thread): 29,746 usec/op
Iteration   1 (100ms in 1 thread): 28,965 usec/op
Iteration   2 (100ms in 1 thread): 29,973 usec/op
Iteration   3 (100ms in 1 thread): 29,149 usec/op
Iteration   4 (100ms in 1 thread): 30,464 usec/op
Iteration   5 (100ms in 1 thread): 29,626 usec/op
Iteration   6 (100ms in 1 thread): 30,521 usec/op
Iteration   7 (100ms in 1 thread): 30,452 usec/op
Iteration   8 (100ms in 1 thread): 30,145 usec/op
Iteration   9 (100ms in 1 thread): 29,738 usec/op
Iteration  10 (100ms in 1 thread): 29,553 usec/op
Iteration  11 (100ms in 1 thread): 29,170 usec/op
Iteration  12 (100ms in 1 thread): 29,886 usec/op
Iteration  13 (100ms in 1 thread): 30,128 usec/op
Iteration  14 (100ms in 1 thread): 30,595 usec/op
Iteration  15 (100ms in 1 thread): 29,950 usec/op
Iteration  16 (100ms in 1 thread): 29,734 usec/op
Iteration  17 (100ms in 1 thread): 29,730 usec/op
Iteration  18 (100ms in 1 thread): 29,750 usec/op
Iteration  19 (100ms in 1 thread): 29,826 usec/op
Iteration  20 (100ms in 1 thread): 29,048 usec/op

Run result "construction_intSet_0_5": 29,820 ±(95%) 0,227 ±(99%) 0,311 usec/op
Run statistics "construction_intSet_0_5": min = 28,965, avg = 29,820, max = 30,595, stdev = 0,486
Run confidence intervals "construction_intSet_0_5": 95% [29,593, 30,048], 99% [29,509, 30,131]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.AvgProbing.construction_intSet_0_8
# Warmup Iteration   1 (1s in 1 thread): 37,675 usec/op
# Warmup Iteration   2 (1s in 1 thread): 37,854 usec/op
# Warmup Iteration   3 (1s in 1 thread): 37,264 usec/op
Iteration   1 (100ms in 1 thread): 37,224 usec/op
Iteration   2 (100ms in 1 thread): 37,263 usec/op
Iteration   3 (100ms in 1 thread): 36,664 usec/op
Iteration   4 (100ms in 1 thread): 37,407 usec/op
Iteration   5 (100ms in 1 thread): 36,673 usec/op
Iteration   6 (100ms in 1 thread): 36,690 usec/op
Iteration   7 (100ms in 1 thread): 36,562 usec/op
Iteration   8 (100ms in 1 thread): 36,837 usec/op
Iteration   9 (100ms in 1 thread): 36,697 usec/op
Iteration  10 (100ms in 1 thread): 36,822 usec/op
Iteration  11 (100ms in 1 thread): 36,231 usec/op
Iteration  12 (100ms in 1 thread): 37,262 usec/op
Iteration  13 (100ms in 1 thread): 36,860 usec/op
Iteration  14 (100ms in 1 thread): 36,918 usec/op
Iteration  15 (100ms in 1 thread): 37,443 usec/op
Iteration  16 (100ms in 1 thread): 36,573 usec/op
Iteration  17 (100ms in 1 thread): 36,079 usec/op
Iteration  18 (100ms in 1 thread): 36,489 usec/op
Iteration  19 (100ms in 1 thread): 36,817 usec/op
Iteration  20 (100ms in 1 thread): 36,736 usec/op

Run result "construction_intSet_0_8": 36,812 ±(95%) 0,170 ±(99%) 0,233 usec/op
Run statistics "construction_intSet_0_8": min = 36,079, avg = 36,812, max = 37,443, stdev = 0,364
Run confidence intervals "construction_intSet_0_8": 95% [36,642, 36,983], 99% [36,580, 37,045]


Benchmark                                       Mode Thr    Cnt  Sec         Mean   Mean error    Units
g.t.j.AvgProbing.construction_charSet_0_5       avgt   1     20    0       25,428        0,268  usec/op
g.t.j.AvgProbing.construction_charSet_0_8       avgt   1     20    0       30,384        0,275  usec/op
g.t.j.AvgProbing.construction_doubleSet_0_5     avgt   1     20    0       32,292        0,331  usec/op
g.t.j.AvgProbing.construction_doubleSet_0_8     avgt   1     20    0       42,518        0,302  usec/op
g.t.j.AvgProbing.construction_intSet_0_5        avgt   1     20    0       29,820        0,311  usec/op
g.t.j.AvgProbing.construction_intSet_0_8        avgt   1     20    0       36,812        0,233  usec/op
