# Measurement Section
# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.charSet_forEachFunction
# Warmup Iteration   1 (1s in 1 thread): 4,799 usec/op
# Warmup Iteration   2 (1s in 1 thread): 6,320 usec/op
# Warmup Iteration   3 (1s in 1 thread): 4,805 usec/op
Iteration   1 (100ms in 1 thread): 7,234 usec/op
Iteration   2 (100ms in 1 thread): 7,292 usec/op
Iteration   3 (100ms in 1 thread): 7,297 usec/op
Iteration   4 (100ms in 1 thread): 7,393 usec/op
Iteration   5 (100ms in 1 thread): 7,302 usec/op
Iteration   6 (100ms in 1 thread): 4,927 usec/op
Iteration   7 (100ms in 1 thread): 7,419 usec/op
Iteration   8 (100ms in 1 thread): 7,359 usec/op
Iteration   9 (100ms in 1 thread): 7,458 usec/op
Iteration  10 (100ms in 1 thread): 4,826 usec/op
Iteration  11 (100ms in 1 thread): 4,707 usec/op
Iteration  12 (100ms in 1 thread): 7,320 usec/op
Iteration  13 (100ms in 1 thread): 7,401 usec/op
Iteration  14 (100ms in 1 thread): 5,155 usec/op
Iteration  15 (100ms in 1 thread): 4,810 usec/op
Iteration  16 (100ms in 1 thread): 4,673 usec/op
Iteration  17 (100ms in 1 thread): 4,620 usec/op
Iteration  18 (100ms in 1 thread): 7,146 usec/op
Iteration  19 (100ms in 1 thread): 5,047 usec/op
Iteration  20 (100ms in 1 thread): 4,618 usec/op

Run result "charSet_forEachFunction": 6,200 ±(95%) 0,603 ±(99%) 0,824 usec/op
Run statistics "charSet_forEachFunction": min = 4,618, avg = 6,200, max = 7,458, stdev = 1,288
Run confidence intervals "charSet_forEachFunction": 95% [5,597, 6,803], 99% [5,376, 7,024]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.charSet_forEachLoop
# Warmup Iteration   1 (1s in 1 thread): 17,515 usec/op
# Warmup Iteration   2 (1s in 1 thread): 17,416 usec/op
# Warmup Iteration   3 (1s in 1 thread): 12,865 usec/op
Iteration   1 (100ms in 1 thread): 13,047 usec/op
Iteration   2 (100ms in 1 thread): 13,019 usec/op
Iteration   3 (100ms in 1 thread): 13,021 usec/op
Iteration   4 (100ms in 1 thread): 13,036 usec/op
Iteration   5 (100ms in 1 thread): 13,081 usec/op
Iteration   6 (100ms in 1 thread): 13,075 usec/op
Iteration   7 (100ms in 1 thread): 12,837 usec/op
Iteration   8 (100ms in 1 thread): 13,064 usec/op
Iteration   9 (100ms in 1 thread): 13,042 usec/op
Iteration  10 (100ms in 1 thread): 12,975 usec/op
Iteration  11 (100ms in 1 thread): 12,937 usec/op
Iteration  12 (100ms in 1 thread): 13,180 usec/op
Iteration  13 (100ms in 1 thread): 12,962 usec/op
Iteration  14 (100ms in 1 thread): 12,784 usec/op
Iteration  15 (100ms in 1 thread): 13,147 usec/op
Iteration  16 (100ms in 1 thread): 12,982 usec/op
Iteration  17 (100ms in 1 thread): 13,034 usec/op
Iteration  18 (100ms in 1 thread): 13,035 usec/op
Iteration  19 (100ms in 1 thread): 12,857 usec/op
Iteration  20 (100ms in 1 thread): 12,973 usec/op

Run result "charSet_forEachLoop": 13,004 ±(95%) 0,045 ±(99%) 0,062 usec/op
Run statistics "charSet_forEachLoop": min = 12,784, avg = 13,004, max = 13,180, stdev = 0,097
Run confidence intervals "charSet_forEachLoop": 95% [12,959, 13,050], 99% [12,942, 13,067]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.charSet_simple
# Warmup Iteration   1 (1s in 1 thread): 9,067 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,823 usec/op
# Warmup Iteration   3 (1s in 1 thread): 8,773 usec/op
Iteration   1 (100ms in 1 thread): 8,498 usec/op
Iteration   2 (100ms in 1 thread): 8,493 usec/op
Iteration   3 (100ms in 1 thread): 8,504 usec/op
Iteration   4 (100ms in 1 thread): 8,489 usec/op
Iteration   5 (100ms in 1 thread): 8,528 usec/op
Iteration   6 (100ms in 1 thread): 8,431 usec/op
Iteration   7 (100ms in 1 thread): 8,681 usec/op
Iteration   8 (100ms in 1 thread): 8,388 usec/op
Iteration   9 (100ms in 1 thread): 8,406 usec/op
Iteration  10 (100ms in 1 thread): 8,532 usec/op
Iteration  11 (100ms in 1 thread): 8,741 usec/op
Iteration  12 (100ms in 1 thread): 8,545 usec/op
Iteration  13 (100ms in 1 thread): 8,768 usec/op
Iteration  14 (100ms in 1 thread): 8,489 usec/op
Iteration  15 (100ms in 1 thread): 8,434 usec/op
Iteration  16 (100ms in 1 thread): 8,417 usec/op
Iteration  17 (100ms in 1 thread): 8,683 usec/op
Iteration  18 (100ms in 1 thread): 8,728 usec/op
Iteration  19 (100ms in 1 thread): 8,757 usec/op
Iteration  20 (100ms in 1 thread): 8,649 usec/op

Run result "charSet_simple": 8,558 ±(95%) 0,060 ±(99%) 0,082 usec/op
Run statistics "charSet_simple": min = 8,388, avg = 8,558, max = 8,768, stdev = 0,128
Run confidence intervals "charSet_simple": 95% [8,498, 8,618], 99% [8,476, 8,640]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.charSet_tryAdvance
# Warmup Iteration   1 (1s in 1 thread): 8,243 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,828 usec/op
# Warmup Iteration   3 (1s in 1 thread): 8,548 usec/op
Iteration   1 (100ms in 1 thread): 8,619 usec/op
Iteration   2 (100ms in 1 thread): 8,518 usec/op
Iteration   3 (100ms in 1 thread): 8,742 usec/op
Iteration   4 (100ms in 1 thread): 8,639 usec/op
Iteration   5 (100ms in 1 thread): 8,649 usec/op
Iteration   6 (100ms in 1 thread): 8,603 usec/op
Iteration   7 (100ms in 1 thread): 9,228 usec/op
Iteration   8 (100ms in 1 thread): 8,563 usec/op
Iteration   9 (100ms in 1 thread): 8,714 usec/op
Iteration  10 (100ms in 1 thread): 8,409 usec/op
Iteration  11 (100ms in 1 thread): 8,737 usec/op
Iteration  12 (100ms in 1 thread): 8,485 usec/op
Iteration  13 (100ms in 1 thread): 8,671 usec/op
Iteration  14 (100ms in 1 thread): 8,653 usec/op
Iteration  15 (100ms in 1 thread): 8,571 usec/op
Iteration  16 (100ms in 1 thread): 8,651 usec/op
Iteration  17 (100ms in 1 thread): 8,331 usec/op
Iteration  18 (100ms in 1 thread): 8,657 usec/op
Iteration  19 (100ms in 1 thread): 8,822 usec/op
Iteration  20 (100ms in 1 thread): 8,566 usec/op

Run result "charSet_tryAdvance": 8,641 ±(95%) 0,084 ±(99%) 0,115 usec/op
Run statistics "charSet_tryAdvance": min = 8,331, avg = 8,641, max = 9,228, stdev = 0,179
Run confidence intervals "charSet_tryAdvance": 95% [8,557, 8,725], 99% [8,527, 8,756]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.doubleSet_forEachFunction
# Warmup Iteration   1 (1s in 1 thread): 9,005 usec/op
# Warmup Iteration   2 (1s in 1 thread): 6,825 usec/op
# Warmup Iteration   3 (1s in 1 thread): 8,162 usec/op
Iteration   1 (100ms in 1 thread): 8,184 usec/op
Iteration   2 (100ms in 1 thread): 8,080 usec/op
Iteration   3 (100ms in 1 thread): 8,159 usec/op
Iteration   4 (100ms in 1 thread): 5,708 usec/op
Iteration   5 (100ms in 1 thread): 5,702 usec/op
Iteration   6 (100ms in 1 thread): 8,147 usec/op
Iteration   7 (100ms in 1 thread): 8,226 usec/op
Iteration   8 (100ms in 1 thread): 5,102 usec/op
Iteration   9 (100ms in 1 thread): 5,121 usec/op
Iteration  10 (100ms in 1 thread): 5,121 usec/op
Iteration  11 (100ms in 1 thread): 5,232 usec/op
Iteration  12 (100ms in 1 thread): 8,172 usec/op
Iteration  13 (100ms in 1 thread): 5,105 usec/op
Iteration  14 (100ms in 1 thread): 8,133 usec/op
Iteration  15 (100ms in 1 thread): 5,182 usec/op
Iteration  16 (100ms in 1 thread): 5,667 usec/op
Iteration  17 (100ms in 1 thread): 8,256 usec/op
Iteration  18 (100ms in 1 thread): 8,279 usec/op
Iteration  19 (100ms in 1 thread): 5,249 usec/op
Iteration  20 (100ms in 1 thread): 8,228 usec/op

Run result "doubleSet_forEachFunction": 6,753 ±(95%) 0,694 ±(99%) 0,948 usec/op
Run statistics "doubleSet_forEachFunction": min = 5,102, avg = 6,753, max = 8,279, stdev = 1,483
Run confidence intervals "doubleSet_forEachFunction": 95% [6,059, 7,447], 99% [5,804, 7,701]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.doubleSet_forEachLoop
# Warmup Iteration   1 (1s in 1 thread): 9,783 usec/op
# Warmup Iteration   2 (1s in 1 thread): 9,959 usec/op
# Warmup Iteration   3 (1s in 1 thread): 9,788 usec/op
Iteration   1 (100ms in 1 thread): 9,896 usec/op
Iteration   2 (100ms in 1 thread): 10,010 usec/op
Iteration   3 (100ms in 1 thread): 9,930 usec/op
Iteration   4 (100ms in 1 thread): 9,739 usec/op
Iteration   5 (100ms in 1 thread): 9,906 usec/op
Iteration   6 (100ms in 1 thread): 9,774 usec/op
Iteration   7 (100ms in 1 thread): 9,889 usec/op
Iteration   8 (100ms in 1 thread): 9,993 usec/op
Iteration   9 (100ms in 1 thread): 10,058 usec/op
Iteration  10 (100ms in 1 thread): 9,976 usec/op
Iteration  11 (100ms in 1 thread): 9,952 usec/op
Iteration  12 (100ms in 1 thread): 9,882 usec/op
Iteration  13 (100ms in 1 thread): 10,099 usec/op
Iteration  14 (100ms in 1 thread): 10,153 usec/op
Iteration  15 (100ms in 1 thread): 9,905 usec/op
Iteration  16 (100ms in 1 thread): 9,849 usec/op
Iteration  17 (100ms in 1 thread): 9,973 usec/op
Iteration  18 (100ms in 1 thread): 9,922 usec/op
Iteration  19 (100ms in 1 thread): 10,047 usec/op
Iteration  20 (100ms in 1 thread): 9,982 usec/op

Run result "doubleSet_forEachLoop": 9,947 ±(95%) 0,047 ±(99%) 0,065 usec/op
Run statistics "doubleSet_forEachLoop": min = 9,739, avg = 9,947, max = 10,153, stdev = 0,101
Run confidence intervals "doubleSet_forEachLoop": 95% [9,900, 9,994], 99% [9,882, 10,011]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.doubleSet_simple
# Warmup Iteration   1 (1s in 1 thread): 8,343 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,300 usec/op
# Warmup Iteration   3 (1s in 1 thread): 7,959 usec/op
Iteration   1 (100ms in 1 thread): 7,997 usec/op
Iteration   2 (100ms in 1 thread): 8,311 usec/op
Iteration   3 (100ms in 1 thread): 8,036 usec/op
Iteration   4 (100ms in 1 thread): 8,228 usec/op
Iteration   5 (100ms in 1 thread): 8,044 usec/op
Iteration   6 (100ms in 1 thread): 8,083 usec/op
Iteration   7 (100ms in 1 thread): 8,241 usec/op
Iteration   8 (100ms in 1 thread): 8,155 usec/op
Iteration   9 (100ms in 1 thread): 8,088 usec/op
Iteration  10 (100ms in 1 thread): 7,960 usec/op
Iteration  11 (100ms in 1 thread): 7,964 usec/op
Iteration  12 (100ms in 1 thread): 7,973 usec/op
Iteration  13 (100ms in 1 thread): 8,172 usec/op
Iteration  14 (100ms in 1 thread): 7,904 usec/op
Iteration  15 (100ms in 1 thread): 8,091 usec/op
Iteration  16 (100ms in 1 thread): 8,073 usec/op
Iteration  17 (100ms in 1 thread): 8,148 usec/op
Iteration  18 (100ms in 1 thread): 8,108 usec/op
Iteration  19 (100ms in 1 thread): 7,954 usec/op
Iteration  20 (100ms in 1 thread): 8,079 usec/op

Run result "doubleSet_simple": 8,081 ±(95%) 0,050 ±(99%) 0,068 usec/op
Run statistics "doubleSet_simple": min = 7,904, avg = 8,081, max = 8,311, stdev = 0,107
Run confidence intervals "doubleSet_simple": 95% [8,030, 8,131], 99% [8,012, 8,149]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.doubleSet_tryAdvance
# Warmup Iteration   1 (1s in 1 thread): 8,274 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,340 usec/op
# Warmup Iteration   3 (1s in 1 thread): 8,718 usec/op
Iteration   1 (100ms in 1 thread): 8,496 usec/op
Iteration   2 (100ms in 1 thread): 9,196 usec/op
Iteration   3 (100ms in 1 thread): 8,273 usec/op
Iteration   4 (100ms in 1 thread): 8,285 usec/op
Iteration   5 (100ms in 1 thread): 8,314 usec/op
Iteration   6 (100ms in 1 thread): 9,437 usec/op
Iteration   7 (100ms in 1 thread): 8,652 usec/op
Iteration   8 (100ms in 1 thread): 9,289 usec/op
Iteration   9 (100ms in 1 thread): 9,325 usec/op
Iteration  10 (100ms in 1 thread): 9,351 usec/op
Iteration  11 (100ms in 1 thread): 8,320 usec/op
Iteration  12 (100ms in 1 thread): 8,930 usec/op
Iteration  13 (100ms in 1 thread): 9,302 usec/op
Iteration  14 (100ms in 1 thread): 9,283 usec/op
Iteration  15 (100ms in 1 thread): 9,172 usec/op
Iteration  16 (100ms in 1 thread): 8,288 usec/op
Iteration  17 (100ms in 1 thread): 9,250 usec/op
Iteration  18 (100ms in 1 thread): 8,322 usec/op
Iteration  19 (100ms in 1 thread): 8,471 usec/op
Iteration  20 (100ms in 1 thread): 8,426 usec/op

Run result "doubleSet_tryAdvance": 8,819 ±(95%) 0,217 ±(99%) 0,296 usec/op
Run statistics "doubleSet_tryAdvance": min = 8,273, avg = 8,819, max = 9,437, stdev = 0,463
Run confidence intervals "doubleSet_tryAdvance": 95% [8,602, 9,036], 99% [8,523, 9,116]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.intSet_forEachFunction
# Warmup Iteration   1 (1s in 1 thread): 7,899 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,026 usec/op
# Warmup Iteration   3 (1s in 1 thread): 6,688 usec/op
Iteration   1 (100ms in 1 thread): 8,021 usec/op
Iteration   2 (100ms in 1 thread): 7,082 usec/op
Iteration   3 (100ms in 1 thread): 6,921 usec/op
Iteration   4 (100ms in 1 thread): 7,858 usec/op
Iteration   5 (100ms in 1 thread): 8,013 usec/op
Iteration   6 (100ms in 1 thread): 6,467 usec/op
Iteration   7 (100ms in 1 thread): 6,687 usec/op
Iteration   8 (100ms in 1 thread): 6,575 usec/op
Iteration   9 (100ms in 1 thread): 5,438 usec/op
Iteration  10 (100ms in 1 thread): 6,706 usec/op
Iteration  11 (100ms in 1 thread): 7,966 usec/op
Iteration  12 (100ms in 1 thread): 5,404 usec/op
Iteration  13 (100ms in 1 thread): 6,813 usec/op
Iteration  14 (100ms in 1 thread): 8,374 usec/op
Iteration  15 (100ms in 1 thread): 8,126 usec/op
Iteration  16 (100ms in 1 thread): 6,731 usec/op
Iteration  17 (100ms in 1 thread): 6,630 usec/op
Iteration  18 (100ms in 1 thread): 6,586 usec/op
Iteration  19 (100ms in 1 thread): 7,951 usec/op
Iteration  20 (100ms in 1 thread): 7,992 usec/op

Run result "intSet_forEachFunction": 7,117 ±(95%) 0,410 ±(99%) 0,560 usec/op
Run statistics "intSet_forEachFunction": min = 5,404, avg = 7,117, max = 8,374, stdev = 0,876
Run confidence intervals "intSet_forEachFunction": 95% [6,707, 7,527], 99% [6,557, 7,677]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.intSet_forEachLoop
# Warmup Iteration   1 (1s in 1 thread): 19,903 usec/op
# Warmup Iteration   2 (1s in 1 thread): 19,984 usec/op
# Warmup Iteration   3 (1s in 1 thread): 15,006 usec/op
Iteration   1 (100ms in 1 thread): 15,009 usec/op
Iteration   2 (100ms in 1 thread): 15,286 usec/op
Iteration   3 (100ms in 1 thread): 15,215 usec/op
Iteration   4 (100ms in 1 thread): 15,139 usec/op
Iteration   5 (100ms in 1 thread): 15,147 usec/op
Iteration   6 (100ms in 1 thread): 15,122 usec/op
Iteration   7 (100ms in 1 thread): 15,204 usec/op
Iteration   8 (100ms in 1 thread): 15,031 usec/op
Iteration   9 (100ms in 1 thread): 15,037 usec/op
Iteration  10 (100ms in 1 thread): 15,243 usec/op
Iteration  11 (100ms in 1 thread): 14,910 usec/op
Iteration  12 (100ms in 1 thread): 15,217 usec/op
Iteration  13 (100ms in 1 thread): 15,279 usec/op
Iteration  14 (100ms in 1 thread): 15,183 usec/op
Iteration  15 (100ms in 1 thread): 15,279 usec/op
Iteration  16 (100ms in 1 thread): 15,190 usec/op
Iteration  17 (100ms in 1 thread): 15,187 usec/op
Iteration  18 (100ms in 1 thread): 15,088 usec/op
Iteration  19 (100ms in 1 thread): 15,202 usec/op
Iteration  20 (100ms in 1 thread): 15,095 usec/op

Run result "intSet_forEachLoop": 15,153 ±(95%) 0,047 ±(99%) 0,064 usec/op
Run statistics "intSet_forEachLoop": min = 14,910, avg = 15,153, max = 15,286, stdev = 0,100
Run confidence intervals "intSet_forEachLoop": 95% [15,106, 15,200], 99% [15,089, 15,217]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.intSet_simple
# Warmup Iteration   1 (1s in 1 thread): 8,002 usec/op
# Warmup Iteration   2 (1s in 1 thread): 8,212 usec/op
# Warmup Iteration   3 (1s in 1 thread): 7,634 usec/op
Iteration   1 (100ms in 1 thread): 8,238 usec/op
Iteration   2 (100ms in 1 thread): 8,236 usec/op
Iteration   3 (100ms in 1 thread): 8,222 usec/op
Iteration   4 (100ms in 1 thread): 8,252 usec/op
Iteration   5 (100ms in 1 thread): 8,324 usec/op
Iteration   6 (100ms in 1 thread): 8,254 usec/op
Iteration   7 (100ms in 1 thread): 7,772 usec/op
Iteration   8 (100ms in 1 thread): 8,056 usec/op
Iteration   9 (100ms in 1 thread): 7,801 usec/op
Iteration  10 (100ms in 1 thread): 8,563 usec/op
Iteration  11 (100ms in 1 thread): 8,001 usec/op
Iteration  12 (100ms in 1 thread): 7,775 usec/op
Iteration  13 (100ms in 1 thread): 8,191 usec/op
Iteration  14 (100ms in 1 thread): 7,833 usec/op
Iteration  15 (100ms in 1 thread): 8,337 usec/op
Iteration  16 (100ms in 1 thread): 7,966 usec/op
Iteration  17 (100ms in 1 thread): 7,811 usec/op
Iteration  18 (100ms in 1 thread): 7,998 usec/op
Iteration  19 (100ms in 1 thread): 8,174 usec/op
Iteration  20 (100ms in 1 thread): 8,062 usec/op

Run result "intSet_simple": 8,093 ±(95%) 0,104 ±(99%) 0,142 usec/op
Run statistics "intSet_simple": min = 7,772, avg = 8,093, max = 8,563, stdev = 0,221
Run confidence intervals "intSet_simple": 95% [7,990, 8,197], 99% [7,952, 8,235]


# Runtime (per iteration): 100ms
# Iterations: 20
# Thread counts (concurrent threads per iteration): [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
# Threads will synchronize iterations
# Benchmark mode: Average time, time/op
# Running: gnu.trove.jmh_benchmark.Iteration.intSet_tryAdvance
# Warmup Iteration   1 (1s in 1 thread): 8,254 usec/op
# Warmup Iteration   2 (1s in 1 thread): 7,530 usec/op
# Warmup Iteration   3 (1s in 1 thread): 8,106 usec/op
Iteration   1 (100ms in 1 thread): 7,943 usec/op
Iteration   2 (100ms in 1 thread): 8,129 usec/op
Iteration   3 (100ms in 1 thread): 7,724 usec/op
Iteration   4 (100ms in 1 thread): 7,988 usec/op
Iteration   5 (100ms in 1 thread): 8,069 usec/op
Iteration   6 (100ms in 1 thread): 7,968 usec/op
Iteration   7 (100ms in 1 thread): 8,126 usec/op
Iteration   8 (100ms in 1 thread): 8,328 usec/op
Iteration   9 (100ms in 1 thread): 8,027 usec/op
Iteration  10 (100ms in 1 thread): 7,965 usec/op
Iteration  11 (100ms in 1 thread): 7,946 usec/op
Iteration  12 (100ms in 1 thread): 8,073 usec/op
Iteration  13 (100ms in 1 thread): 8,047 usec/op
Iteration  14 (100ms in 1 thread): 7,914 usec/op
Iteration  15 (100ms in 1 thread): 8,273 usec/op
Iteration  16 (100ms in 1 thread): 7,959 usec/op
Iteration  17 (100ms in 1 thread): 7,993 usec/op
Iteration  18 (100ms in 1 thread): 8,139 usec/op
Iteration  19 (100ms in 1 thread): 8,170 usec/op
Iteration  20 (100ms in 1 thread): 8,117 usec/op

Run result "intSet_tryAdvance": 8,045 ±(95%) 0,063 ±(99%) 0,086 usec/op
Run statistics "intSet_tryAdvance": min = 7,724, avg = 8,045, max = 8,328, stdev = 0,134
Run confidence intervals "intSet_tryAdvance": 95% [7,982, 8,108], 99% [7,959, 8,131]


Benchmark                                     Mode Thr    Cnt  Sec         Mean   Mean error    Units
g.t.j.Iteration.charSet_forEachFunction       avgt   1     20    0        6,200        0,824  usec/op
g.t.j.Iteration.charSet_forEachLoop           avgt   1     20    0       13,004        0,062  usec/op
g.t.j.Iteration.charSet_simple                avgt   1     20    0        8,558        0,082  usec/op
g.t.j.Iteration.charSet_tryAdvance            avgt   1     20    0        8,641        0,115  usec/op
g.t.j.Iteration.doubleSet_forEachFunction     avgt   1     20    0        6,753        0,948  usec/op
g.t.j.Iteration.doubleSet_forEachLoop         avgt   1     20    0        9,947        0,065  usec/op
g.t.j.Iteration.doubleSet_simple              avgt   1     20    0        8,081        0,068  usec/op
g.t.j.Iteration.doubleSet_tryAdvance          avgt   1     20    0        8,819        0,296  usec/op
g.t.j.Iteration.intSet_forEachFunction        avgt   1     20    0        7,117        0,560  usec/op
g.t.j.Iteration.intSet_forEachLoop            avgt   1     20    0       15,153        0,064  usec/op
g.t.j.Iteration.intSet_simple                 avgt   1     20    0        8,093        0,142  usec/op
g.t.j.Iteration.intSet_tryAdvance             avgt   1     20    0        8,045        0,086  usec/op
