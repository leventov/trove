package gnu.trove.jmh_benchmark;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.hash.*;
import gnu.trove.set.hash.HashCharSet;
import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class Iteration {
    public static final int GENERIC_SET_SIZE = 1000;

/* Char|Int|Double elem */

    public static class CharSum implements CharConsumer {
        public char sum;
        public void accept( char b ) {
            sum += b;
        }
    }

    @State
    public static class CharSetState {
        public HashCharSet set;
        public byte[] _states;
        public char[] _set;
        public CharIterator iterator;
        public CharSum sum = new CharSum();


        @Setup(Level.Trial)
        public void init() {
            set = new DHashCharSet( GENERIC_SET_SIZE );
        }

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            Random r = new Random();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add( (char) r.nextInt() );
            }
        }

        @Setup(Level.Invocation)
        public void createIterator() {
            iterator = set.iterator();
        }
    }

    @GenerateMicroBenchmark
    public char charSet_simple(CharSetState state) {
        char sum = 0;
        CharIterator it = state.iterator;
        while (it.hasNext()) {
            sum += it.nextChar();
        }
        return sum;
    }


    @GenerateMicroBenchmark
    public char charSet_forEachLoop(CharSetState state) {
        char sum = 0;
        CharIterator it = state.iterator;
        char v;
        while (it.hasNext()) {
            v = it.next();
            sum += v;
        }
        return sum;
    }

    @GenerateMicroBenchmark
    public char charSet_forEachFunction(CharSetState state) {
        state.set.forEach( state.sum );
        return state.sum.sum;
    }

    @GenerateMicroBenchmark
    public char charSet_tryAdvance(CharSetState state) {
        char sum = 0;
        CharIterator it = state.iterator;
        while (it.tryAdvance()) {
            sum += it.charValue();
        }
        return sum;
    }

/* end */

}
