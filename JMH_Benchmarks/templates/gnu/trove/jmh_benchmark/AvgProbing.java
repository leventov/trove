package gnu.trove.jmh_benchmark;

import gnu.trove.impl.hash.*;
import gnu.trove.set.hash.HashCharSet;
import org.openjdk.jmh.annotations.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 3, time = 1, timeUnit = TimeUnit.SECONDS)
@Measurement(time = 100, timeUnit = TimeUnit.MILLISECONDS)
public class AvgProbing {
    public static final int GENERIC_SET_SIZE = 1000;

/* Char|Int|Double elem */

    private static void shuffleChars(Random r, char[] a) {
        for (int i = a.length; i-- > 0; ) {
            int ri = r.nextInt(i + 1);
            if (ri != i) {
                char rc = a[ri];
                a[ri] = a[i];
                a[i] = rc;
            }
        }
    }

    @State
    public static class _0_5_CharSetFailState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.5f );
        public char[] notInSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            int i = 0;
            while ( i < GENERIC_SET_SIZE ) {
                char val = ( char ) r.nextInt();
                if (!set.contains( val )) {
                    notInSet[i++] = val;
                }
            }
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            shuffleChars( r, notInSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean contains_charSet_0_5_fail( _0_5_CharSetFailState state ) {
        char[] notInSet = state.notInSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : notInSet ) {
            r |= set.contains( c );
        }
        return r;
    }


    @State
    public static class _0_8_CharSetFailState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.8f );
        public char[] notInSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            int i = 0;
            while ( i < GENERIC_SET_SIZE ) {
                char val = ( char ) r.nextInt();
                if (!set.contains( val )) {
                    notInSet[i++] = val;
                }
            }
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            shuffleChars( r, notInSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean contains_charSet_0_8_fail( _0_8_CharSetFailState state ) {
        char[] notInSet = state.notInSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : notInSet ) {
            r |= set.contains( c );
        }
        return r;
    }


    @State
    public static class _0_5_CharSetSuccessState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.5f );
        public char[] inSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            inSet = set.toArray(inSet);
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            shuffleChars( r, inSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean contains_charSet_0_5_success( _0_5_CharSetSuccessState state ) {
        char[] inSet = state.inSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : inSet ) {
            r |= set.contains( c );
        }
        return r;
    }


    @State
    public static class _0_8_CharSetSuccessState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.8f );
        public char[] inSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            inSet = set.toArray(inSet);
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            shuffleChars( r, inSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean contains_charSet_0_8_success( _0_8_CharSetSuccessState state ) {
        char[] inSet = state.inSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : inSet ) {
            r |= set.contains( c );
        }
        return r;
    }

    @State
    public static class _0_5_CharSetConstructionState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.5f );
        public char[] inSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            inSet = set.toArray(inSet);
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            set.clear();
            shuffleChars( r, inSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean construction_charSet_0_5( _0_5_CharSetConstructionState state ) {
        char[] inSet = state.inSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : inSet ) {
            r |= set.add( c );
        }
        return r;
    }

    @State
    public static class _0_8_CharSetConstructionState {
        public HashCharSet set = new DHashCharSet( GENERIC_SET_SIZE, 0.8f );
        public char[] inSet = new char[GENERIC_SET_SIZE];
        private Random r = new Random();

        @Setup(Level.Iteration)
        public void fill() {
            set.clear();
            while (set.size() < GENERIC_SET_SIZE) {
                set.add((char) r.nextInt());
            }
            inSet = set.toArray(inSet);
        }

        @Setup(Level.Invocation)
        public void shuffle() {
            set.clear();
            shuffleChars( r, inSet );
        }
    }

    @GenerateMicroBenchmark
    public boolean construction_charSet_0_8( _0_8_CharSetConstructionState state ) {
        char[] inSet = state.inSet;
        HashCharSet set = state.set;
        boolean r = false;
        for ( char c : inSet ) {
            r |= set.add( c );
        }
        return r;
    }

/* end */

}