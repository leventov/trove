1. Installing JMH
    See http://openjdk.java.net/projects/code-tools/jmh/:

    $ hg clone http://hg.openjdk.java.net/code-tools/jmh/ jmh
    $ cd jmh/
    $ mvn clean install -DskipTests=true

    That is all. See pom.xml

2. Other preparations
    Create a results/ subdir with your processor name

    Install indicator-cpufreq Gnome applet or any other tool
    to control CPU frequency

2. Develop benchmarks in templates/ dir.

3. Build benchmarks using build_jmh_benchmarks task of main project Ant script

5. Running
    1) Close all heavy applications: browsers, IDEs, etc.

    2) Ensure that CPU frequency is set to constant maximum

    3) Running itself:
    $ java -jar target/microbenchmarks.jar --threads 1 '.*' | \
        tee results/<proc_name>/<revision>_<key_changes_with_prev_revision>.txt

    For example,
    $ java -jar target/microbenchmarks.jar --threads 1 '.*' | \
        tee results/AMD_1090T/1_jmh_initial.txt

    4) Don't forget to restore dynamic or low CPU frequency

6. Analyze results in perspective
    Some examples:

    $ grep -r 'g.t.j.*Probing.*fail' results/AMD_1090T | sort
    $ grep -r 'g.t.j.*Iteration.*int' results/AMD_1090T | sort




