// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////
package gnu.trove;

import gnu.trove.function.Consumer;

import java.util.Iterator;

/**
 * Extension of standard {@link Iterator} interface, used in Trove.
 *
 * <p>{@link #tryAdvance()} may be a little more efficient than {@link #next()},
 * which is actually a combination of {@code tryAdvance()} and {@code value()}
 * from descendant interfaces, when the passing element is not needed.
 *
 * <p>Note that original {@link java.util.Iterator#remove()} contract
 * is slightly violated to allow faster implementation.
 *
 * @param <E> the type of elements returned by this iterator
 */
public interface TIterator<E> extends Iterator<E> {

    /**
     * Returns {@code true} if the iteration has more elements.
     * In other words, returns {@code true} if {@link #next()}
     * wouldn't throw a {@code NoSuchElementException}.
     *
     * @return {@code true} if the iteration has more elements
     */
    boolean hasNext();


    /**
     * Moves the iterator forward to the next element, if it exists, and returns
     * {@code true}, returns {@code false} if the iteration has no more elements.
     *
     * @return {@code true} if the iterator has moved forward to the next element,
     *         {@code false} if the iteration has no more elements
     */
    boolean tryAdvance();


    /**
     * Returns the last element, passed by the iterator.
     *
     * <p>{@code TIterator} implementations should document specific
     * type of runtime exceptions, thrown if {@code value()} is invoked on
     * newly-created iterator prior to advancing.
     *
     * @return the last element, passed by the iterator
     * @throws RuntimeException if the {@code value()} method is invoked on
     *         on newly-created iterator prior to calling {@link #tryAdvance()}
     *         or {@link #next()} at least once
     */
    E value();


    /**
     * Moves the iterator forward to the next element, and returns it.
     * Calling {@code move()} has the exactly same effect as calling
     * {@code value()} method, preceded by {@link #tryAdvance()}.
     *
     * @return the next element in the iteration
     * @throws java.util.NoSuchElementException if the iteration has no more elements
     */
    E next();


    /**
     * Removes from the underlying collection the last element, passed
     * by the iterator (optional operation).
     *
     * <p>The result of invoking this method more than once for a single element
     * as well as when the iterator hasn't passed any element yet
     * is undefined and can leave the underlying collection in a confused state.
     * Note the difference with {@link java.util.Iterator#remove()}
     * contract, which guides to throw {@code IllegalStateException} in such situations.
     *
     * @throws UnsupportedOperationException if the {@code remove}
     *         operation is not supported by this iterator
     */
    void remove();
}
