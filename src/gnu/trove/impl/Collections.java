// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl;

import gnu.trove.TCollection;
import gnu.trove.function.*;
import gnu.trove.set.TSet;
import org.jetbrains.annotations.NotNull;

import java.util.*;


public final class Collections {

    private Collections() {}


    ////////////////////////
    // Collections


    public static <E> boolean containsAll( Collection<E> collection,
            Collection<?> another) {
        for ( Object o : another ) {
            if ( !collection.contains( o ) )
                return false;
        }
        return true;
    }

    public static <E> boolean addAll( Collection<E> collection,
            Collection<? extends E> another) {
        boolean changed = false;
        for ( E e : another ) {
            changed |= collection.add( e );
        }
        return changed;
    }


    //TODO doc
    public static <E> String toString( TCollection<E> c ) {
        if ( c.isEmpty() )
            return "{}";

        CollectionStringBuilder<E> builder =
                new CollectionStringBuilder<E>( c );
        c.forEach( builder );
        return builder.toString();
    }


    private static class CollectionStringBuilder<E> implements Consumer<E> {
        StringBuilder sb = new StringBuilder().append( '{' );
        TCollection<E> target;
        boolean first = true;

        public CollectionStringBuilder( TCollection<E> c ) {
            target = c;
        }

        public void accept( E object ) {
            if (first) first = false;
            else sb.append( ',' ).append( ' ' );
            sb.append( object == target ? "(this collection)" : object );
        }

        public String toString() {
            return sb.append( '}' ).toString();
        }
    }

    ////////////////////////////
    // Sets


    public static boolean setEquals( @NotNull TSet<?> set, Object obj ) {
        if ( set == obj )
            return true;
        if ( !( obj instanceof Set ) ) {
            return false;
        }
        Set<?> another = ( Set<?> ) obj;
        if ( another.size() != set.size() )
            return false;
        try {
            return set.containsAll( another );
        } catch ( ClassCastException e ) {
            return false;
        } catch ( NullPointerException e ) {
            return false;
        }
    }

}
