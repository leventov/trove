///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001, Eric D. Friedman All Rights Reserved.
// Copyright (c) 2009, Rob Eden All Rights Reserved.
// Copyright (c) 2009, Jeff Randall All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
///////////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.Consumer;
import gnu.trove.impl.Collections;
import gnu.trove.function.Predicate;
import gnu.trove.impl.ReverseCollectionOps;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


public class DHashObjSet<E> extends ObjSetDHash<E>
        implements HashObjSet<E>, ReverseCollectionOps<E> {


    public DHashObjSet() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashObjSet( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashObjSet( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashObjSet( Sequence<? extends E> sequence ) {
        this( sequence, sequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashObjSet( Sequence<? extends E> sequence,
            int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
        if ( sequence instanceof ReverseCollectionOps ) {
            //noinspection unchecked
            ( ( ReverseCollectionOps ) sequence ).reverseAddAllTo( this );
        } else {
            sequence.forEach( new Consumer<E>() {
                @Override
                public void accept( E e ) {
                    DHashObjSet.this.add( e );
                }
            });
        }
    }


    protected boolean elementsEquals( @NotNull E a, @Nullable E b ) {
        return a.equals( b );
    }


    @Override
    final boolean equals( @NotNull E a, @Nullable E b ) {
        return elementsEquals( a, b );
    }


    protected int elementHashCode( @NotNull E e ) {
        return e.hashCode();
    }


    @Override
    final int hashCode( @NotNull E obj ) {
        return elementHashCode( obj );
    }


    public boolean equals(Object other) {
        return Collections.setEquals( this, other );
    }


    public int hashCode() {
        return HashBlocks.setHashCode( this );
    }


    @Override
    @NotNull
    public Object[] toArray() {
        return HashBlocks.toArray( this, set );
    }


    @Override
    @NotNull
    public <T> T[] toArray( @NotNull T[] a) {
        return HashBlocks.toArray( this, set, a );
    }


    @Override
    public boolean add( E e ) {
        return insert( e ) < 0;
    }


    @Override
    public boolean remove( Object obj ) {
        int index = index( obj );
        if ( index >= 0 ) {
            removeAt( index );
            return true;
        }
        return false;
    }


    @Override
    public boolean contains( Object o ) {
        return index( o ) >= 0;
    }


    @Override
    @NotNull
    public TIterator<E> iterator() {
        if ( noRemoved() ) {
            return new ObjHashNoRemovedIterator();
        } else {
            return new ObjHashIterator();
        }
    }


    @Override
    public void forEach( Consumer<? super E> action ) {
        //noinspection unchecked
        HashBlocks.forEach( this, ( E[] ) set, action );
    }


    @Override
    public boolean testWhile( Predicate<? super E> predicate ) {
        //noinspection unchecked
        return HashBlocks.testWhile( this, ( E[] ) set, predicate );
    }


    @Override
    public boolean containsAll( @NotNull Collection<?> c ) {
        return HashBlocks.containsAll( this, c );
    }


    @Override
    public boolean allContainingIn( TCollection<? super E> collection ) {
        //noinspection unchecked
        return HashBlocks.reverseAllContainingIn( this, ( E[] ) set, collection );
    }


    @Override
    public boolean addAll( @NotNull Collection<? extends  E> c ) {
        if ( this == c )
            throw new IllegalArgumentException();
        ensureCapacity( c.size() );
        if ( c instanceof ReverseCollectionOps ) {
            //noinspection unchecked
            return ( ( ReverseCollectionOps ) c ).reverseAddAllTo( this );
        } else {
            return Collections.addAll( this, c );
        }
    }


    @Override
    public boolean reverseAddAllTo( TCollection<? super E> collection ) {
        //noinspection unchecked
        return HashBlocks.reverseAddAll( this, ( E[] ) set, collection );
    }


    @Override
    public boolean reverseRemoveAllFrom( TSet<? super E> s ) {
        //noinspection unchecked
        return HashBlocks.reverseRemoveAll( this, ( E[] ) set, s );
    }


    @Override
    public boolean removeIf( Predicate<? super E> filter ) {
        //noinspection unchecked
        return HashBlocks.removeIf( this, ( E[] ) set, filter );
    }


    @Override
    public boolean removeAll( @NotNull Collection<?> c ) {
        return HashBlocks.removeAll( this, this, set, c );
    }


    @Override
    public boolean retainAll( @NotNull Collection<?> c ) {
        return HashBlocks.retainAll( this, this, set, c );
    }


    public String toString() {
        return Collections.toString( this );
    }

} // DHashObjSet
