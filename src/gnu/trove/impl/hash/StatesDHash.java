// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import java.util.Arrays;


public abstract class StatesDHash extends DHash {

    static final byte FREE = 0;
    static final byte FULL = -1;
    static final byte REMOVED = 1;


    static boolean isFull( byte state ) {
        return state < 0;
    }


    byte[] states;


    @Override
    void init( int capacity ) {
        super.init( capacity );
        states = new byte[ capacity ];
        // FREE is 0, no need to do this:
        // Arrays.fill( states, FREE );
    }


    @Override
    public final int capacity() {
        return states.length;
    }


    @Override
    void removeAt( int index ) {
        states[ index ] = REMOVED;
        postRemoveHook();
    }


    @Override
    public void clear() {
        super.clear();
        Arrays.fill( states, FREE );
    }


    abstract class StatesIterator extends HashIterator {

        protected final byte[] states = StatesDHash.this.states;


        protected StatesIterator() {
            findFirstIndex();
        }

        @Override
        protected int findNext( int current ) {
            byte[] states = this.states;
            //noinspection StatementWithEmptyBody
            while ( current-- > 0 && states[ current ] >= 0 ) // FREE or REMOVED
                ;
            return current;
        }
    }

} // StatesDHash
