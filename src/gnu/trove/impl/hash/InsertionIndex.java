// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;


/**
 * Low-level insertion methods of open-addressing hash tables
 * should return index along with slot type:
 * 1) the same element is already present in the hash
 * 2) free slot
 * 3) "removed" slot
 *
 * There are several ways to pass this information to higher-level methods:
 *
 *   - Member boolean field + "-index - 1" hack. Allows to encode even 4 states,
 *     we need 3. Overhead: index arithmetic, 1 field IO, + 0-8 bytes to every
 *     hash table (most likely 0).
 *
 *   - "Tuple" class (this one). Overhead before escape analysis and scalarization:
 *     object allocation, 2 fields IO.
 *     After scalarization: theoretically, almost no overhead.
 */
public final class InsertionIndex {
    private static final int FREE = 0;
    private static final int EXISTING = -1;
    private static final int REMOVED = 1;

    public static InsertionIndex free( int index ) {
        return new InsertionIndex( index, FREE );
    }

    public static InsertionIndex existing( int index ) {
        return new InsertionIndex( index, EXISTING );
    }

    public static InsertionIndex removed( int index ) {
        return new InsertionIndex( index, REMOVED );
    }

    private final int index;
    private final int type;

    InsertionIndex( int index, int type ) {
        this.index = index;
        this.type = type;
    }

    public int get() {
        return index;
    }

    public boolean existing() {
        return type < 0;
    }

    public boolean absent() {
        return type >= 0;
    }

    public boolean freeSlot() {
        return type == FREE;
    }
}
