// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import java.util.ArrayList;
import java.util.BitSet;

import static java.lang.Math.abs;

/**
 * @see DHash#REGULAR_CAPACITIES
 */
final class DHashCapacities {

    private DHashCapacities() {}

    public static void main( String[] args ) {
        int[] a = generateRegularCapacities( 0.005 );
        for ( int i = 0; i < a.length; i += 10 ) {
            for ( int j = i; j < Math.min( i + 10, a.length ); j++ ) {
                System.out.print( a[ j ] + ", " );
            }
            System.out.println();
        }
    }

    /**
     * Searches for high of twin primes, not very close to powers of 2.
     *
     * While possible, keeps max difference between neighbouring capacities
     * not greater than {@code maxDiff}: cap / next cap > 1 - maxDiff.
     *
     * @param maxDiff maximum difference between neighbouring capacities,
     *                keeping while possible
     * @return array containing generated capacities in ascending order
     */
    public static int[] generateRegularCapacities( double maxDiff ) {
        int avoidBitsAroundPowersOf2 = 1;
        while ( ( 1.0 / ( 1 << avoidBitsAroundPowersOf2 ) ) * 2 > maxDiff )
            avoidBitsAroundPowersOf2++;

        BitSet primes = new BitSet( Integer.MAX_VALUE );
        primes.set( 0, Integer.MAX_VALUE );
        sieve( primes );

        ArrayList<Integer> capacities = new ArrayList<Integer>();
        int highPrime;
        for ( int i = Integer.MAX_VALUE - 1; ; i-- ) {
            if ( primes.get( i ) && primes.get( i - 2 ) ) {
                highPrime = i;
                capacities.add( highPrime );
                break;
            }
        }
        int highTwinCount = 0, highTwinNearPowerOf2Count = 0, simplePrimeCount = 0;
        for (; ; ) {
            int minPrevPrime = ( ( int ) ( ( 1 - maxDiff ) * highPrime ) ) + 1;
            int lowestSimplePrime = 0;
            int highTwinNearToPowerOf2 = 0;
            int highTwinPrime = 0;
            for ( int i = minPrevPrime; i < highPrime; i++ ) {
                if ( primes.get( i ) ) {
                    if ( lowestSimplePrime == 0 ) lowestSimplePrime = i;
                    if ( primes.get( i - 2 ) ) {
                        int lowerPowerOf2 = Integer.highestOneBit( i );
                        int avoidDiffWithPowerOf2 = lowerPowerOf2 >> avoidBitsAroundPowersOf2;
                        if ( abs( lowerPowerOf2 - i ) < avoidDiffWithPowerOf2 ) {
                            if ( highTwinNearToPowerOf2 == 0 ||
                                    abs( lowerPowerOf2 - i ) > abs( lowerPowerOf2 - highTwinNearToPowerOf2 ) ) {
                                highTwinNearToPowerOf2 = i;
                            }
                            continue;
                        }
                        int higherPowerOf2 = lowerPowerOf2 << 1;
                        avoidDiffWithPowerOf2 = higherPowerOf2 >> avoidBitsAroundPowersOf2;
                        if ( abs( higherPowerOf2 - i ) < avoidDiffWithPowerOf2 ) {
                            if ( highTwinNearToPowerOf2 == 0 ||
                                    abs( higherPowerOf2 - i ) > abs( higherPowerOf2 - highTwinNearToPowerOf2 ) ) {
                                highTwinNearToPowerOf2 = i;
                            }
                            continue;
                        }
                        highTwinPrime = i;
                        break;
                    }
                }
            }
            int prevPrime = highTwinPrime;
            if ( prevPrime != 0 ) {
                highTwinCount++;
            } else {
                prevPrime = highTwinNearToPowerOf2;
                if ( prevPrime != 0 ) {
                    highTwinNearPowerOf2Count++;
                } else {
                    prevPrime = lowestSimplePrime;
                    if ( prevPrime != 0 ) {
                        simplePrimeCount++;
                    } else {
                        break;
                    }
                }
            }
            capacities.add( prevPrime );
            highPrime = prevPrime;
        }
        System.out.println( "highTwinCount: " + highTwinCount );
        System.out.println( "highTwinNearPowerOf2Count: " + highTwinNearPowerOf2Count );
        System.out.println( "simplePrimeCount: " + simplePrimeCount );
        System.out.println( "Lowest diff prime: " + highPrime );
        // Differences between nearest primes becomes greater than maxDiff.
        for ( int i = highPrime - 1; i > 2; i-- ) {
            if ( primes.get( i ) && !primes.get( i + 2 ) )
                capacities.add( i );
        }

        int[] res = new int[ capacities.size() ];
        for ( int i = capacities.size() - 1, j = 0; i >= 0; i-- ) {
            res[ j++ ] = capacities.get( i );
        }
        return res;
    }


    private static void sieve( BitSet fullSet ) {
        int n = fullSet.size();
        for ( int i = 2; i * i - n < 0; i++ ) {
            if ( fullSet.get( i ) ) {
                for ( int m = i * i; m - n < 0; m += i ) {
                    fullSet.clear( m );
                }
            }
        }
    }
}
