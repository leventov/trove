// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.function.Function;
import gnu.trove.impl.*;
import gnu.trove.map.ObjObjEntrySequence;
import gnu.trove.map.MapIterator;
import gnu.trove.map.TMap;
import gnu.trove.map.hash.HashObjObjMap;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


//TODO doc
public class DHashObjObjMap<K, V>
        extends ObjObjDHash<K, V>
        implements HashObjObjMap<K, V>, ReverseMapOps<K, V> {

    /////////////////////////////
    // Constructors

    public DHashObjObjMap() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashObjObjMap( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashObjObjMap( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashObjObjMap( ObjObjEntrySequence<? extends K, ? extends V> entrySequence ) {
        this( entrySequence, entrySequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashObjObjMap( ObjObjEntrySequence<? extends K, ? extends V> entrySequence,
            int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
        if ( entrySequence instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) entrySequence ).reversePutAllTo( this );
        } else {
            entrySequence.forEach( new BiConsumer<K, V>() {
                @Override
                public void accept( K key, V value ) {
                    DHashObjObjMap.this.justPut( key, value );
                }
            } );
        }
    }


    ////////////////////////////////////
    // DHash "chain" methods

    @Override
    public void clear() {
        super.clear();
        Arrays.fill( values, null );
    }


    @Override
    void removeAt( int index ) {
        values[ index ] = null;
        super.removeAt( index );
    }


    //////////////////////////////////
    // Public operations


    protected boolean valuesEquals( @NotNull V a, @Nullable V b ) {
        return a.equals( b );
    }


    final boolean nullableValuesEquals( @Nullable V a, @Nullable V b ) {
        return a == b || ( a != null && valuesEquals( a, b ) );
    }


    protected int valueHashCode( @NotNull V value ) {
        return value.hashCode();
    }


    final int nullableValueHashCode( @Nullable V value ) {
        return value != null ? valueHashCode( value ) : 0;
    }


    @Override
    public V put( K key, V value ) {
        int index = insert( key, value );
        if ( index < 0 ) {
            return null;
        } else {
            V previous = values[ index ];
            values[ index ] = value;
            return previous;
        }
    }


    @Override
    public V putIfAbsent( K key, V value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            if (oldValue != null) {
                return oldValue;
            } else {
                values[ index ] = value;
                return null;
            }
        } else {
            return null;
        }
    }


    @Override
    public void justPut( K key, V value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            values[ index ] = value;
        }
    }


    @Override
    public V compute( K key,
            BiFunction<? super K, ? super V, ? extends V> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            V newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            V newValue = remappingFunction.apply( key, null );
            if ( newValue != null ) {
                insertAt( insertionIndex, key, newValue );
                return newValue;
            } else {
                return null;
            }
        }
    }


    @Override
    public V computeIfPresent( K key,
            BiFunction<? super K, ? super V, ? extends V> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply( key, oldValue );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            }
        }
        return null;
    }


    @Override
    public void forEach( BiConsumer<? super K, ? super V> action ) {
        if ( isEmpty() )
            return;
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        V[] values = this.values;
        int mc = modCount();
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public V computeIfAbsent( K key,
            Function<? super K, ? extends V> mappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.absent() ) {
            V newValue = mappingFunction.apply( key );
            if ( newValue != null ) {
                insertAt( insertionIndex, key, newValue );
                return newValue;
            } else {
                return null;
            }
        } else {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                return oldValue;
            } else {
                V newValue = mappingFunction.apply( key );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    return null;
                }
            }
        }
    }


    @Override
    public V merge( K key, V value,
            BiFunction<? super V, ? super V, ? extends V> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply(oldValue, value);
                if (newValue != null) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            } else {
                if ( value != null ) {
                    values[ index ] = value;
                    return value;
                } else {
                    return null;
                }
            }
        } else {
            if ( value != null ) {
                insertAt( insertionIndex, key, value );
                return value;
            } else {
                return null;
            }
        }
    }


    @Override
    public final void putAll( @NotNull Map<? extends K, ? extends V> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        ensureCapacity( map.size() );
        if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) map ).reversePutAllTo( this );
        } else {
            Maps.putAll( this, map );
        }
    }


    @Override
    public final boolean containsAllEntries( Map<? extends K, ? extends V> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        if ( map instanceof ReverseMapOps ) {
            // noinspection unchecked
            return ( ( ReverseMapOps ) map ).allEntriesContainingIn( this );
        } else {
            return Maps.containsAllEntries( this, map );
        }
    }


    @Override
    public boolean allEntriesContainingIn( TMap<? super K, ? super V> map ) {
        if ( isEmpty() )
            return true;
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        V[] vals = values;
        int mc = modCount();
        boolean containsAll = true;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    @NotNull
    public MapIterator<K, V> mapIterator() {
        if ( noRemoved() ) {
            return new NoRemovedIteratorImpl();
        } else {
            return new IteratorImpl();
        }
    }


    @Override
    protected boolean keysEquals( @NotNull K a, @Nullable K b ) {
        return a.equals( b );
    }


    @Override
    protected int keyHashCode( @NotNull K key ) {
        return key.hashCode();
    }


    public int hashCode() {
        if ( isEmpty() )
            return 0;
        Object[] keys = set;
        V[] vals = values;
        int mc = modCount();
        int hashCode = 0;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) ) {
                    // noinspection unchecked
                    hashCode += nullableKeyHashCode( ( K ) keys[ i ] ) ^
                            nullableValueHashCode( vals[ i ] );
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) ) {
                    // noinspection unchecked
                    hashCode += nullableKeyHashCode( ( K ) keys[ i ] ) ^
                            nullableValueHashCode( vals[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }


    private class IteratorImpl extends ObjKeyHashIterator
            implements MapIterator<K, V> {

        protected final V[] values = DHashObjObjMap.this.values;


        public V next() {
            advance();
            return values[ index ];
        }


        @Override
        public V value() {
            return values[ index ];
        }


        @Override
        public void setValue( V value ) {
            values[ index ] = value;
        }
    }


    private class NoRemovedIteratorImpl extends ObjKeyHashNoRemovedIterator
            implements MapIterator<K, V> {

        protected final V[] values = DHashObjObjMap.this.values;


        public V next() {
            advance();
            return values[ index ];
        }


        @Override
        public V value() {
            return values[ index ];
        }


        @Override
        public void setValue( V value ) {
            values[ index ] = value;
        }
    }


    @Override
    public void replaceAll(
            BiFunction<? super K, ? super V, ? extends V> function ) {
        if ( isEmpty() )
            return;
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        V[] vals = values;
        int mc = modCount();
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) ) {
                    // noinspection unchecked
                    vals[ i ] = function.apply( keys[ i ], vals[ i ] );
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) ) {
                    // noinspection unchecked
                    vals[ i ] = function.apply( keys[ i ], vals[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public V get( Object key ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : null;
    }


    @Override
    public V getOrDefault( Object key, V defaultValue ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : defaultValue;
    }


    @Override
    public V remove( Object key ) {
        int index = index( key );
        if ( index >= 0 ) {
            V previous = values[index];
            removeAt( index );
            return previous;
        } else {
            return null;
        }
    }


    @Override
    @SuppressWarnings("unchecked")
    public boolean remove( Object key, Object value ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( nullableValuesEquals( values[ index ], ( V ) value ) ) {
                removeAt( index );
                return true;
            }
        }
        return false;
    }


    @Override
    public V replace( K key, V value ) {
        int index = index( key );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        }
        return null;
    }


    @Override
    public boolean replace( K key, V oldValue, V newValue ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( nullableValuesEquals( values[ index ], oldValue ) ) {
                values[ index ] = newValue;
                return true;
            }
        }
        return false;
    }


    @Override
    @NotNull
    public HashCollection<V> values() {
        return new ValueView();
    }


    @Override
    @NotNull
    public HashObjSet<Map.Entry<K, V>> entrySet() {
        return new EntryView();
    }


    @Override
    public boolean containsValue( Object value ) {
        return valueIndex( value ) >= 0;
    }


    @SuppressWarnings("unchecked")
    private int valueIndex( Object value ) {
        if ( isEmpty() )
            return -1;
        Object[] keys = set;
        V[] values = this.values;
        int mc = modCount();
        int index = -1;
        // special case null values so that we don't have to
        // perform null checks before every call to equals()
        if ( value != null ) {
            V v = ( V ) value;
            if ( noRemoved() ) {
                for (int i = values.length; i-- > 0;) {
                    if ( ( notFree( keys[ i ] ) ) &&
                            ( valuesEquals( v, values[ i ] ) ) ) {
                        index = i;
                        break;
                    }
                }
            } else {
                for (int i = values.length; i-- > 0;) {
                    if ( ( isFull( keys[ i ] ) ) &&
                            ( valuesEquals( v, values[ i ] ) ) ) {
                        index = i;
                        break;
                    }
                }
            }
        }
        else {
            if ( noRemoved() ) {
                for (int i = values.length; i-- > 0;) {
                    if ( notFree( keys[ i ] ) && values[ i ] == null ) {
                        index = i;
                        break;
                    }
                }
            } else {
                for (int i = values.length; i-- > 0;) {
                    if ( isFull( keys[ i ] ) && values[ i ] == null ) {
                        index = i;
                        break;
                    }
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return index;
    }


    @Override
    @SuppressWarnings("unchecked")
    public boolean containsEntry( K key, V value ) {
        int index = index( key );
        return index >= 0 && nullableValuesEquals( values[ index ], value );
    }


    @Override
    public void reversePutAllTo( TMap<? super K, ? super V> map ) {
        if ( isEmpty() )
            return;
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        V[] vals = this.values;
        int mc = modCount();
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean testWhile( BiPredicate<? super K, ? super V> predicate ) {
        if ( isEmpty() )
            return true;
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        V[] vals = values;
        int mc = modCount();
        boolean terminated = false;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !predicate.test( keys[ i ], vals[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !predicate.test( keys[ i ], vals[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public boolean removeIf( BiPredicate<? super K, ? super V> filter ) {
        if ( isEmpty() )
            return false;
        Object[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean changed = false;
        if ( noRemoved() ) {
            for( int i = keys.length; i-- > 0; ) {
                // noinspection unchecked
                if ( notFree( keys[ i ] ) &&
                        filter.test( ( K ) keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = keys.length; i-- > 0; ) {
                // noinspection unchecked
                if ( isFull( keys[ i ] ) &&
                        filter.test( ( K ) keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    /**
     * a view onto the values of the map.
     */
    private class ValueView extends View<V> {

        @Override
        public boolean removeIf( Predicate<? super V> filter ) {
            return HashBlocks.removeIf( DHashObjObjMap.this, values, filter );
        }


        @Override
        public boolean allContainingIn( TCollection<? super V> collection ) {
            return HashBlocks.reverseAllContainingIn(
                    DHashObjObjMap.this, values, collection );
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super V> collection ) {
            return HashBlocks.reverseAddAll( DHashObjObjMap.this, values, collection );
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super V> s ) {
            return HashBlocks.reverseRemoveAll( DHashObjObjMap.this, values, s );
        }


        @Override
        @NotNull
        public TIterator<V> iterator() {
            if ( noRemoved() ) {
                return new NoRemovedValueIterator();
            } else {
                return new ValueIterator();
            }
        }


        @Override
        public void forEach( Consumer<? super V> action ) {
            HashBlocks.forEach( DHashObjObjMap.this, values, action );
        }


        @Override
        public boolean testWhile( Predicate<? super V> predicate ) {
            return HashBlocks.testWhile( DHashObjObjMap.this, values, predicate );
        }


        @Override
        @NotNull
        public Object[] toArray() {
            return HashBlocks.toArray( DHashObjObjMap.this, values );
        }


        @Override
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            return HashBlocks.toArray( DHashObjObjMap.this, values, a );
        }


        @Override
        public boolean contains( Object value ) {
            return containsValue( value );
        }


        @Override
        public boolean remove( Object value ) {
            int index = valueIndex( value );
            if ( index >= 0 ) {
                removeAt( index );
                return true;
            } else {
                return false;
            }
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> c ) {
            return HashBlocks.removeAll( this, DHashObjObjMap.this, values, c );
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> c ) {
            return HashBlocks.retainAll( this, DHashObjObjMap.this, values, c );
        }
    }


    private class ValueIterator extends ObjKeyHashIterator
            implements TIterator<V> {

        protected final V[] values = DHashObjObjMap.this.values;


        @Override
        public V next() {
            advance();
            return values[ index ];
        }


        @Override
        public V value() {
            return values[ index ];
        }
    }

    private class NoRemovedValueIterator extends ObjKeyHashNoRemovedIterator
            implements TIterator<V> {

        protected final V[] values = DHashObjObjMap.this.values;


        @Override
        public V next() {
            advance();
            return values[ index ];
        }


        @Override
        public V value() {
            return values[ index ];
        }
    }


    /**
     * a view onto the entries of the map.
     */
    private class EntryView extends ObjHashBackedEntryView {

        @Override
        @NotNull
        public TIterator<Map.Entry<K, V>> iterator() {
            if ( noRemoved() ) {
                return new EntryNoRemovedIterator();
            } else {
                return new EntryIterator();
            }
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean remove( Object o ) {
            Map.Entry<K, V> e = ( Map.Entry<K, V> ) o;
            int index = index( e.getKey() );
            if ( index >= 0 ) {
                if ( nullableValuesEquals( e.getValue(), values[ index ] ) ) {
                    removeAt( index );
                    return true;
                }
            }
            return false;
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean contains( Object o ) {
            Map.Entry<K, V> e = ( Map.Entry<K, V> ) o;
            int index = index( e.getKey() );
            return index >= 0 &&
                    nullableValuesEquals( values[ index ], e.getValue() );
        }


        @Override
        protected Map.Entry<K, V> entryAt( int index ) {
            return new Entry( index );
        }
    }


    class EntryIterator extends ObjKeyHashIterator
            implements TIterator<Map.Entry<K, V>> {
        private Entry entry;

        @Override
        public Map.Entry<K, V> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<K, V> value() {
            return entry;
        }
    }


    class EntryNoRemovedIterator extends ObjKeyHashNoRemovedIterator
            implements TIterator<Map.Entry<K, V>> {
        private Entry entry;

        @Override
        public Map.Entry<K, V> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<K, V> value() {
            return entry;
        }
    }


    private class Entry extends ObjHashEntry {

        Entry( int index ) {
            super( index );
        }


        @Override
        public V getValue() {
            return values[ index ];
        }


        @Override
        public V setValue( V newValue ) {
            V oldValue = getValue();
            values[ index ] = newValue;
            return oldValue;
        }


        @SuppressWarnings("unchecked")
        public boolean equals( Object o ) {
            if ( o == null )
                return false;
            Map.Entry e2;
            K key2;
            V value2;
            try {
                e2 = ( Map.Entry ) o;
                key2 = ( K ) e2.getKey();
                value2 = ( V ) e2.getValue();
            } catch ( ClassCastException e ) {
                return false;
            }
            int i = index;
            return nullableKeyEquals( ( K ) set[ i ], key2 ) &&
                    nullableValuesEquals( values[ i ], value2 );
        }


        public int hashCode() {
            int i = index;
            //noinspection unchecked
            return nullableKeyHashCode( ( K ) set[ i ] ) ^
                    nullableValueHashCode( values[ i ] );
        }

    }

} // DHashObjObjMap
