// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;


import gnu.trove.TCollection;
import gnu.trove.TIterator;
import gnu.trove.function.*;
import gnu.trove.impl.Collections;
import gnu.trove.impl.Maps;
import gnu.trove.map.TMap;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Array;
import java.util.*;


//TODO doc
public abstract class ObjDHashBackedMap<K, V> extends ObjDHash<K>
        implements TMap<K, V> {

    protected abstract boolean keysEquals( @NotNull K a, @Nullable K b );


    @Override
    final boolean equals( @NotNull K a, @Nullable K b ) {
        return keysEquals( a, b );
    }


    final boolean nullableKeyEquals( @Nullable K a, @Nullable K b ) {
        return a == b || ( a != null && keysEquals( a, b ) );
    }


    protected abstract int keyHashCode( @NotNull K key );


    @Override
    final int hashCode( @NotNull K key ) {
        return keyHashCode( key );
    }


    final int nullableKeyHashCode( @Nullable K key ) {
        return key != null ? keyHashCode( key ) : 0;
    }


    @Override
    public boolean containsKey( Object key ) {
        return index( key ) >= 0;
    }


    @Override
    public boolean justRemove( K key ) {
        int index = index( key );
        if (index >= 0) {
            removeAt( index );
            return true;
        }
        return false;
    }


    public final boolean equals( Object obj ) {
        return Maps.equals( this, obj );
    }


    public abstract int hashCode();


    public String toString() {
        return Maps.toString( this );
    }


    @Override
    @NotNull
    public HashObjSet<K> keySet() {
        return new KeyView();
    }


    private class KeyView extends View<K>
            implements HashObjSet<K> {

        @Override
        @NotNull
        public TIterator<K> iterator() {
            if ( noRemoved() ) {
                return new ObjHashNoRemovedIterator();
            } else {
                return new ObjHashIterator();
            }
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean remove( Object key ) {
            return justRemove( ( K ) key );
        }


        @Override
        public boolean contains( Object key ) {
            return containsKey( key );
        }


        @Override
        public boolean removeIf( Predicate<? super K> filter ) {
            //noinspection unchecked
            return HashBlocks.removeIf(
                    ObjDHashBackedMap.this, ( K[] ) set, filter );
        }


        @Override
        public boolean allContainingIn( TCollection<? super K> collection ) {
            //noinspection unchecked
            return HashBlocks.reverseAllContainingIn(
                    ObjDHashBackedMap.this, ( K[] ) set, collection );
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super K> collection ) {
            //noinspection unchecked
            return HashBlocks.reverseAddAll(
                    ObjDHashBackedMap.this, ( K[] ) set, collection );
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super K> s ) {

            //noinspection unchecked
            return HashBlocks.reverseRemoveAll(
                    ObjDHashBackedMap.this, ( K[] )set, s );
        }


        @Override
        public void forEach( Consumer<? super K> action ) {
            //noinspection unchecked
            HashBlocks.forEach( ObjDHashBackedMap.this, ( K[] ) set, action );
        }


        @Override
        public boolean testWhile( Predicate<? super K> predicate ) {
            //noinspection unchecked
            return HashBlocks.testWhile( ObjDHashBackedMap.this, ( K[] ) set, predicate );
        }


        @Override
        @NotNull
        public Object[] toArray() {
            return HashBlocks.toArray( ObjDHashBackedMap.this, set );
        }


        @Override
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            return HashBlocks.toArray( ObjDHashBackedMap.this, set, a );
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> c ) {
            return HashBlocks.removeAll( this, ObjDHashBackedMap.this, set, c );
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> c ) {
            return HashBlocks.retainAll( this, ObjDHashBackedMap.this, set, c );
        }


        public int hashCode() {
            return HashBlocks.setHashCode( ObjDHashBackedMap.this );
        }


        public boolean equals( Object obj ) {
            return Collections.setEquals( this, obj );
        }
    }


    abstract class ObjHashBackedEntryView
            extends View<Entry<K, V>>
            implements HashObjSet<Entry<K, V>> {

        public final int hashCode() {
            return ObjDHashBackedMap.this.hashCode();
        }


        public boolean equals( Object o ) {
            return Collections.setEquals( this, o );
        }


        protected abstract Map.Entry<K, V> entryAt( int index );


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super Entry<K, V>> s ) {
            if ( this.isEmpty() || s.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= s.remove( entryAt( i ) );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= s.remove( entryAt( i ) );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean removeIf( Predicate<? super Map.Entry<K, V>> filter ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && filter.test( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && filter.test( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean allContainingIn(
                TCollection<? super Map.Entry<K, V>> collection ) {
            if ( isEmpty() )
                return true;
            boolean containsAll = true;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !collection.contains( entryAt( i ) ) ) {
                        containsAll = false;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !collection.contains( entryAt( i ) ) ) {
                        containsAll = false;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return containsAll;
        }


        @Override
        public final boolean reverseAddAllTo(
                TCollection<? super Map.Entry<K, V>> collection ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= collection.add( entryAt( i ) );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= collection.add( entryAt( i ) );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        @NotNull
        public final Object[] toArray() {
            int size = size();
            Object[] result = new Object[ size ];
            if ( size == 0 )
                return result;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        result[ resultIndex++ ] = entryAt( i );
                }
            } else {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        result[ resultIndex++ ] = entryAt( i );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return result;
        }

        @Override
        @SuppressWarnings("unchecked")
        @NotNull
        public final <T> T[] toArray( @NotNull T[] a ) {
            int size = size();
            if ( a.length < size ) {
                Class<?> tType = a.getClass().getComponentType();
                a = ( T[] ) Array.newInstance( tType, size );
            }
            if ( size == 0 ) {
                if ( a.length > 0 )
                    a[ 0 ] = null;
                return a;
            }
            int resultIndex = 0;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for (int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        a[ resultIndex++ ] = ( T ) entryAt( i );
                }
            } else {
                for (int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        a[ resultIndex++ ] = ( T ) entryAt( i );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            if ( a.length > resultIndex ) {
                a[ resultIndex ] = null;
            }
            return a;
        }


        @Override
        public final boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public final boolean removeAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || another.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) &&
                            another.contains( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) &&
                            another.contains( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean retainAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( another.isEmpty() ) {
                if ( this.isEmpty() ) {
                    return false;
                } else {
                    clear();
                    return true;
                }
            }
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !another.contains( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !another.contains( entryAt( i ) ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final void forEach( Consumer<? super Map.Entry<K, V>> action ) {
            if ( isEmpty() )
                return;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        action.accept( entryAt( i ) );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        action.accept( entryAt( i ) );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
        }


        @Override
        public final boolean testWhile( Predicate<? super Map.Entry<K, V>> predicate ) {
            if ( isEmpty() )
                return true;
            boolean terminated = false;
            int mc = modCount();
            Object[] hash = set;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !predicate.test( entryAt( i ) ) ) {
                        terminated = true;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !predicate.test( entryAt( i ) ) ) {
                        terminated = true;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return !terminated;
        }
    }


    abstract class ObjHashEntry implements Map.Entry<K, V> {

        final int index;


        ObjHashEntry( int index ) {
            this.index = index;
        }


        @Override
        public K getKey() {
            //noinspection unchecked
            return ( K ) set[ index ];
        }


        @Override
        public String toString() {
            return getKey() + "=" + getValue();
        }

        public abstract int hashCode();


        public abstract boolean equals( Object obj );
    }
}
