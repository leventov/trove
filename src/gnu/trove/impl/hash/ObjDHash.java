// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.TIterator;
import gnu.trove.impl.Primitives;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;


//TODO doc
public abstract class ObjDHash<K> extends DHash {

    /////////////////////
    // Static utilities

    static final Object REMOVED = new Object(), FREE = new Object();


    static boolean isFull( Object o ) {
        return o != FREE && o != REMOVED;
    }


    static boolean notFree( Object o ) {
        return o != FREE;
    }

    /////////////////////////
    // Fields

    Object[] set;


    /////////////////////////
    // DHash "chain" methods

    @Override
    void init( int capacity ) {
        super.init( capacity );
        set = new Object[ capacity ];
        Arrays.fill( set, FREE );
    }


    @Override
    public final int capacity() {
        return set.length;
    }


    @Override
    void removeAt( int index ) {
        set[ index ] = REMOVED;
        postRemoveHook();
    }


    @Override
    public void clear() {
        super.clear();
        Arrays.fill( set, FREE );
    }


    ///////////////////////
    // Equality


    abstract boolean equals( @NotNull K a, @Nullable K b );


    abstract int hashCode( @NotNull K obj );


    /////////////////////////////////////
    // Index methods

    @SuppressWarnings("unchecked")
    int index( @Nullable Object obj ) {
        if ( obj != null ) {
            K key = ( K ) obj;

            K[] set = ( K[] ) this.set;
            int capacity = set.length;
            // Hash should be positive, because index = hash % capacity
            // should be positive.
            int hash = hashCode( key ) & Primitives.DROP_INT_SIGN_BIT;
            int index = hash % capacity; // firstIndex
            K cur = set[ index ];
            // In index operation, we suppose target object is most likely
            // already stored in the hash. Most probable branch goes first:
            if ( cur == key ) {
                return index;
            }
            else if ( cur == FREE ) {
                return -1;
            }
            // Calling equals() call after REMOVED and FREE ensures it indeed
            // has the key type - K.
            else if ( cur != REMOVED && equals( key, cur ) ) {
                return index;
            }
            else {
                return indexProbingLoop( key, set, hash, capacity, index );
            }
        }
        else {
            return nullIndex();
        }
    }


    private int indexProbingLoop( K key, K[] set,
                                  int hash, int capacity, int index ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == key ) {
                    return index;
                }
                else if ( cur == FREE ) {
                    return -1;
                }
                else if ( equals( key, cur ) ) {
                    return index;
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == key ) {
                    return index;
                }
                else if ( cur == FREE ) {
                    return -1;
                }
                else if ( cur != REMOVED && equals( key, cur ) ) {
                    return index;
                }
            }
        }
    }


    private int nullIndex() {
        Object[] set = this.set;
        for ( int index = 0; ; index++ ) {
            Object cur = set[ index ];
            if ( cur == null ) return index;
            else if ( cur == FREE ) return -1;
        }
    }


    @SuppressWarnings("unchecked")
    InsertionIndex insertionIndex( @Nullable Object obj ) {
        if ( obj != null ) {
            K key = ( K ) obj;

            K[] set = ( K[] ) this.set;
            int capacity = set.length;
            // Hash should be positive, because index = hash % capacity
            // should be positive.
            int hash = hashCode( key ) & Primitives.DROP_INT_SIGN_BIT;
            int index = hash % capacity; // firstIndex
            K cur = set[ index ];
            // In index operation, we suppose target object is most likely
            // already stored in the hash. Most probable branch goes first:
            if ( cur == key ) {
                return InsertionIndex.existing( index );
            }
            else if ( cur == FREE ) {
                return InsertionIndex.free( index );
            }
            else {
                int firstRemoved;
                if ( cur != REMOVED ) {
                    if ( equals( key, cur ) )
                        return InsertionIndex.existing( index );
                    firstRemoved = -1;
                } else {
                    firstRemoved = index;
                }
                return insertionIndexProbingLoop(
                        key, set, hash, capacity, index, firstRemoved );
            }
        }
        else {
            return nullInsertionIndex();
        }
    }


    private InsertionIndex insertionIndexProbingLoop( K key, K[] set,
            int hash, int capacity, int index, int firstRemoved ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( firstRemoved < 0 && noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == key ) {
                    return InsertionIndex.existing( index );
                }
                else if ( cur == FREE ) {
                    return InsertionIndex.free( index );
                }
                else if ( equals( key, cur ) ) {
                    return InsertionIndex.existing( index );
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == key ) {
                    return InsertionIndex.existing( index );
                }
                else if ( cur == FREE ) {
                    if ( firstRemoved < 0 ) return InsertionIndex.free( index );
                    else return InsertionIndex.removed( firstRemoved );
                }
                else if ( cur != REMOVED ) {
                    if ( equals( key, cur ) ) {
                        return InsertionIndex.existing( index );
                    }
                }
                // cur is REMOVED
                else if ( firstRemoved < 0 ) {
                    firstRemoved = index;
                }
            }
        }
    }


    private InsertionIndex nullInsertionIndex() {
        // noinspection unchecked
        Object[] set = this.set;
        int firstRemoved = -1;
        for ( int index = 0; ; index++ ) {
            Object cur = set[ index ];
            if ( cur == null ) {
                return InsertionIndex.existing( index );
            }
            else if ( cur == FREE ) {
                if ( firstRemoved < 0 ) return InsertionIndex.free( index );
                else return InsertionIndex.removed( firstRemoved );
            }
            else if ( cur == REMOVED && firstRemoved < 0 ) {
                firstRemoved = index;
            }
        }
    }

    abstract class ObjKeyHashIterator extends HashIterator {

        protected final Object[] set = ObjDHash.this.set;


        protected ObjKeyHashIterator() {
            findFirstIndex();
        }

        @Override
        protected int findNext( int current ) {
            Object[] set = this.set;
            //noinspection StatementWithEmptyBody
            while ( current-- > 0 && !isFull( set[ current ] ) )
                ;
            return current;
        }


        public K key() {
            //noinspection unchecked
            return ( K ) set[ index ];
        }

    }


    final class ObjHashIterator extends ObjKeyHashIterator
            implements TIterator<K> {

        public ObjHashIterator() {}

        @Override
        public K next() {
            advance();
            //noinspection unchecked
            return ( K ) set[ index ];
        }

        @Override
        public K value() {
            //noinspection unchecked
            return ( K ) set[ index ];
        }

    }


    abstract class ObjKeyHashNoRemovedIterator extends HashIterator {

        protected final Object[] set = ObjDHash.this.set;


        protected ObjKeyHashNoRemovedIterator() {
            findFirstIndex();
        }

        @Override
        protected int findNext( int current ) {
            Object[] set = this.set;
            //noinspection StatementWithEmptyBody
            while ( current-- > 0 && set[ current ] == FREE )
                ;
            return current;
        }


        public K key() {
            //noinspection unchecked
            return ( K ) set[ index ];
        }
    }


    class ObjHashNoRemovedIterator extends ObjKeyHashNoRemovedIterator
       implements TIterator<K> {

        public ObjHashNoRemovedIterator() {}

        @Override
        public K next() {
            advance();
            //noinspection unchecked
            return ( K ) set[ index ];
        }

        @Override
        public K value() {
            //noinspection unchecked
            return ( K ) set[ index ];
        }

    }
}
