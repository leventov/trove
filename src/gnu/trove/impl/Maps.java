// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl;

import gnu.trove.function.BiConsumer;
import gnu.trove.map.TMap;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

public final class Maps {

    private Maps() {}


    public static <K, V> boolean containsAllEntries( TMap<K, V> map,
            Map<? extends K, ? extends V> another) {
        for ( Map.Entry<? extends K, ? extends V> e : another.entrySet() ) {
            if ( !map.containsEntry( e.getKey(), e.getValue() ) )
                return false;
        }
        return true;
    }


    @SuppressWarnings("unchecked")
    public static boolean equals( @NotNull TMap map, Object obj ) {
        if ( map == obj )
            return true;
        if ( !( obj instanceof Map ) ) {
            return false;
        }
        Map<?, ?> that = ( Map<?, ?> ) obj;
        if ( that.size() != map.size() )
            return false;
        try {
            return map.containsAllEntries( that );
        } catch ( ClassCastException e ) {
            return false;
        } catch ( NullPointerException e ) {
            return false;
        }
    }


    public static <K, V> void putAll( TMap<K, V> map,
            Map<? extends K, ? extends V> another ) {
        for ( Map.Entry<? extends K, ? extends V> e : another.entrySet() ) {
            map.justPut( e.getKey(), e.getValue() );
        }
    }


    public static <K, V> String toString( TMap<K, V> map ) {
        if ( map.isEmpty() )
            return "{}";

        MapStringBuilder<K, V> builder = new MapStringBuilder<K, V>( map );
        map.forEach( builder );
        return builder.toString();
    }


    private static class MapStringBuilder<K, V>
            implements BiConsumer<K, V> {
        StringBuilder sb = new StringBuilder().append( '{' );
        TMap<K, V> target;
        boolean first = true;

        public MapStringBuilder( TMap<K, V> map ) {
            target = map;
        }

        public void accept( K key, V value ) {
            if (first) first = false;
            else sb.append( ',' ).append( ' ' );
            sb.append( key == target ? "(this collection)" : key );
            sb.append( '=' );
            sb.append( value == target ? "(this collection)" : value );
        }


        public String toString() {
            return sb.append( '}' ).toString();
        }
    }
}
