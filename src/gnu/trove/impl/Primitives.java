// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl;


public final class Primitives {

    public static final int DROP_INT_SIGN_BIT = -1 >>> 1;
    private static final int BYTE_MASK = ( 1 << 8 ) - 1;
    private static final int SHORT_MASK = ( 1 << 16 ) - 1;


    private Primitives() {}


    public static int positiveHash( double value ) {
        // We shouldn't collapse all NaN's to a single "canonical" hash code,
        // because NaN != NaN
        long bits = Double.doubleToRawLongBits( value );
        // We should drop double sign, because -0.0 == 0.0
        return ( int ) ( bits ^ ( bits >>> 32 ) ) & DROP_INT_SIGN_BIT;
    }


    public static int positiveHash( float value ) {
        int bits = Float.floatToRawIntBits( value );
        return bits & DROP_INT_SIGN_BIT;
    }


    public static int positiveHash( long value ) {
        return ( int ) ( value ^ ( value >>> 32 ) ) & DROP_INT_SIGN_BIT;
    }


    public static int positiveHash( int value ) {
        return value & DROP_INT_SIGN_BIT;
    }


    public static int positiveHash( char value ) {
        // char is always positive
        return value;
    }


    public static int positiveHash( short value ) {
        // unsigned widening
        return value & SHORT_MASK;
    }


    public static int positiveHash( byte value ) {
        // unsigned widening
        return value & BYTE_MASK;
    }


    // Hash codes for equality operator (==)

    public static int identityHashCode( double value ) {
        long bits = Double.doubleToRawLongBits( value );
        // We should drop double sign, because -0.0 == 0.0
        return ( int ) ( bits ^ ( bits >>> 32 ) ) & DROP_INT_SIGN_BIT;
    }


    public static int identityHashCode( float value ) {
        return Float.floatToRawIntBits( value ) & DROP_INT_SIGN_BIT;
    }


    // Nothing special for integral types

    public static int identityHashCode( long value ) {
        return ( int ) ( value ^ ( value >>> 32 ) );
    }


    public static int identityHashCode( int value ) {
        return value;
    }


    public static int identityHashCode( char value ) {
        return value;
    }


    public static int identityHashCode( short value ) {
        return value;
    }


    public static int identityHashCode( byte value ) {
        return value;
    }

}
