// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove;


/**
 * Common interface of collections and maps, based on hash tables.
 */
public interface AbstractHash {

    /**
     * Returns the size of internal table (one or more arrays),
     * representing the hash.
     *
     * <p>It is NOT equal to the maximum size of the collection,
     * which could reached without expansion. The maximum size
     * is near to {@code capacity() * loadFactor()}.
     *
     * @see #size();
     * @see #loadFactor()
     */
    int capacity();


    /**
     * Returns the current number of elements (map entries, etc.) in the hash.
     */
    int size();


    /**
     * Returns the load factor of the hash. Load factor determines how full
     * the internal table can become before expansion is required.
     * Load factor must be a value in (0.0, 1.0) range.
     */
    float loadFactor();


    /**
     * Returns fullness of the internal tables in {@link #loadFactor()}
     * dimension. If current load reaches load factor of the hash, expansion
     * is triggered
     * @return fullness of the hash
     */
    float currentLoad();


    /**
     * Prepares hash for inserting {@code additionalSize} new elements without
     * excessive rehashes. Calling this method is a hint, but not a strict
     * guarantee that the next {@code additionalSize} insertions will be done
     * in real time.
     * @param additionalSize the number of additional elements that will be
     *                       inserted in the hash soon
     * @return {@code true} if rehash was actually performed to ensure capacity,
     *         and the next {@code additionalSize} insertions won't cause rehash
     *         for sure.
     */
    boolean ensureCapacity( int additionalSize );


    /**
     * If {@link #currentLoad()} is less than {@link #loadFactor()},
     * compaction is performed to fix this.
     * @return {@code true} if the hash was actually shrunk
     */
    boolean shrink();
}
