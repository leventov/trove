// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map.hash;


import gnu.trove.HashCollection;
import gnu.trove.map.TMap;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;


public interface HashObjObjMap<K, V> extends TMap<K, V> {

    @Override
    @NotNull
    HashObjSet<K> keySet();


    @Override
    @NotNull
    HashCollection<V> values();


    @Override
    @NotNull
    HashObjSet<Entry<K, V>> entrySet();
}
