// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map;


//TODO doc
public interface MapIterator<K, V> {

    /**
     * Returns {@code true} if the iteration has more entries.
     *
     * @return {@code true} if the iteration has more entries
     */
    boolean hasNext();


    /**
     * Moves the iterator forward to the next entry, if it exists, and returns
     * {@code true}, returns {@code false} if the iteration has no more entries.
     *
     * @return {@code true} if the iterator has moved forward to the next entry,
     *         {@code false} if the iteration has no more entries
     */
    boolean tryAdvance();


    /**
     * Returns the key of the last entry, passed by the iterator.
     *
     * <p>The result of calling {@code key()} on newly-created iterator prior
     * to {@link #tryAdvance()} is undefined, for example
     * an {@code IndexOutOfBoundsException} could be thrown.
     *
     * @return the key of the last entry, passed by the iterator
     * @see java.util.Map.Entry#getKey()
     */
    K key();


    /**
     * Returns the value of the last entry, passed by the iterator.
     *
     * <p>{@code TIterator} implementations should document specific
     * type of runtime exceptions, thrown if {@code value()} is invoked on
     * newly-created iterator prior to advancing.
     *
     * @return the value of the last entry, passed by the iterator
     * @throws RuntimeException if the {@code value()} method is invoked on
     *         on newly-created iterator prior to calling {@link #tryAdvance()}
     *         at least once
     */
    V value();


    /**
     * Replace the value of the last entry, passed by the iterator, with the
     * specified value (optional operation). Writes through to the map.
     *
     * <p>The behavior of this call is undefined
     * if the entry has already been removed from the map by the iterator's
     * {@link #remove()} operation, or on newly-created iterator prior
     * to {@link #tryAdvance()}, for example
     * an {@code IndexOutOfBoundsException} could be thrown.
     *
     * <p>Unlike {@link java.util.Map.Entry#setValue(Object)}, this method
     * doesn't return the previous value of the entry to allow more
     * efficient implementation. If you need the previous value,
     * call {@link #value()} before setting the new value.
     *
     * @param value the value to set in the current entry
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by the backing map
     * @throws IllegalArgumentException if some property of this value prevents
     *         it from being stored in the backing map
     * @throws IllegalStateException implementations may, but are not required to,
     *         throw this exception if the entry has been removed from the backing map
     * @see java.util.Map.Entry#setValue(Object)
     */
    void setValue( V value );


    /**
     * Removes from the underlying collection the last entry, passed
     * by the iterator (optional operation).
     *
     * <p>The result of invoking this method more than once for a single entry
     * as well as when the iterator hasn't passed any entry yet
     * is undefined and can leave the underlying collection in a confused state.
     * Note the difference with {@link java.util.Iterator#remove()}
     * contract, which guides to throw {@code IllegalStateException} in such situations.
     *
     * @throws UnsupportedOperationException if the {@code remove}
     *         operation is not supported by this iterator
     */
    void remove();
}
