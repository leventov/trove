package gnu.trove.set;

import gnu.trove.TCollection;

import java.util.Set;

//TODO doc
public interface TSet<E> extends TCollection<E>, Set<E> {}
