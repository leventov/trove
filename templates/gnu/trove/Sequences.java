// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove;

import gnu.trove.function.*;
import gnu.trove.map.*;

import java.lang.*;
import java.util.Collection;
import java.util.Map;


public final class Sequences {

    private Sequences() {}


    public static <E> Sequence<E> asSequence( final E... elements ) {
        return new Sequence<E>() {
            @Override
            public int size() {
                return elements.length;
            }

            @Override
            public void forEach( Consumer<? super E> action ) {
                for ( E e : elements ) {
                    action.accept( e );
                }
            }
        };
    }

/* char elem */

    public static CharSequence asCharSequence( final char... elements ) {
        return new CharSequence() {
            @Override
            public int size() {
                return elements.length;
            }

            @Override
            public void forEach( CharConsumer action ) {
                for ( char e : elements ) {
                    action.accept( e );
                }
            }
        };
    }

/* end */


    public static <E> Sequence<E> asSequence( final Collection<E> collection ) {
        if ( collection instanceof Sequence )
            // noinspection unchecked
            return ( Sequence<E> ) collection;
        return new Sequence<E>() {
            @Override
            public int size() {
                return collection.size();
            }

            @Override
            public void forEach( Consumer<? super E> action ) {
                for ( E e : collection ) {
                    action.accept( e );
                }
            }
        };
    }

/* char elem */

    public static CharSequence asCharSequence(
            final Collection<Character> collection ) {
        if ( collection instanceof CharSequence )
            return ( CharSequence ) collection;
        return new CharSequence() {
            @Override
            public int size() {
                return collection.size();
            }

            @Override
            public void forEach( CharConsumer action ) {
                for ( char e : collection ) {
                    action.accept( e );
                }
            }
        };
    }

/* end */


    public static <K, V> ObjObjEntrySequence<K, V> asEntrySequence(
            final Map<K, V> map ) {
        if ( map instanceof ObjObjEntrySequence )
            // noinspection unchecked
            return ( ObjObjEntrySequence<K, V> ) map;
        return new ObjObjEntrySequence<K, V>() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public void forEach( BiConsumer<? super K, ? super V> action ) {
                for ( Map.Entry<K, V> entry : map.entrySet() ) {
                    action.accept( entry.getKey(), entry.getValue() );
                }
            }
        };
    }

/* char elem */

    public static <V> CharObjEntrySequence<V> asCharObjEntrySequence(
            final Map<Character, V> map ) {
        if ( map instanceof CharObjEntrySequence )
            // noinspection unchecked
            return ( CharObjEntrySequence<V> ) map;
        return new CharObjEntrySequence<V>() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public void forEach( CharObjConsumer<? super V> action ) {
                for ( Map.Entry<Character, V> entry : map.entrySet() ) {
                    action.accept( entry.getKey(), entry.getValue() );
                }
            }
        };
    }


    public static <K> ObjCharEntrySequence<K> asObjCharEntrySequence(
            final Map<K, Character> map ) {
        if ( map instanceof ObjCharEntrySequence )
            // noinspection unchecked
            return ( ObjCharEntrySequence<K> ) map;
        return new ObjCharEntrySequence<K>() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public void forEach( ObjCharConsumer<? super K> action ) {
                for ( Map.Entry<K, Character> entry : map.entrySet() ) {
                    action.accept( entry.getKey(), entry.getValue() );
                }
            }
        };
    }

/* end */
/* char key short value */

    public static CharShortEntrySequence asCharShortEntrySequence(
            final Map<Character, Short> map ) {
        if ( map instanceof CharShortEntrySequence )
            // noinspection unchecked
            return ( CharShortEntrySequence ) map;
        return new CharShortEntrySequence() {
            @Override
            public int size() {
                return map.size();
            }

            @Override
            public void forEach( CharShortConsumer action ) {
                for ( Map.Entry<Character, Short> entry : map.entrySet() ) {
                    action.accept( entry.getKey(), entry.getValue() );
                }
            }
        };
    }

/* end */


    public static <K, V> ObjObjEntrySequence<K, V> asEntrySequence(
            final K[] keys, final V[] values ) {
        if ( keys.length != values.length )
            throw new IllegalArgumentException();
        return new ObjObjEntrySequence<K, V>() {
            @Override
            public int size() {
                return keys.length;
            }

            @Override
            public void forEach( BiConsumer<? super K, ? super V> action ) {
                K[] ks = keys;
                V[] vs = values;
                for ( int i = 0; i < ks.length; i++ ) {
                    action.accept( ks[i], vs[i] );
                }
            }
        };
    }

/* char elem */

    public static <V> CharObjEntrySequence<V> asCharObjEntrySequence(
            final char[] keys, final V[] values ) {
        if ( keys.length != values.length )
            throw new IllegalArgumentException();
        return new CharObjEntrySequence<V>() {
            @Override
            public int size() {
                return keys.length;
            }

            @Override
            public void forEach( CharObjConsumer<? super V> action ) {
                char[] ks = keys;
                V[] vs = values;
                for ( int i = 0; i < ks.length; i++ ) {
                    action.accept( ks[i], vs[i] );
                }
            }
        };
    }

    public static <K> ObjCharEntrySequence<K> asObjCharEntrySequence(
            final K[] keys, final char[] values ) {
        if ( keys.length != values.length )
            throw new IllegalArgumentException();
        return new ObjCharEntrySequence<K>() {
            @Override
            public int size() {
                return keys.length;
            }

            @Override
            public void forEach( ObjCharConsumer<? super K> action ) {
                K[] ks = keys;
                char[] vs = values;
                for ( int i = 0; i < ks.length; i++ ) {
                    action.accept( ks[i], vs[i] );
                }
            }
        };
    }

/* end */
/* char key short value */

    public static CharShortEntrySequence asCharShortEntrySequence(
            final char[] keys, final short[] values ) {
        if ( keys.length != values.length )
            throw new IllegalArgumentException();
        return new CharShortEntrySequence() {
            @Override
            public int size() {
                return keys.length;
            }

            @Override
            public void forEach( CharShortConsumer action ) {
                char[] ks = keys;
                short[] vs = values;
                for ( int i = 0; i < ks.length; i++ ) {
                    action.accept( ks[i], vs[i] );
                }
            }
        };
    }

/* end */


    public static <E> Sequence<E> join( final Sequence<E>... sequences ) {
        return new Sequence<E>() {
            @Override
            public int size() {
                int s = 0;
                for ( Sequence<E> sequence : sequences ) {
                    s += sequence.size();
                    if ( s < 0 ) s = Integer.MAX_VALUE;
                }
                return s;
            }

            @Override
            public void forEach( Consumer<? super E> action ) {
                for ( Sequence<E> sequence : sequences ) {
                    sequence.forEach( action );
                }
            }
        };
    }

/* char elem */

    public static CharSequence join( final CharSequence... sequences ) {
        return new CharSequence() {
            @Override
            public int size() {
                int s = 0;
                for ( CharSequence sequence : sequences ) {
                    s += sequence.size();
                    if ( s < 0 ) s = Integer.MAX_VALUE;
                }
                return s;
            }

            @Override
            public void forEach( CharConsumer action ) {
                for ( CharSequence sequence : sequences ) {
                    sequence.forEach( action );
                }
            }
        };
    }


    public static <V> CharObjEntrySequence<V> join(
            final CharObjEntrySequence<V>... sequences ) {
        return new CharObjEntrySequence<V>() {
            @Override
            public int size() {
                int s = 0;
                for ( CharObjEntrySequence<V> sequence : sequences ) {
                    s += sequence.size();
                    if ( s < 0 ) s = Integer.MAX_VALUE;
                }
                return s;
            }

            @Override
            public void forEach( CharObjConsumer<? super V> action ) {
                for ( CharObjEntrySequence<V> sequence : sequences ) {
                    sequence.forEach( action );
                }
            }
        };
    }


    public static <K> ObjCharEntrySequence<K> join(
            final ObjCharEntrySequence<K>... sequences ) {
        return new ObjCharEntrySequence<K>() {
            @Override
            public int size() {
                int s = 0;
                for ( ObjCharEntrySequence<K> sequence : sequences ) {
                    s += sequence.size();
                    if ( s < 0 ) s = Integer.MAX_VALUE;
                }
                return s;
            }

            @Override
            public void forEach( ObjCharConsumer<? super K> action ) {
                for ( ObjCharEntrySequence<K> sequence : sequences ) {
                    sequence.forEach( action );
                }
            }
        };
    }

/* end */
/* char key short value */

    public static CharShortEntrySequence join(
            final CharShortEntrySequence... sequences ) {
        return new CharShortEntrySequence() {
            @Override
            public int size() {
                int s = 0;
                for ( CharShortEntrySequence sequence : sequences ) {
                    s += sequence.size();
                    if ( s < 0 ) s = Integer.MAX_VALUE;
                }
                return s;
            }

            @Override
            public void forEach( CharShortConsumer action ) {
                for ( CharShortEntrySequence sequence : sequences ) {
                    sequence.forEach( action );
                }
            }
        };
    }

/* end */

}
