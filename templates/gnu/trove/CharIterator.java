// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove;

//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


import gnu.trove.TIterator;

//TODO doc
public interface CharIterator extends TIterator</*c*/Character> {

    /**
     * Primitive specialization of {@link #next()}.
     *
     * <p>Moves the iterator forward to the next element, and returns it.
     * Calling {@code move()} has the exactly same effect as calling
     * {@code charValue()} method, preceded by {@link #tryAdvance()}.
     *
     * @return the next element in the iteration
     */
    char nextChar();


    /**
     * Primitive specialization of {@link #value()}.
     *
     * <p>Returns the last element, passed by the iterator.
     *
     * <p>The result of calling {@code value()} on newly-created iterator prior
     * to {@link #tryAdvance()} or {@link #next()} is undefined, for example
     * an {@code IndexOutOfBoundsException} could be thrown.
     *
     * @return the last element, passed by the iterator
     */
    char charValue();

}
