// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.*;
import gnu.trove.map.*;
import gnu.trove.map.hash.*;
import gnu.trove.set.ShortSet;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class DHashCharShortMap extends CharShortDHash<Short>
        implements HashCharShortMap, ReverseCharShortMapOps {

    public DHashCharShortMap() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashCharShortMap( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashCharShortMap( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashCharShortMap( CharShortEntrySequence entrySequence ) {
        this( entrySequence, entrySequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashCharShortMap( CharShortEntrySequence entrySequence,
            int expectedSize, float loadFactor ) {
        if ( entrySequence instanceof DHashCharShortMap ) {
            DHashCharShortMap m = ( DHashCharShortMap ) entrySequence;
            setUp( loadFactor, m.size(), false );
            byte[] hash = m.states;
            char[] keys = m.set;
            short[] vals = m.values;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    insertForRehash( keys[ i ], vals[ i ] );
            }
        }
        else {
            setUp( loadFactor, expectedSize, true );
            entrySequence.forEach( new CharShortConsumer() {
                @Override
                public void accept( char key, short value ) {
                    DHashCharShortMap.this.justPut( key, value );
                }
            } );
        }
    }


    @Override
    public short getNoEntryValue() {
        return Constants.DEFAULT_SHORT_NO_ENTRY_VALUE;
    }


    public int hashCode() {
        if ( isEmpty() )
            return 0;
        int mc = modCount();
        int hashCode = 0;
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                hashCode += Primitives.identityHashCode( keys[ i ] ) ^
                        Primitives.identityHashCode( vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }


    @Override
    public boolean containsValue( short value ) {
        return valueIndex( value ) >= 0;
    }


    @Override
    public boolean containsValue( Object value ) {
        return containsValue( ( short ) ( Short ) value );
    }


    @Override
    public boolean containsEntry( char key, short value ) {
        int index = index( key );
        return index >= 0 && values[ index ] == value;
    }


    @Override
    public boolean containsEntry( Character key, Short value ) {
        return containsEntry( ( char ) key, ( short ) value );
    }


    private int valueIndex( short value ) {
        if ( isEmpty() )
            return -1;
        byte[] hash = states;
        short[] vals = this.values;
        for (int i = hash.length; i-- > 0;) {
            if ( isFull( hash[ i ] ) && vals[ i ] == value ) {
                return i;
            }
        }
        return -1;
    }


    @Override
    public short get( char key ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : getNoEntryValue();
    }


    @Override
    public short getOrDefault( char key, short defaultValue ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : defaultValue;
    }


    @Override
    public Short get( Object k ) {
        char key = ( Character ) k;
        int index = index( key );
        return index >= 0 ? values[ index ] : null;
    }


    @Override
    public Short getOrDefault( Object k, Short defaultValue ) {
        char key = ( Character ) k;
        int index = index( key );
        return index >= 0 ? Short.valueOf( values[ index ] ) : defaultValue;
    }


    @Override
    public short remove( char key ) {
        int index = index( key );
        if ( index >= 0 ) {
            short previous = values[ index ];
            removeAt( index );
            return previous;
        } else {
            return getNoEntryValue();
        }
    }


    @Override
    public Short remove( Object key ) {
        int index = index( ( Character ) key );
        if ( index >= 0 ) {
            short previous = values[ index ];
            removeAt( index );
            return previous;
        } else {
            return null;
        }
    }


    @Override
    public boolean remove( char key, short value ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( values[ index ] == value ) {
                removeAt( index );
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean remove( Object key, Object value ) {
        return remove( ( char ) ( Character ) key, ( short ) ( Short ) value );
    }


    @Override
    public short put( char key, short value ) {
        int index = insert( key, value );
        if ( index < 0 ) {
            return getNoEntryValue();
        } else {
            short previous = values[ index ];
            values[ index ] = value;
            return previous;
        }
    }


    @Override
    public Short put( Character key, Short value ) {
        short v = value;
        int index = insert( key, v );
        if ( index < 0 ) {
            return null;
        } else {
            short previous = values[ index ];
            values[ index ] = v;
            return previous;
        }
    }


    @Override
    public short putIfAbsent( char key, short value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            return values[ index ];
        } else {
            return getNoEntryValue();
        }
    }


    @Override
    public Short putIfAbsent( Character key, Short value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            return values[ index ];
        } else {
            return null;
        }
    }


    @Override
    public void justPut( char key, short value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            values[ index ] = value;
        }
    }


    @Override
    public void justPut( Character key, Short value ) {
        justPut( ( char ) key, ( short ) value );
    }


    @Override
    public short adjustOrPutValue( char key, short adjustAmount, short putAmount ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            return values[ insertionIndex.get() ] += adjustAmount;
        } else {
            insertAt(insertionIndex, key, putAmount);
            return putAmount;
        }
    }


    @Override
    public short compute( char key,
            CharShortToShortFunction remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            short oldValue = values[ index ];
            short newValue = remappingFunction.applyAsShort( key, oldValue );
            values[ index ] = newValue;
            return newValue;
        } else {
            short newValue = remappingFunction.applyAsShort( key, getNoEntryValue() );
            insertAt( insertionIndex, key, newValue );
            return newValue;
        }
    }


    @Override
    public Short compute( Character key,
            BiFunction<? super Character, ? super Short, ? extends Short> remappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        int index = insertionIndex.get();
        if ( insertionIndex.existing() ) {
            short oldValue = values[ index ];
            Short newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            Short newValue = remappingFunction.apply( key, null );
            if ( newValue != null ) {
                insertAt( insertionIndex, k, newValue );
                return newValue;
            } else {
                return null;
            }
        }
    }


    @Override
    public short computeIfAbsent( char key, CharToShortFunction mappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.absent() ) {
            short newValue = mappingFunction.applyAsShort( key );
            insertAt( insertionIndex, key, newValue );
            return newValue;
        } else {
            return values[ insertionIndex.get() ];
        }
    }


    @Override
    public Short computeIfAbsent( Character key,
            Function<? super Character, ? extends Short> mappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        if ( insertionIndex.absent() ) {
            Short newValue = mappingFunction.apply( key );
            if ( newValue != null ) {
                insertAt( insertionIndex, k, newValue );
                return newValue;
            } else {
                return null;
            }
        } else {
            return values[ insertionIndex.get() ];
        }
    }


    @Override
    public short computeIfPresent( char key, CharShortToShortFunction remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            short oldValue = values[ index ];
            short newValue = remappingFunction.applyAsShort( key, oldValue );
            values[ index ] = newValue;
            return newValue;
        }
        return getNoEntryValue();
    }


    @Override
    public Short computeIfPresent( Character key,
            BiFunction<? super Character, ? super Short, ? extends Short> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            short oldValue = values[ index ];
            Short newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        }
        return null;
    }


    @Override
    public short merge( char key, short value, ShortBinaryOperator remappingOperator ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            short oldValue = values[ index ];
            short newValue = remappingOperator.applyAsShort( oldValue, value );
            values[ index ] = newValue;
            return newValue;
        } else {
            insertAt( insertionIndex, key, value );
            return value;
        }
    }


    @Override
    public Short merge( Character key, Short value,
            BiFunction<? super Short, ? super Short, ? extends Short> remappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            short oldValue = values[ index ];
            Short newValue = remappingFunction.apply( oldValue, value );
            if (newValue != null) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            if ( value != null ) {
                insertAt( insertionIndex, k, value );
                return value;
            } else {
                return null;
            }
        }
    }


    @Override
    public short replace( char key, short value ) {
        int index = index( key );
        if (index >= 0) {
            short oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        } else {
            return getNoEntryValue();
        }
    }


    @Override
    public Short replace( Character key, Short value ) {
        int index = index( key );
        if (index >= 0) {
            short oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        } else {
            return null;
        }
    }


    @Override
    public boolean replace( char key, short oldValue, short newValue ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( values[ index ] == oldValue ) {
                values[ index ] = newValue;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean replace( Character key, Short oldValue, Short newValue ) {
        return replace( ( char ) key, ( short ) oldValue, ( short ) newValue );
    }


    @Override
    public void forEach( CharShortConsumer action ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void forEach( BiConsumer<? super Character, ? super Short> action ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean testWhile( CharShortPredicate predicate ) {
        if ( isEmpty() )
            return true;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        boolean terminated = false;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !predicate.test( keys[ i ], vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public boolean testWhile( BiPredicate<? super Character, ? super Short> predicate ) {
        if ( isEmpty() )
            return true;
        boolean terminated = false;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !predicate.test( keys[ i ], vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public void replaceAll( CharShortToShortFunction function ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                vals[ i ] = function.applyAsShort( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void replaceAll( BiFunction<? super Character, ? super Short, ? extends Short> function ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                vals[ i ] = function.apply( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean allEntriesContainingIn( CharShortMap map ) {
        if ( isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public boolean allEntriesContainingIn(
            TMap<? super Character, ? super Short> map ) {
        if ( isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public void reversePutAllTo( CharShortMap map ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                map.justPut( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void reversePutAllTo( TMap<? super Character, ? super Short> map ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                map.justPut( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean removeIf( CharShortPredicate filter ) {
        if ( isEmpty() )
            return false;
        boolean changed = false;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    filter.test( keys[ i ], vals[ i ] ) ) {
                removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public boolean removeIf( BiPredicate<? super Character, ? super Short> filter ) {
        if ( isEmpty() )
            return false;
        boolean changed = false;
        int mc = modCount();
        byte[] hash = states;
        char[] keys = set;
        short[] vals = values;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    filter.test( keys[ i ], vals[ i ] ) ) {
                removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public boolean containsAllEntries( Map<? extends Character, ? extends Short> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        if ( map instanceof ReverseCharShortMapOps ) {
            //noinspection unchecked
            return ( ( ReverseCharShortMapOps ) map ).allEntriesContainingIn( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            return ( ( ReverseMapOps ) map ).allEntriesContainingIn( this );
        } else {
            return Maps.containsAllEntries( this, map );
        }
    }


    @Override
    public void putAll( @NotNull Map<? extends Character, ? extends Short> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        ensureCapacity( map.size() );
        if ( map instanceof ReverseCharShortMapOps ) {
            //noinspection unchecked
            ( ( ReverseCharShortMapOps ) map ).reversePutAllTo( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) map ).reversePutAllTo( this );
        } else {
            Maps.putAll( this, map );
        }
    }


    @Override
    @NotNull
    public CharShortMapIterator mapIterator() {
        return new IteratorImpl();
    }


    private class IteratorImpl extends CharKeyHashIterator<Short>
        implements CharShortMapIterator {

        protected final short[] values = DHashCharShortMap.this.values;


        @Override
        public Short value() {
            return values[ index ];
        }


        @Override
        public void setValue( Short value ) {
            values[ index ] = value;
        }


        @Override
        public short shortValue() {
            return values[ index ];
        }


        @Override
        public void setValue( short value ) {
            values[ index ] = value;
        }
    }


    @Override
    @NotNull
    public HashShortCollection values() {
        return new ValueView();
    }


    private class ValueView extends View<Short>
            implements HashShortCollection, ReverseShortCollectionOps {

        @Override
        public short getNoEntryValue() {
            return DHashCharShortMap.this.getNoEntryValue();
        }


        @Override
        public boolean contains( short value ) {
            return DHashCharShortMap.this.containsValue( value );
        }


        @Override
        public short[] toShortArray() {
            return HashBlocks.toShortArray( DHashCharShortMap.this, values );
        }


        @Override
        public short[] toArray( short[] dest ) {
            return HashBlocks.toShortArray(
                    DHashCharShortMap.this, values, getNoEntryValue(), dest );
        }


        @Override
        public boolean add( short value ) {
            throw new UnsupportedOperationException();
        }


        @Override
        public boolean remove( short value ) {
            int index = valueIndex( value );
            if ( index >= 0 ) {
                removeAt( index );
                return true;
            } else {
                return false;
            }
        }


        @Override
        public boolean removeIf( ShortPredicate filter ) {
            return HashBlocks.removeShortsIf(
                    DHashCharShortMap.this, values, filter );
        }


        @Override
        public boolean removeIf( Predicate<? super Short> filter ) {
            return HashBlocks.removeShortsIf(
                    DHashCharShortMap.this, values, filter );
        }


        @Override
        public boolean allShortsContainingIn( ShortCollection collection ) {
            return HashBlocks.reverseAllShortsContainingIn(
                    DHashCharShortMap.this, values, collection );
        }


        @Override
        public boolean allContainingIn( TCollection<? super Short> collection ) {
            return HashBlocks.reverseAllShortsContainingIn(
                    DHashCharShortMap.this, values, collection );
        }


        @Override
        public boolean reverseAddAllShortsTo( ShortCollection collection ) {
            return HashBlocks.reverseAddAllShorts(
                    DHashCharShortMap.this, values, collection );
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super Short> collection ) {
            return HashBlocks.reverseAddAllShorts(
                    DHashCharShortMap.this, values, collection );
        }


        @Override
        public boolean reverseRemoveAllShortsFrom( ShortSet set ) {
            return HashBlocks.reverseRemoveAllShorts(
                    DHashCharShortMap.this, values, set );
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super Short> set ) {
            return HashBlocks.reverseRemoveAllShorts(
                    DHashCharShortMap.this, values, set );
        }


        @Override
        public boolean contains( Object o ) {
            return DHashCharShortMap.this.containsValue( o );
        }


        @Override
        @NotNull
        public Object[] toArray() {
            return HashBlocks.toArray( DHashCharShortMap.this, values );
        }


        @Override
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            return HashBlocks.toArray( DHashCharShortMap.this, values, a );
        }


        @Override
        public boolean remove( Object o ) {
            return remove( ( short ) ( Short ) o );
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || another.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            short[] vals = values;
            if ( another instanceof ShortCollection ) {
                ShortCollection anotherChars = ( ShortCollection ) another;
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) &&
                            anotherChars.contains( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && another.contains( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> another ) {
            return HashBlocks.retainAllShorts(
                    this, DHashCharShortMap.this, values, another );
        }


        @Override
        @NotNull
        public ShortIterator iterator() {
            return new ValueIterator();
        }


        @Override
        public void forEach( ShortConsumer action ) {
            HashBlocks.forEachShort( DHashCharShortMap.this, values, action );
        }


        @Override
        public boolean testWhile( ShortPredicate predicate ) {
            return HashBlocks.testShortsWhile( DHashCharShortMap.this, values, predicate );
        }


        @Override
        public void forEach( Consumer<? super Short> action ) {
            HashBlocks.forEachShort( DHashCharShortMap.this, values, action );
        }


        @Override
        public boolean testWhile( Predicate<? super Short> predicate ) {
            return HashBlocks.testShortsWhile( DHashCharShortMap.this, values, predicate );
        }
    }


    private class ValueIterator extends StatesIterator implements ShortIterator {

        protected final short[] values = DHashCharShortMap.this.values;


        @Override
        public Short value() {
            return values[ index ];
        }


        @Override
        public Short next() {
            advance();
            return values[ index ];
        }


        @Override
        public short nextShort() {
            advance();
            return values[ index ];
        }


        @Override
        public short shortValue() {
            return values[ index ];
        }
    }



    @Override
    @NotNull
    public HashObjSet<Map.Entry<Character, Short>> entrySet() {
        return new EntryView();
    }


    private class EntryView extends CharHashBackedEntryView {

        @Override
        @NotNull
        public TIterator<Map.Entry<Character, Short>> iterator() {
            return new EntryIterator();
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean remove( Object o ) {
            Map.Entry<Character, Short> e = ( Map.Entry<Character, Short> ) o;
            int index = index( e.getKey() );
            short value = e.getValue();
            if ( index >= 0 ) {
                if ( values[ index ] == value ) {
                    removeAt( index );
                    return true;
                }
            }
            return false;
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean contains( Object o ) {
            Map.Entry<Character, Short> e = ( Map.Entry<Character, Short> ) o;
            int index = index( e.getKey() );
            short value = e.getValue();
            return index >= 0 && values[ index ] == value;
        }


        @Override
        protected Map.Entry<Character, Short> entryAt( int index ) {
            return new Entry( index );
        }
    }


    private class EntryIterator extends StatesIterator
            implements TIterator<Map.Entry<Character, Short>> {
        private Entry entry;

        @Override
        public Map.Entry<Character, Short> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<Character, Short> value() {
            return entry;
        }
    }


    private class Entry extends CharHashEntry {

        Entry( int index ) {
            super( index );
        }


        @Override
        public Short getValue() {
            return values[ index ];
        }


        @Override
        public Short setValue( Short newValue ) {
            Short oldValue = getValue();
            values[ index ] = newValue;
            return oldValue;
        }


        @SuppressWarnings("unchecked")
        public boolean equals( Object o ) {
            if ( o == null )
                return false;
            Map.Entry e2;
            char key2;
            short value2;
            try {
                e2 = ( Map.Entry ) o;
                key2 = ( Character ) e2.getKey();
                value2 = ( Short ) e2.getValue();
            } catch ( ClassCastException e ) {
                return false;
            } catch ( NullPointerException e ) {
                return false;
            }
            int i = index;
            return set[ i ] == key2 && values[ i ] == value2;
        }


        public int hashCode() {
            int i = index;
            return Primitives.identityHashCode( set[ i ] ) ^
                    Primitives.identityHashCode( values[ i ] );
        }

    }
}
