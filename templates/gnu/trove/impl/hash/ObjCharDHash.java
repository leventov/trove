// byte|short|char|int|long|float|double|object value //
// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.impl.Primitives;
import org.jetbrains.annotations.Nullable;

import java.util.ConcurrentModificationException;


public abstract class ObjCharDHash<K, V> extends ObjDHashBackedMap<K, V> {
    
    char[] values;

    
    @Override
    void init( int capacity ) {
        super.init( capacity );
        /* if byte|short|char|int|long|float|double value */
        values = new char[ capacity ];
        /* elif object value //
            // noinspection unchecked
            values = ( V[] ) new Object[ capacity ];
        // endif */
    }

    
    @Override
    final void rehash( int newCapacity ) {
        int mc = modCount();
        int oldSize = size();
        // noinspection unchecked
        K[] oldKeys = ( K[] ) set;
        char[] oldValues = values;
        boolean wasNoRemoved = noRemoved();

        initForRehash( newCapacity );
        mc++; // modCount is incremented in initForRehash()
        if ( oldSize == 0 )
            return;
        // Process entries from the old array, skipping free and removed slots.
        // Put the values into the appropriate place in the new array.
        if ( wasNoRemoved ) {
            for ( int i = oldKeys.length; i-- > 0; ) {
                K key = oldKeys[ i ];
                if ( key != FREE ) {
                    insertForRehash( key, oldValues[ i ] );
                }
            }
        } else {
            for ( int i = oldKeys.length; i-- > 0; ) {
                K key = oldKeys[ i ];
                if ( key != FREE && key != REMOVED ) {
                    insertForRehash( key, oldValues[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    int insert( @Nullable K key, char value ) {
        if ( key != null ) {
            // noinspection unchecked
            K[] set = ( K[] ) this.set;
            int capacity = set.length;
            // Hash should be positive, because index = hash % capacity
            // should be positive.
            int hash = hashCode( key ) & Primitives.DROP_INT_SIGN_BIT;
            int index = hash % capacity; // firstIndex
            K cur = set[ index ];
            // In insertion operation, we suppose target object
            // is most likely absent in the hash map yet,
            // and the hash map is mostly empty (current load < 0.5).
            // Most probable branch goes first:
            if ( cur == FREE ) {
                set[ index ] = key;
                values[ index ] = value;
                postFreeSlotInsertHook();
                return -1;
            }
            else if ( cur == key ) {
                return index;
            }
            else {
                int firstRemoved;
                if ( cur != REMOVED ) {
                    if ( equals( key, cur ) ) {
                        return index;
                    }
                    firstRemoved = -1;
                } else {
                    firstRemoved = index;
                }
                return insertProbingLoop(
                        key, value, set, hash, capacity, index, firstRemoved );
            }
        }
        else {
            return insertNull( value );
        }
    }


    private int insertProbingLoop( K key, char value, K[] set,
            int hash, int capacity, int index, int firstRemoved ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( firstRemoved < 0 && noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == FREE ) {
                    set[ index ] = key;
                    values[ index ] = value;
                    postFreeSlotInsertHook();
                    return -1;
                }
                // explicit identity check to reduce
                // the harm from low-quality equals() implementation
                else if ( cur == key || equals( key, cur ) ) {
                    return index;
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                K cur = set[ index ];
                if ( cur == FREE ) {
                    if ( firstRemoved < 0 ) {
                        set[ index ] = key;
                        values[ index ] = value;
                        postFreeSlotInsertHook();
                    } else {
                        set[ firstRemoved ] = key;
                        values[ firstRemoved ] = value;
                        postRemovedSlotInsertHook();
                    }
                    return -1;
                }
                else if ( cur == key ) {
                    return index;
                }
                else if ( cur != REMOVED ) {
                    if ( equals( key, cur ) ) {
                        return index;
                    }
                }
                // cur is REMOVED
                else if ( firstRemoved < 0 ) {
                    firstRemoved = index;
                }
            }
        }
    }


    private int insertNull( char value ) {
        Object[] set = this.set;
        int firstRemoved = -1;
        for ( int index = 0; ; index++ ) {
            Object cur = set[ index ];
            if ( cur == FREE ) {
                if ( firstRemoved < 0 ) {
                    set[ index ] = null;
                    values[ index ] = value;
                    postFreeSlotInsertHook();
                } else {
                    set[ firstRemoved ] = null;
                    values[ firstRemoved ] = value;
                    postRemovedSlotInsertHook();
                }
                return -1;
            } else if ( cur == null ) {
                return index;
            } else if ( cur == REMOVED && firstRemoved < 0 ) {
                firstRemoved = index;
            }
        }
    }


    final void insertForRehash( @Nullable K key, char value ) {
        if ( key != null ) {
            Object[] set = this.set;
            int capacity = set.length;
            // Hash should be positive, because index = hash % capacity
            // should be positive.
            int hash = hashCode( key ) & Primitives.DROP_INT_SIGN_BIT;
            int index = hash % capacity; // firstIndex

            // During rehash, there are no REMOVED slots and key is not present
            // in the hash map.
            if ( set[ index ] == FREE ) {
                set[ index ] = key;
                values[ index ] = value;
            } else {
                insertForRehashProbingLoop(
                        key, value, set, hash, capacity, index );
            }
        }
        else {
            insertNullForRehash( value );
        }
    }


    private void insertForRehashProbingLoop( K key, char value, Object[] set,
            int hash, int capacity, int index ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        for ( ; ; ) {
            if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
            if ( set[ index ] == FREE ) {
                set[ index ] = key;
                values[ index ] = value;
                return;
            }
        }
    }


    private void insertNullForRehash( char value ) {
        Object[] set = this.set;
        for ( int index = 0; ; index++ ) {
            if ( set[ index ] == FREE ) {
                set[ index ] = null;
                values[ index ] = value;
                return;
            }
        }
    }


    final void insertAt( InsertionIndex index, K key, char value ) {
        int i = index.get();
        set[ i ] = key;
        values[ i ] = value;
        postInsertHook( index.freeSlot() );
    }
}
