// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.impl.Primitives;

//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


public abstract class CharSetDHash<V> extends CharDHash<V> {

    final int insert( char key ) {
        /* if float|double elem //
        if ( key != key )
            throw new IllegalArgumentException("NaN keys are not supported");
        // endif */

        byte[] states = this.states;
        char[] set = this.set;
        int capacity = states.length;
        // Hash should be positive, because index = hash % capacity
        // should be positive.
        /* if byte|short|int|long|float|double elem */
            int hash = Primitives.positiveHash( key );
        /* elif char // // Considerably simplifies resulting bytecode.
            int hash = key;
        // endif */
        int index = hash % capacity; // firstIndex
        byte state = states[ index ];
        int firstRemoved;
        // In insertion operation, we suppose target value
        // is most likely absent in the hash yet,
        // and the hash is mostly empty (current load < 0.5).
        // Most probable branch goes first:
        if ( state == FREE ) {
            states[ index ] = FULL;
            set[ index ] = key;
            postFreeSlotInsertHook();
            return -1;
        }
        else if ( state < 0 ) { // FULL state
            if ( set[ index ] == key ) return index;
            else firstRemoved = -1;
        }
        else { // state is REMOVED
            firstRemoved = index;
        }

        return insertProbingLoop(
                key, states, set, capacity, hash, index, firstRemoved );
    }


    private int insertProbingLoop( char key, byte[] states, char[] set,
            int capacity, int hash, int index, int firstRemoved ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( firstRemoved < 0 && noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                if ( states[ index ] == FREE ) {
                    states[ index ] = FULL;
                    set[ index ] = key;
                    postFreeSlotInsertHook();
                    return -1;
                }
                else if ( set[ index ] == key ) {
                    return index;
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                byte state = states[ index ];
                if ( state == FREE ) {
                    if ( firstRemoved < 0 ) {
                        states[ index ] = FULL;
                        set[ index ] = key;
                        postFreeSlotInsertHook();
                    } else {
                        states[ firstRemoved ] = FULL;
                        set[ firstRemoved ] = key;
                        postRemovedSlotInsertHook();
                    }
                    return -1;
                }
                else if ( state < 0 ) { // FULL state
                    if ( set[ index ] == key ) {
                        return index;
                    }
                }
                // state is REMOVED
                else if ( firstRemoved < 0 ) {
                    firstRemoved = index;
                }
            }
        }
    }


    final void insertForRehash( char key ) {
        byte[] states = this.states;
        int capacity = states.length;
        // Hash should be positive, because index = hash % capacity
        // should be positive.
        /* if byte|short|int|long|float|double elem */
            int hash = Primitives.positiveHash( key );
        /* elif char // // Considerably simplifies resulting bytecode.
            int hash = key;
        // endif */
        int index = hash % capacity; // firstIndex
        // During rehash, there are no REMOVED slots and key is not present
        // in the hash.
        if ( states[ index ] == FREE ) {
            states[ index ] = FULL;
            set[ index ] = key;
        } else {
            insertForRehashProbingLoop( key, states, hash, capacity, index );
        }
    }


    private void insertForRehashProbingLoop( char key,
            byte[] states, int hash, int capacity, int index ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        for ( ; ; ) {
            if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
            if ( states[ index ] == FREE ) {
                states[ index ] = FULL;
                set[ index ] = key;
                return;
            }
        }
    }
}
