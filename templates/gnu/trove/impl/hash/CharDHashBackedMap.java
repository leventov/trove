// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.*;
import gnu.trove.CharIterator;
import gnu.trove.impl.Collections;
import gnu.trove.map.TMap;
import gnu.trove.set.CharSet;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashCharSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.*;


public abstract class CharDHashBackedMap<V> extends CharDHash<V>
        implements TMap<Character, V> {

    public char getNoEntryKey() {
        return Constants.DEFAULT_CHAR_NO_ENTRY_VALUE;
    }


    @Override
    public final boolean containsKey( Object key ) {
        return containsKey( ( char ) ( Character ) key );
    }


    public boolean containsKey( char key ) {
        return index( key ) >= 0;
    }


    @Override
    public final boolean justRemove( Character key ) {
        return justRemove( ( char ) key );
    }


    public boolean justRemove( char key ) {
        int index = index( key );
        if (index >= 0) {
            removeAt( index );
            return true;
        }
        return false;
    }


    public final boolean equals( Object obj ) {
        return Maps.equals( this, obj );
    }


    public abstract int hashCode();


    public String toString() {
        return Maps.toString( this );
    }


    @Override
    @NotNull
    public HashCharSet keySet() {
        return new KeyView();
    }


    class KeyView extends View<Character>
            implements HashCharSet, HashCharCollection, ReverseCharCollectionOps {


        CharDHash getHash() {
            return CharDHashBackedMap.this;
        }


        @Override
        public char getNoEntryValue() {
            return CharDHashBackedMap.this.getNoEntryKey();
        }


        @Override
        @NotNull
        public CharIterator iterator() {
            return new CharHashIterator();
        }


        @Override
        public boolean remove( char value ) {
            return justRemove( value );
        }


        @Override
        public boolean contains( char key ) {
            return containsKey( key );
        }


        @Override
        public boolean removeIf( Predicate<? super Character> filter ) {
            return HashBlocks.removeCharsIf( CharDHashBackedMap.this, set, filter );
        }


        @Override
        public boolean allContainingIn( TCollection<? super Character> collection ) {
            return HashBlocks.reverseAllCharsContainingIn(
                    CharDHashBackedMap.this, set, collection );
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super Character> collection ) {
            return HashBlocks.reverseAddAllChars( CharDHashBackedMap.this, set, collection );
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super Character> s ) {
            return HashBlocks.reverseRemoveAllChars( CharDHashBackedMap.this, set, s );
        }


        @Override
        public void forEach( Consumer<? super Character> action ) {
            HashBlocks.forEachChar( CharDHashBackedMap.this, set, action );
        }


        @Override
        public void forEach( CharConsumer action ) {
            HashBlocks.forEachChar( CharDHashBackedMap.this, set, action );
        }


        @Override
        public boolean testWhile( Predicate<? super Character> predicate ) {
            return HashBlocks.testCharsWhile( CharDHashBackedMap.this, set, predicate );
        }


        @Override
        public boolean testWhile( CharPredicate predicate ) {
            return HashBlocks.testCharsWhile( CharDHashBackedMap.this, set, predicate );
        }


        @Override
        @NotNull
        public Object[] toArray() {
            return HashBlocks.toArray( CharDHashBackedMap.this, set );
        }


        @Override
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            return HashBlocks.toArray( CharDHashBackedMap.this, set, a );
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> c ) {
            return HashBlocks.removeAllCharsFromSet(
                    this, CharDHashBackedMap.this, set, c );
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> c ) {
            return HashBlocks.retainAllChars( this, CharDHashBackedMap.this, set, c );
        }


        public int hashCode() {
            return HashBlocks.setHashCode( CharDHashBackedMap.this );
        }


        public boolean equals( Object obj ) {
            return Collections.setEquals( this, obj );
        }


        @Override
        public char[] toCharArray() {
            return HashBlocks.toCharArray( CharDHashBackedMap.this, set );
        }


        @Override
        public char[] toArray( char[] dest ) {
            return HashBlocks.toCharArray(
                    CharDHashBackedMap.this, set, getNoEntryValue(), dest );
        }


        @Override
        public boolean removeIf( CharPredicate filter ) {
            return HashBlocks.removeCharsIf( CharDHashBackedMap.this, set, filter );
        }


        @Override
        public boolean allCharsContainingIn( CharCollection collection ) {
            return HashBlocks.reverseAllCharsContainingIn( CharDHashBackedMap.this, set, collection );
        }


        @Override
        public boolean reverseAddAllCharsTo( CharCollection collection ) {
            return HashBlocks.reverseAddAllChars( CharDHashBackedMap.this, set, collection );
        }


        @Override
        public boolean reverseRemoveAllCharsFrom( CharSet s ) {
            return HashBlocks.reverseRemoveAllChars( CharDHashBackedMap.this, set, s );
        }


        @Override
        public final boolean contains( Object o ) {
            return contains( ( char ) ( Character ) o );
        }


        @Override
        public final boolean remove( Object o ) {
            return remove( ( char ) ( Character ) o );
        }


        @Override
        public final boolean add( char value ) {
            throw new UnsupportedOperationException();
        }
    }


    abstract class CharHashBackedEntryView
            extends View<Entry<Character, V>>
            implements HashObjSet<Entry<Character, V>> {

        public final int hashCode() {
            return CharDHashBackedMap.this.hashCode();
        }


        public boolean equals( Object o ) {
            return Collections.setEquals( this, o );
        }


        protected abstract Map.Entry<Character, V> entryAt( int index );


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super Entry<Character, V>> s ) {
            if ( this.isEmpty() || s.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= s.remove( entryAt( i ) );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean removeIf( Predicate<? super Map.Entry<Character, V>> filter ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && filter.test( entryAt( i ) ) ) {
                    removeAt( i );
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean allContainingIn(
                TCollection<? super Map.Entry<Character, V>> collection ) {
            if ( isEmpty() )
                return true;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !collection.contains( entryAt( i ) ) )
                    return false;
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return true;
        }


        @Override
        public final boolean reverseAddAllTo(
                TCollection<? super Map.Entry<Character, V>> collection ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= collection.add( entryAt( i ) );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        @NotNull
        public final Object[] toArray() {
            int size = size();
            Object[] result = new Object[ size ];
            if ( size == 0 )
                return result;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    result[ resultIndex++ ] = entryAt( i );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return result;
        }

        @Override
        @SuppressWarnings("unchecked")
        @NotNull
        public final <T> T[] toArray( @NotNull T[] a ) {
            int size = size();
            if ( a.length < size ) {
                Class<?> tType = a.getClass().getComponentType();
                a = ( T[] ) Array.newInstance( tType, size );
            }
            if ( size == 0 ) {
                if ( a.length > 0 )
                    a[ 0 ] = null;
                return a;
            }
            int resultIndex = 0;
            int mc = modCount();
            byte[] hash = states;
            for (int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    a[ resultIndex++ ] = ( T ) entryAt( i );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            if ( a.length > resultIndex ) {
                a[ resultIndex ] = null;
            }
            return a;
        }


        @Override
        public final boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public final boolean removeAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || another.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) &&
                        another.contains( entryAt( i ) ) ) {
                    mc++;
                    removeAt( i );
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean retainAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( another.isEmpty() ) {
                if ( this.isEmpty() ) {
                    return false;
                } else {
                    clear();
                    return true;
                }
            }
            boolean changed = false;
            int mc = modCount();
            byte[] hash = states;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !another.contains( entryAt( i ) ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final void forEach( Consumer<? super Map.Entry<Character, V>> action ) {
            if ( isEmpty() )
                return;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    action.accept( entryAt( i ) );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
        }


        @Override
        public final boolean testWhile( Predicate<? super Map.Entry<Character, V>> predicate ) {
            if ( isEmpty() )
                return true;
            boolean terminated = false;
            int mc = modCount();
            byte[] hash = states;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !predicate.test( entryAt( i ) ) ) {
                    terminated = true;
                    break;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return !terminated;
        }
    }


    abstract class CharHashEntry implements Map.Entry<Character, V> {

        final int index;


        CharHashEntry( int index ) {
            this.index = index;
        }


        @Override
        public Character getKey() {
            return set[ index ];
        }


        @Override
        public String toString() {
            return getKey() + "=" + getValue();
        }


        public abstract int hashCode();


        public abstract boolean equals( Object obj );
    }
}
