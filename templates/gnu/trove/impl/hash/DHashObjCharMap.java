// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.*;
import gnu.trove.CharIterator;
import gnu.trove.map.*;
import gnu.trove.map.hash.HashObjCharMap;
import gnu.trove.set.CharSet;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.lang.reflect.Array;
import java.util.*;


//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


//TODO doc
public class DHashObjCharMap<K> extends ObjCharDHash<K, Character>
        implements HashObjCharMap<K>, ReverseObjCharMapOps<K> {


    public DHashObjCharMap() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashObjCharMap( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashObjCharMap( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashObjCharMap( ObjCharEntrySequence<? extends K> entrySequence ) {
        this( entrySequence, entrySequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashObjCharMap( ObjCharEntrySequence<? extends K> entrySequence,
            int expectedSize, float loadFactor ) {
        // just expected size because equality operation may differ
        setUp( loadFactor, expectedSize, true );
        if ( entrySequence instanceof ReverseObjCharMapOps ) {
            //noinspection unchecked
            ( ( ReverseObjCharMapOps ) entrySequence ).reversePutAllTo( this );
        } else if ( entrySequence instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) entrySequence ).reversePutAllTo( this );
        } else {
            entrySequence.forEach( new ObjCharConsumer<K>() {
                @Override
                public void accept( K key, char value ) {
                    DHashObjCharMap.this.justPut( key, value );
                }
            } );
        }
    }


    // Query Operations

    @Override
    public char getNoEntryValue() {
        return Constants.DEFAULT_CHAR_NO_ENTRY_VALUE;
    }


    @Override
    public boolean containsValue( char value ) {
        return valueIndex( value ) >= 0;
    }


    private int valueIndex( char value ) {
        if ( isEmpty() )
            return -1;
        int index = -1;
        int mc = modCount();
        Object[] hash = set;
        char[] values = this.values;
        if ( noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && value == values[i] ) {
                    index = i;
                    break;
                }
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && value == values[i] ) {
                    index = i;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return index;
    }


    @Override
    public boolean containsValue( Object value ) {
        return containsValue( ( char ) ( Character ) value );
    }


    @Override
    public char getChar( K key ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : getNoEntryValue();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Character get( Object key ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : null;
    }


    // Modification Operations

    @Override
    public char put( K key, char value ) {
        int index = insert( key, value );
        if ( index < 0 ) {
            return getNoEntryValue();
        } else {
            char previous = values[ index ];
            values[ index ] = value;
            return previous;
        }
    }


    @Override
    public Character put( K key, Character value ) {
        int index = insert( key, value );
        if ( index < 0 ) {
            return null;
        } else {
            char previous = values[ index ];
            values[ index ] = value;
            return previous;
        }
    }


    @Override
    public char putIfAbsent( K key, char value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            return values[ index ];
        } else {
            return getNoEntryValue();
        }
    }

    @Override
    public Character putIfAbsent( K key, Character value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            return values[ index ];
        } else {
            return null;
        }
    }


    @Override
    public void justPut( K key, char value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            values[ index ] = value;
        }
    }


    @Override
    @NotNull
    public HashCharCollection values() {
        return new ValueView();
    }


    @Override
    @NotNull
    public HashObjSet<Map.Entry<K, Character>> entrySet() {
        return new EntryView();
    }


    @Override
    public final void putAll( @NotNull Map<? extends K, ? extends Character> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        ensureCapacity( map.size() );
        if ( map instanceof ReverseObjCharMapOps ) {
            //noinspection unchecked
            ( ( ReverseObjCharMapOps ) map ).reversePutAllTo( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) map ).reversePutAllTo( this );
        } else {
            Maps.putAll( this, map );
        }
    }


    @Override
    public final boolean containsAllEntries( Map<? extends K, ? extends Character> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        if ( map instanceof ReverseObjCharMapOps ) {
            //noinspection unchecked
            return ( ( ReverseObjCharMapOps ) map ).allEntriesContainingIn( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            return ( ( ReverseMapOps ) map ).allEntriesContainingIn( this );
        } else {
            return Maps.containsAllEntries( this, map );
        }
    }


    @Override
    public boolean testWhile( ObjCharPredicate<? super K> predicate ) {
        if ( isEmpty() )
            return true;
        boolean terminated = false;
        int mc = modCount();
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] values = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !predicate.test( keys[ i ], values[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !predicate.test( keys[ i ], values[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public boolean testWhile( BiPredicate<? super K, ? super Character> predicate ) {
        if ( isEmpty() )
            return true;
        boolean terminated = false;
        int mc = modCount();
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] values = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !predicate.test( keys[ i ], values[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !predicate.test( keys[ i ], values[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public void reversePutAllTo( ObjCharMap<? super K> map ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void reversePutAllTo( TMap<? super K, ? super Character> map ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        //noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    map.justPut( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean allEntriesContainingIn( ObjCharMap<? super K> map ) {
        if ( isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public boolean allEntriesContainingIn( TMap<? super K, ? super Character> map ) {
        if ( isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) &&
                        !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public boolean removeIf( BiPredicate<? super K, ? super Character> filter ) {
        if ( isEmpty() )
            return false;
        boolean changed = false;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) && filter.test( keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) && filter.test( keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public boolean removeIf( ObjCharPredicate<? super K> filter ) {
        if ( isEmpty() )
            return false;
        boolean changed = false;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) && filter.test( keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) && filter.test( keys[ i ], vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public void justPut( K key, Character value ) {
        justPut( key, ( char ) value );
    }

    @Override
    public char removeAsChar( Object key ) {
        int index = index( key );
        if ( index >= 0 ) {
            char previous = values[index];
            removeAt( index );
            return previous;
        } else {
            return getNoEntryValue();
        }
    }


    @Override
    public Character remove( Object key ) {
        int index = index( key );
        if ( index >= 0 ) {
            char previous = values[index];
            removeAt( index );
            return previous;
        } else {
            return null;
        }
    }

    @Override
    public boolean remove( Object key, char value ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( values[ index ] == value ) {
                removeAt( index );
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean remove( Object key, Object value ) {
        return remove( key, ( char ) ( Character ) value );
    }

    @Override
    public char replace( K key, char value ) {
        int index = index( key );
        if (index >= 0) {
            char oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        } else {
            return getNoEntryValue();
        }
    }


    @Override
    public Character replace( K key, Character value ) {
        int index = index( key );
        if (index >= 0) {
            char oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        } else {
            return null;
        }
    }


    @Override
    public boolean replace( K key, char oldValue, char newValue ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( values[ index ] == oldValue ) {
                values[ index ] = newValue;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean containsEntry( Object key, char value ) {
        int index = index( key );
        return index >= 0 && values[ index ] == value;
    }


    @Override
    public boolean containsEntry( K key, Character value ) {
        return containsEntry( key, ( char ) value );
    }


    @Override
    public boolean replace( K key, Character oldValue, Character newValue ) {
        return replace( key, ( char ) newValue, ( char ) newValue );
    }


    @Override
    public void replaceAll( ObjCharToCharFunction<? super K> function ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) ) {
                    vals[ i ] = function.applyAsChar( keys[ i ], vals[ i ] );
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) ) {
                    vals[ i ] = function.applyAsChar( keys[ i ], vals[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void replaceAll( BiFunction<? super K, ? super Character, ? extends Character> function ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) ) {
                    vals[ i ] = function.apply( keys[ i ], vals[ i ] );
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) ) {
                    vals[ i ] = function.apply( keys[ i ], vals[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public Character compute( K key,
            BiFunction<? super K, ? super Character, ? extends Character> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        int index = insertionIndex.get();
        if ( insertionIndex.existing() ) {
            char oldValue = values[ index ];
            Character newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            Character newValue = remappingFunction.apply( key, null );
            if ( newValue != null ) {
                insertAt(insertionIndex, key, newValue);
                return newValue;
            } else {
                return null;
            }
        }
    }


    @Override
    public char compute( K key, ObjCharToCharFunction<? super K> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            char oldValue = values[ index ];
            char newValue = remappingFunction.applyAsChar( key, oldValue );
            values[ index ] = newValue;
            return newValue;
        } else {
            char newValue = remappingFunction.applyAsChar( key, getNoEntryValue() );
            insertAt( insertionIndex, key, newValue );
            return newValue;
        }
    }


    @Override
    public Character computeIfAbsent( K key,
            Function<? super K, ? extends Character> mappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.absent() ) {
            Character newValue = mappingFunction.apply( key );
            if ( newValue != null ) {
                insertAt( insertionIndex, key, newValue );
                return newValue;
            } else {
                return null;
            }
        } else {
            return values[ insertionIndex.get() ];
        }
    }


    @Override
    public char computeIfAbsent( K key, ToCharFunction<? super K> mappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.absent() ) {
            char newValue = mappingFunction.applyAsChar( key );
            insertAt( insertionIndex, key, newValue );
            return newValue;
        } else {
            return values[ insertionIndex.get() ];
        }
    }


    @Override
    public Character computeIfPresent( K key,
            BiFunction<? super K, ? super Character, ? extends Character> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            char oldValue = values[ index ];
            Character newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        }
        return null;
    }


    @Override
    public void forEach( BiConsumer<? super K, ? super Character> action ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] values = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void forEach( ObjCharConsumer<? super K> action ) {
        if ( isEmpty() )
            return;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] values = this.values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) )
                    action.accept( keys[ i ], values[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public char computeIfPresent( K key,
            ObjCharToCharFunction<? super K> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            char oldValue = values[ index ];
            char newValue = remappingFunction.applyAsChar( key, oldValue );
                values[ index ] = newValue;
                return newValue;
        }
        return getNoEntryValue();
    }


    @Override
    public Character merge( K key, Character value,
            BiFunction<? super Character, ? super Character, ? extends Character> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            char oldValue = values[ index ];
            Character newValue = remappingFunction.apply(oldValue, value);
            if (newValue != null) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            if ( value != null ) {
                insertAt( insertionIndex, key, value );
                return value;
            } else {
                return null;
            }
        }
    }


    @Override
    public char merge( K key, char value, CharBinaryOperator remappingOperator ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            char oldValue = values[ index ];
            char newValue = remappingOperator.applyAsChar(oldValue, value);
            values[ index ] = newValue;
            return newValue;
        } else {
            insertAt( insertionIndex, key, value );
            return value;
        }
    }


    /**
     * @return an iterator over the entries in this map
     */
    @Override
    @NotNull
    public CharValueMapIterator<K> mapIterator() {
        if ( noRemoved() ) {
            return new KeyValueNoRemovedIterator();
        } else {
            return new KeyValueIterator();
        }
    }


    @Override
    public Character getOrDefault( Object key, Character defaultValue ) {
        int index = index( key );
        return index >= 0 ? Character.valueOf( values[ index ] ) : defaultValue;
    }


    @Override
    public char getOrDefault( Object key, char defaultValue ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : defaultValue;
    }


    @Override
    public char adjustOrPutValue( K key, char adjustAmount, char putAmount ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            return values[ insertionIndex.get() ] += adjustAmount;
        } else {
            insertAt(insertionIndex, key, putAmount);
            return putAmount;
        }
    }


    @Override
    protected boolean keysEquals( @NotNull K a, @Nullable K b ) {
        return a.equals( b );
    }


    @Override
    protected int keyHashCode( @NotNull K key ) {
        return key.hashCode();
    }


    public int hashCode() {
        if ( isEmpty() )
            return 0;
        int hashCode = 0;
        int mc = modCount();
        // noinspection unchecked
        K[] keys = ( K[] ) set;
        char[] vals = values;
        if ( noRemoved() ) {
            for ( int i = keys.length; i-- > 0; ) {
                if ( notFree( keys[ i ] ) ) {
                    hashCode += nullableKeyHashCode( keys[ i ] ) ^
                            Primitives.identityHashCode( vals[ i ] );
                }
            }
        } else {
            for ( int i = keys.length; i-- > 0; ) {
                if ( isFull( keys[ i ] ) ) {
                    hashCode += nullableKeyHashCode( keys[ i ] ) ^
                            Primitives.identityHashCode( vals[ i ] );
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }


    private class ValueView extends View<Character>
            implements HashCharCollection, ReverseCharCollectionOps {

        @Override
        public char getNoEntryValue() {
            return DHashObjCharMap.this.getNoEntryValue();
        }


        @Override
        public boolean removeIf( CharPredicate filter ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean removeIf( Predicate<? super Character> filter ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            } else {
                for( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                        removeAt( i );
                        mc++;
                        changed = true;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean allCharsContainingIn( CharCollection collection ) {
            if ( isEmpty() )
                return true;
            boolean containsAll = true;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                        containsAll = false;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                        containsAll = false;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return containsAll;
        }


        @Override
        public boolean allContainingIn( TCollection<? super Character> collection ) {
            if ( isEmpty() )
                return true;
            boolean containsAll = true;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                        containsAll = false;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                        containsAll = false;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return containsAll;
        }


        @Override
        public boolean reverseAddAllCharsTo( CharCollection collection ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= collection.add( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= collection.add( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super Character> collection ) {
            if ( isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= collection.add( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= collection.add( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean reverseRemoveAllCharsFrom( CharSet s ) {
            if ( this.isEmpty() || s.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= s.remove( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= s.remove( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super Character> s ) {
            if ( this.isEmpty() || s.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        changed |= s.remove( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        changed |= s.remove( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        @NotNull
        public CharIterator iterator() {
            if ( noRemoved() ) {
                return new NoRemovedValueIterator();
            } else {
                return new ValueIterator();
            }
        }


        @Override
        public void forEach( CharConsumer action ) {
            if ( isEmpty() )
                return;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        action.accept( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        action.accept( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
        }


        @Override
        public void forEach( Consumer<? super Character> action ) {
            if ( isEmpty() )
                return;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        action.accept( vals[ i ] );
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        action.accept( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
        }


        @Override
        public boolean testWhile( CharPredicate predicate ) {
            if ( isEmpty() )
                return true;
            boolean terminated = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                        terminated = true;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                        terminated = true;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return !terminated;
        }


        @Override
        public boolean testWhile( Predicate<? super Character> predicate ) {
            if ( isEmpty() )
                return true;
            boolean terminated = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                        terminated = true;
                        break;
                    }
                }
            } else {
                for ( int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                        terminated = true;
                        break;
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return !terminated;
        }


        @Override
        @NotNull
        public Object[] toArray() {
            int size = size();
            Object[] result = new Object[ size ];
            if ( size == 0 )
                return result;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        result[ resultIndex++ ] = vals[ i ];
                }
            } else {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        result[ resultIndex++ ] = vals[ i ];
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return result;
        }


        @Override
        public char[] toCharArray() {
            int size = size();
            char[] result = new char[ size ];
            if ( size == 0 )
                return result;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        result[ resultIndex++ ] = vals[ i ];
                }
            } else {
                for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        result[ resultIndex++ ] = vals[ i ];
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return result;
        }


        @Override
        @SuppressWarnings("unchecked")
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            int size = size();
            if ( a.length < size ) {
                Class<?> tType = a.getClass().getComponentType();
                a = ( T[] ) Array.newInstance( tType, size );
            }
            if ( size == 0 ) {
                if ( a.length > 0 )
                    a[ 0 ] = null;
                return a;
            }
            int resultIndex = 0;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for (int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        a[ resultIndex++ ] = ( T ) Character.valueOf( vals[ i ] );
                }
            } else {
                for (int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        a[ resultIndex++ ] = ( T ) Character.valueOf( vals[ i ] );
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            if ( a.length > resultIndex ) {
                a[ resultIndex ] = null;
            }
            return a;
        }


        @Override
        public char[] toArray( char[] dest ) {
            int size = size();
            if ( dest.length < size ) {
                dest = new char[ size ];
            }
            if ( size == 0 ) {
                if ( dest.length > 0 )
                    dest[ 0 ] = getNoEntryValue();
                return dest;
            }
            int resultIndex = 0;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( noRemoved() ) {
                for (int i = hash.length; i-- > 0; ) {
                    if ( notFree( hash[ i ] ) )
                        dest[ resultIndex++ ] = vals[ i ];
                }
            } else {
                for (int i = hash.length; i-- > 0; ) {
                    if ( isFull( hash[ i ] ) )
                        dest[ resultIndex++ ] = vals[ i ];
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            if ( dest.length > resultIndex ) {
                dest[ resultIndex ] = getNoEntryValue();
            }
            return dest;
        }


        @Override
        public boolean contains( char value ) {
            return containsValue( value );
        }


        @Override
        public boolean remove( char value ) {
            int index = valueIndex( value );
            if ( index >= 0 ) {
                removeAt( index );
                return true;
            } else {
                return false;
            }
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> c ) {
            if ( this == c )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || c.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( c instanceof CharCollection ) {
                CharCollection another = ( CharCollection ) c;
                if ( noRemoved() ) {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( notFree( hash[ i ] ) &&
                                another.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                } else {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( isFull( hash[ i ] ) &&
                                another.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                }
            } else {
                if ( noRemoved() ) {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( notFree( hash[ i ] ) && c.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                } else {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( isFull( hash[ i ] ) && c.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> c ) {
            if ( this == c )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || c.isEmpty() )
                return false;
            boolean changed = false;
            int mc = modCount();
            Object[] hash = set;
            char[] vals = values;
            if ( c instanceof CharCollection ) {
                CharCollection another = ( CharCollection ) c;
                if ( noRemoved() ) {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( notFree( hash[ i ] ) &&
                                !another.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                } else {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( isFull( hash[ i ] ) &&
                                !another.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                }
            } else {
                if ( noRemoved() ) {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( notFree( hash[ i ] ) && !c.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                } else {
                    for( int i = hash.length; i-- > 0; ) {
                        if ( isFull( hash[ i ] ) && !c.contains( vals[ i ] ) ) {
                            removeAt( i );
                            mc++;
                            changed = true;
                        }
                    }
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public final boolean contains( Object o ) {
            return contains( ( char ) ( Character ) o );
        }


        @Override
        public final boolean remove( Object o ) {
            return remove( ( char ) ( Character ) o );
        }


        @Override
        public final boolean add( char value ) {
            throw new UnsupportedOperationException();
        }
    }

    class ValueIterator extends ObjKeyHashIterator implements CharIterator {

        protected final char[] values = DHashObjCharMap.this.values;


        @Override
        public Character value() {
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public Character next() {
            advance();
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public char nextChar() {
            advance();
            return values[ index ];
        }


        @Override
        public char charValue() {
            return values[ index ];
        }
    }


    class NoRemovedValueIterator extends ObjKeyHashNoRemovedIterator
            implements CharIterator {

        protected final char[] values = DHashObjCharMap.this.values;


        @Override
        public Character value() {
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public Character next() {
            advance();
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public char nextChar() {
            advance();
            return values[ index ];
        }


        @Override
        public char charValue() {
            return values[ index ];
        }
    }


    class KeyValueIterator extends ObjKeyHashIterator
            implements CharValueMapIterator<K> {

        protected final char[] values = DHashObjCharMap.this.values;


        @Override
        public char charValue() {
            return values[ index ];
        }


        @Override
        public void setValue( char value ) {
            values[ index ] = value;
        }


        @Override
        public Character value() {
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public void setValue( Character value ) {
            values[ index ] = value;
        }
    }


    class KeyValueNoRemovedIterator extends ObjKeyHashNoRemovedIterator
            implements CharValueMapIterator<K> {

        protected final char[] values = DHashObjCharMap.this.values;


        @Override
        public char charValue() {
            return values[ index ];
        }


        @Override
        public void setValue( char value ) {
            values[ index ] = value;
        }


        @Override
        public Character value() {
            // noinspection boxing
            return values[ index ];
        }


        @Override
        public void setValue( Character value ) {
            values[ index ] = value;
        }
    }


    private class EntryView extends ObjHashBackedEntryView {


        @Override
        @NotNull
        public TIterator<Map.Entry<K, Character>> iterator() {
            if ( noRemoved() ) {
                return new NoRemovedEntryIterator();
            } else {
                return new EntryIterator();
            }
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean remove( Object o ) {
            Map.Entry<K, Character> e = ( Map.Entry<K, Character> ) o;
            int index = index( e.getKey() );
            if ( index >= 0 ) {
                if ( e.getValue() == values[ index ] ) {
                    removeAt( index );
                    return true;
                }
            }
            return false;
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean contains( Object o ) {
            Map.Entry<K, Character> e = ( Map.Entry<K, Character> ) o;
            int index = index( e.getKey() );
            return index >= 0 && e.getValue() == values[ index ];
        }


        @Override
        protected Map.Entry<K, Character> entryAt( int index ) {
            return new Entry( index );
        }

    }


    private class EntryIterator extends ObjKeyHashIterator
            implements TIterator<Map.Entry<K, Character>> {
        private Entry entry;

        @Override
        public Map.Entry<K, Character> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<K, Character> value() {
            return entry;
        }
    }


    private class NoRemovedEntryIterator extends ObjKeyHashNoRemovedIterator
            implements TIterator<Map.Entry<K, Character>> {
        private Entry entry;

        @Override
        public Map.Entry<K, Character> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<K, Character> value() {
            return entry;
        }
    }


    private class Entry extends ObjHashEntry {

        Entry( int index ) {
            super( index );
        }


        @Override
        public Character getValue() {
            return values[ index ];
        }


        @Override
        public Character setValue( Character newValue ) {
            char oldValue = getValue();
            values[ index ] = newValue;
            return oldValue;
        }


        @SuppressWarnings("unchecked")
        public boolean equals( Object o ) {
            if ( o == null )
                return false;
            Map.Entry e2;
            K key2;
            char value2;
            try {
                e2 = ( Map.Entry ) o;
                key2 = ( K ) e2.getKey();
                value2 = ( Character ) e2.getValue();
            } catch ( ClassCastException e ) {
                return false;
            } catch ( NullPointerException e ) {
                return false;
            }
            int i = index;
            return nullableKeyEquals( ( K ) set[ i ], key2 ) &&
                    values[ i ] == value2;
        }


        public int hashCode() {
            int i = index;
            //noinspection unchecked, boxing
            return nullableKeyHashCode( ( K ) set[ i ] ) ^
                    Primitives.identityHashCode( values[ i ] );
        }

    }

}
