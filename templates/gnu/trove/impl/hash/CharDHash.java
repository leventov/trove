// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.CharIterator;
import gnu.trove.impl.Primitives;
import gnu.trove.map.CharKeyMapIterator;

//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


public abstract class CharDHash<V> extends StatesDHash {

    char[] set;


    @Override
    void init( int capacity ) {
        super.init( capacity );
        set = new char[ capacity ];
    }


    // Manual inlining of small methods (isFull, isFree, nextIndex,
    // firstIndex, step) simply works faster.

    // Splitting index() and insertion methods into direct hit and probing loop
    // allows them to be inlined with default FreqInlineSize (325 byte codes).
    // It works faster.

    int index( char key ) {
        // Local copies of fields to prevent repetitive loading.
        byte[] states = this.states;
        // set variable is not used in "first state is free" case,
        // but loading set field immediately before its usage
        // in "first state is full" case increases latency.
        // Loading states and set fields one after another is optimal.
        // They goes one after another in object layout also.
        char[] set = this.set;
        int capacity = states.length;
        // Hash should be positive, because index = hash % capacity
        // should be positive.
        /* if byte|short|int|long|float|double elem */
            int hash = Primitives.positiveHash( key );
        /* elif char // // Considerably simplifies resulting bytecode.
            int hash = key;
        // endif */
        int index = hash % capacity; // firstIndex
        byte state = states[ index ];
        // In index operation, we suppose target key is most likely
        // already stored in the hash. Most probable branch goes first:
        if ( state < 0 ) { // FULL state
            if ( set[ index ] == key )
                return index;
        }
        else if ( state == FREE ) {
            return -1;
        }
        // Passing copies of all fields as arguments is faster than calling
        // a method with shorter signature and reloading fields in its body.
        return indexProbingLoop( key, states, set, capacity, hash, index );
    }


    private int indexProbingLoop( char key, byte[] states, char[] set,
                                  int capacity, int hash, int index ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                if ( states[ index ] < 0 ) { // FULL state
                    if ( set[ index ] == key ) {
                        return index;
                    }
                }
                else { // not FULL => FREE
                    return -1;
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                byte state = states[ index ];
                if ( state < 0 ) { // FULL state
                    if ( set[ index ] == key )
                        return index;
                }
                else if ( state == FREE ) {
                    return -1;
                }
            }
        }
    }


    InsertionIndex insertionIndex( char key ) {
        /* if float|double elem //
        if ( key != key )
            throw new IllegalArgumentException("NaN keys are not supported");
        // endif */

        byte[] states = this.states;
        char[] set = this.set;
        int capacity = states.length;
        // Hash should be positive, because index = hash % capacity
        // should be positive.
        /* if byte|short|int|long|float|double elem */
            int hash = Primitives.positiveHash( key );
        /* elif char // // Considerably simplifies resulting bytecode.
            int hash = key;
        // endif */
        int index = hash % capacity; // firstIndex
        byte state = states[ index ];
        int firstRemoved;
        // In index operation, we suppose target value is most likely
        // already stored in the hash. Most probable branch goes first:
        if ( state < 0 ) { // FULL state
            if ( set[ index ] == key ) return InsertionIndex.existing( index );
            else firstRemoved = -1;
        }
        else if ( state == FREE ) {
            return InsertionIndex.free( index );
        }
        else { // state is REMOVED
            firstRemoved = index;
        }

        return insertionIndexProbingLoop(
                key, states, set, capacity, hash, index, firstRemoved );
    }


    private InsertionIndex insertionIndexProbingLoop( char key, byte[] states,
            char[] set, int capacity, int hash, int index, int firstRemoved ) {
        int step = 1 + ( hash % ( capacity - 2 ) );
        if ( firstRemoved < 0 && noRemoved() ) {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                if ( states[ index ] < 0 ) { // FULL state
                    if ( set[ index ] == key )
                        return InsertionIndex.existing( index );
                }
                else {
                    return InsertionIndex.free( index );
                }
            }
        }
        else {
            for ( ; ; ) {
                if ( ( index -= step ) < 0 ) index += capacity; // nextIndex
                byte state = states[ index ];
                if ( state < 0 ) { // FULL state
                    if ( set[ index ] == key )
                        return InsertionIndex.existing( index );
                }
                else if ( state == FREE ) {
                    if ( firstRemoved < 0 ) return InsertionIndex.free( index );
                    else return InsertionIndex.removed( firstRemoved );
                }
                // state is REMOVED
                else if ( firstRemoved < 0 ) {
                    firstRemoved = index;
                }
            }
        }
    }


    abstract class CharKeyHashIterator<V> extends StatesIterator
        implements CharKeyMapIterator<V> {

        protected final char[] set = CharDHash.this.set;


        @Override
        public final char charKey() {
            return set[ index ];
        }


        @Override
        public Character key() {
            return set[ index ];
        }
    }


    final class CharHashIterator extends StatesIterator
            implements CharIterator {

        protected final char[] set = CharDHash.this.set;


        public CharHashIterator() {}


        @Override
        public Character value() {
            // noinspection boxing
            return set[ index ];
        }


        @Override
        public Character next() {
            advance();
            // noinspection boxing
            return set[ index ];
        }


        @Override
        public char nextChar() {
            advance();
            return set[ index ];
        }


        @Override
        public char charValue() {
            return set[ index ];
        }
    }
}
