// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.*;
import gnu.trove.map.*;
import gnu.trove.map.CharKeyMapIterator;
import gnu.trove.map.hash.HashCharObjMap;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashObjSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Array;
import java.util.*;

public class DHashCharObjMap<V> extends CharObjDHash<V>
        implements HashCharObjMap<V>, ReverseCharObjMapOps<V> {


    public DHashCharObjMap() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashCharObjMap( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashCharObjMap( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashCharObjMap( CharObjEntrySequence<? extends V> entrySequence ) {
        this( entrySequence, entrySequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashCharObjMap( CharObjEntrySequence<? extends V> entrySequence,
            int expectedSize, float loadFactor ) {
        if ( entrySequence instanceof DHashCharObjMap ) {
            DHashCharObjMap m = ( DHashCharObjMap ) entrySequence;
            setUp( loadFactor, m.size(), false );
            byte[] hash = m.states;
            char[] keys = m.set;
            // noinspection unchecked
            V[] vals = ( V[] ) m.values;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    insertForRehash( keys[ i ], vals[ i ] );
            }
        }
        else {
            setUp( loadFactor,expectedSize, true );
            entrySequence.forEach( new CharObjConsumer<V>() {
                @Override
                public void accept( char key, V value ) {
                    DHashCharObjMap.this.justPut( key, value );
                }
            } );
        }
    }


    @Override
    public void clear() {
        super.clear();
        Arrays.fill( values, null );
    }


    @Override
    void removeAt( int index ) {
        values[ index ] = null; // let GC do its work
        super.removeAt( index );
    }


    protected boolean valuesEquals( @NotNull V a, @Nullable V b ) {
        return a.equals( b );
    }


    final boolean nullableValuesEquals( @Nullable V a, @Nullable V b ) {
        return a == b || ( a != null && valuesEquals( a, b ) );
    }


    protected int valueHashCode( @NotNull V value ) {
        return value.hashCode();
    }


    final int nullableValueHashCode( @Nullable V value ) {
        return value != null ? valueHashCode( value ) : 0;
    }


    public int hashCode() {
        if ( isEmpty() )
            return 0;
        // noinspection unchecked
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        int hashCode = 0;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                hashCode += Primitives.identityHashCode( keys[ i ] ) ^
                        nullableValueHashCode( vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }


    @Override
    public boolean containsEntry( char key, V value ) {
        int index = index( key );
        return index >= 0 && nullableValuesEquals( values[ index ], value );
    }


    @Override
    public boolean containsEntry( Character key, V value ) {
        return containsEntry( ( char ) key, value );
    }


    @Override
    public boolean containsValue( Object value ) {
        return valueIndex( value ) >= 0;
    }


    @SuppressWarnings("unchecked")
    private int valueIndex( Object value ) {
        if ( isEmpty() )
            return -1;
        byte[] hash = states;
        V[] vals = this.values;
        int mc = modCount();
        int index = -1;
        // special case null values so that we don't have to
        // perform null checks before every call to equals()
        if ( value != null ) {
            V v = ( V ) value;
            for (int i = hash.length; i-- > 0;) {
                if ( ( isFull( hash[ i ] ) ) &&
                        ( valuesEquals( v, vals[ i ] ) ) ) {
                    index = i;
                    break;
                }
            }

        } else {
            for (int i = hash.length; i-- > 0;) {
                if ( isFull( hash[ i ] ) && vals[ i ] == null ) {
                    index = i;
                    break;
                }
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return index;
    }


    @Override
    public V get( char key ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : null;
    }


    @Override
    public V get( Object key ) {
        return get( ( char ) ( Character ) key );
    }


    @Override
    public V getOrDefault( char key, V defaultValue ) {
        int index = index( key );
        return index >= 0 ? values[ index ] : defaultValue;
    }


    @Override
    public V getOrDefault( Object key, V defaultValue ) {
        return getOrDefault( ( char ) ( Character ) key, defaultValue );
    }


    @Override
    public V remove( char key ) {
        int index = index( key );
        if ( index >= 0 ) {
            V previous = values[ index ];
            removeAt( index );
            return previous;
        } else {
            return null;
        }
    }


    @Override
    public V remove( Object key ) {
        return remove( ( char ) ( Character ) key );
    }


    @Override
    @SuppressWarnings("unchecked")
    public boolean remove( char key, Object value ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( nullableValuesEquals( values[ index ], ( V ) value ) ) {
                removeAt( index );
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean remove( Object key, Object value ) {
        return remove( ( char ) ( Character ) key, value );
    }


    @Override
    public V put( char key, V value ) {
        int index = insert( key, value );
        if ( index < 0 ) {
            return null;
        } else {
            V previous = values[ index ];
            values[ index ] = value;
            return previous;
        }
    }


    @Override
    public V put( Character key, V value ) {
        return put( ( char ) key, value );
    }


    @Override
    public V putIfAbsent( char key, V value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            if (oldValue != null) {
                return oldValue;
            } else {
                values[ index ] = value;
                return null;
            }
        } else {
            return null;
        }
    }


    @Override
    public V putIfAbsent( Character key, V value ) {
        return putIfAbsent( ( char ) key, value );
    }


    @Override
    public void justPut( char key, V value ) {
        int index = insert( key, value );
        if ( index >= 0 ) {
            values[ index ] = value;
        }
    }


    @Override
    public void justPut( Character key, V value ) {
        justPut( ( char ) key, value );
    }


    @Override
    public V compute( char key,
            CharObjFunction<? super V, ? extends V> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            V newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            V newValue = remappingFunction.apply( key, null );
            if ( newValue != null ) {
                insertAt( insertionIndex, key, newValue );
                return newValue;
            } else {
                return null;
            }
        }
    }


    @Override
    public V compute( Character key,
            BiFunction<? super Character, ? super V, ? extends V> remappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            V newValue = remappingFunction.apply( key, oldValue );
            if ( newValue != null ) {
                values[ index ] = newValue;
                return newValue;
            } else {
                removeAt( index );
                return null;
            }
        } else {
            V newValue = remappingFunction.apply( key, null );
            if ( newValue != null ) {
                insertAt( insertionIndex, k, newValue );
                return newValue;
            } else {
                return null;
            }
        }
    }


    @Override
    public V computeIfPresent( char key,
            CharObjFunction<? super V, ? extends V> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply( key, oldValue );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            }
        }
        return null;
    }


    @Override
    public V computeIfPresent( Character key,
            BiFunction<? super Character, ? super V, ? extends V> remappingFunction ) {
        int index = index( key );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply( key, oldValue );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            }
        }
        return null;
    }


    @Override
    public V computeIfAbsent( char key,
            CharFunction<? extends V> mappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.absent() ) {
            V newValue = mappingFunction.apply( key );
            if ( newValue != null ) {
                insertAt( insertionIndex, key, newValue );
                return newValue;
            } else {
                return null;
            }
        } else {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                return oldValue;
            } else {
                V newValue = mappingFunction.apply( key );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    return null;
                }
            }
        }
    }


    @Override
    public V computeIfAbsent( Character key,
            Function<? super Character, ? extends V> mappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        if ( insertionIndex.absent() ) {
            V newValue = mappingFunction.apply( key );
            if ( newValue != null ) {
                insertAt( insertionIndex, k, newValue );
                return newValue;
            } else {
                return null;
            }
        } else {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                return oldValue;
            } else {
                V newValue = mappingFunction.apply( key );
                if ( newValue != null ) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    return null;
                }
            }
        }
    }


    @Override
    public V merge( char key, V value,
            BiFunction<? super V, ? super V, ? extends V> remappingFunction ) {
        InsertionIndex insertionIndex = insertionIndex( key );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply(oldValue, value);
                if (newValue != null) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            } else {
                if ( value != null ) {
                    values[ index ] = value;
                    return value;
                } else {
                    return null;
                }
            }
        } else {
            if ( value != null ) {
                insertAt( insertionIndex, key, value );
                return value;
            } else {
                return null;
            }
        }
    }


    @Override
    public V merge( Character key, V value,
            BiFunction<? super V, ? super V, ? extends V> remappingFunction ) {
        char k = key;
        InsertionIndex insertionIndex = insertionIndex( k );
        if ( insertionIndex.existing() ) {
            int index = insertionIndex.get();
            V oldValue = values[ index ];
            if ( oldValue != null ) {
                V newValue = remappingFunction.apply(oldValue, value);
                if (newValue != null) {
                    values[ index ] = newValue;
                    return newValue;
                } else {
                    removeAt( index );
                    return null;
                }
            } else {
                if ( value != null ) {
                    values[ index ] = value;
                    return value;
                } else {
                    return null;
                }
            }
        } else {
            if ( value != null ) {
                insertAt( insertionIndex, k, value );
                return value;
            } else {
                return null;
            }
        }
    }


    @Override
    public V replace( char key, V value ) {
        int index = index( key );
        if ( index >= 0 ) {
            V oldValue = values[ index ];
            values[ index ] = value;
            return oldValue;
        }
        return null;
    }


    @Override
    public V replace( Character key, V value ) {
        return replace( ( char ) key, value );
    }


    @Override
    public boolean replace( char key, V oldValue, V newValue ) {
        int index = index( key );
        if ( index >= 0 ) {
            if ( nullableValuesEquals( values[ index ], oldValue ) ) {
                values[ index ] = newValue;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean replace( Character key, V oldValue, V newValue ) {
        return replace( ( char ) key, oldValue, newValue );
    }


    @Override
    public void forEach( CharObjConsumer<? super V> action ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void forEach( BiConsumer<? super Character, ? super V> action ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean testWhile( CharObjPredicate<? super V> predicate ) {
        if ( isEmpty() )
            return true;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean terminated = false;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !predicate.test( keys[ i ], vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public boolean testWhile( BiPredicate<? super Character, ? super V> predicate ) {
        if ( isEmpty() )
            return true;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean terminated = false;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !predicate.test( keys[ i ], vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }


    @Override
    public void replaceAll( CharObjFunction<? super V, ? extends V> function ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                vals[ i ] = function.apply( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void replaceAll(
            BiFunction<? super Character, ? super V, ? extends V> function ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                vals[ i ] = function.apply( keys[ i ], vals[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean allEntriesContainingIn( CharObjMap<? super V> map ) {
        if ( isEmpty() )
            return true;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean containsAll = true;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public boolean allEntriesContainingIn( TMap<? super Character, ? super V> map ) {
        if ( isEmpty() )
            return true;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean containsAll = true;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    !map.containsEntry( keys[ i ], vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }


    @Override
    public void reversePutAllTo( CharObjMap<? super V> map ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = this.values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                map.justPut( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public void reversePutAllTo( TMap<? super Character, ? super V> map ) {
        if ( isEmpty() )
            return;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = this.values;
        int mc = modCount();
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                map.justPut( keys[ i ], vals[ i ] );
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public boolean removeIf( CharObjPredicate<? super V> filter ) {
        if ( isEmpty() )
            return false;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean changed = false;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    filter.test( keys[ i ], vals[ i ] ) ) {
                removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public boolean removeIf( BiPredicate<? super Character, ? super V> filter ) {
        if ( isEmpty() )
            return false;
        byte[] hash = states;
        char[] keys = set;
        V[] vals = values;
        int mc = modCount();
        boolean changed = false;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) &&
                    filter.test( keys[ i ], vals[ i ] ) ) {
                removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }


    @Override
    public boolean containsAllEntries( Map<? extends Character, ? extends V> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        if ( map instanceof ReverseCharObjMapOps ) {
            //noinspection unchecked
            return ( ( ReverseCharObjMapOps ) map ).allEntriesContainingIn( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            return ( ( ReverseMapOps ) map ).allEntriesContainingIn( this );
        } else {
            return Maps.containsAllEntries( this, map );
        }
    }


    @Override
    public void putAll( @NotNull Map<? extends Character, ? extends V> map ) {
        if ( this == map )
            throw new IllegalArgumentException();
        ensureCapacity( map.size() );
        if ( map instanceof ReverseCharObjMapOps ) {
            //noinspection unchecked
            ( ( ReverseCharObjMapOps ) map ).reversePutAllTo( this );
        } else if ( map instanceof ReverseMapOps ) {
            //noinspection unchecked
            ( ( ReverseMapOps ) map ).reversePutAllTo( this );
        } else {
            Maps.putAll( this, map );
        }
    }


    @Override
    @NotNull
    public CharKeyMapIterator<V> mapIterator() {
        return new IteratorImpl();
    }


    private class IteratorImpl extends CharKeyHashIterator<V> {

        protected final V[] values = DHashCharObjMap.this.values;


        @Override
        public V value() {
            return values[ index ];
        }


        @Override
        public void setValue( V value ) {
            values[ index ] = value;
        }
    }


    @Override
    @NotNull
    public HashCollection<V> values() {
        return new ValueView();
    }


    private class ValueView extends View<V> {

        @Override
        public boolean removeIf( Predicate<? super V> filter ) {
            if ( isEmpty() )
                return false;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean changed = false;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean allContainingIn( TCollection<? super V> collection ) {
            if ( isEmpty() )
                return true;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean containsAll = true;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return containsAll;
        }


        @Override
        public boolean reverseAddAllTo( TCollection<? super V> collection ) {
            if ( isEmpty() )
                return false;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean changed = false;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= collection.add( vals[ i ] );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean reverseRemoveAllFrom( TSet<? super V> set ) {
            if ( this.isEmpty() || set.isEmpty() )
                return false;
            boolean changed = false;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= set.remove( vals[ i ] );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean contains( Object o ) {
            return valueIndex( o ) >= 0;
        }


        @Override
        @NotNull
        public Object[] toArray() {
            int size = size();
            Object[] result = new Object[ size ];
            if ( size == 0 )
                return result;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    result[ resultIndex++ ] = vals[ i ];
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return result;
        }


        @Override
        @SuppressWarnings("unchecked")
        @NotNull
        public <T> T[] toArray( @NotNull T[] a ) {
            int size = size();
            if ( a.length < size ) {
                Class<?> tType = a.getClass().getComponentType();
                a = ( T[] ) Array.newInstance( tType, size );
            }
            if ( size == 0 ) {
                if ( a.length > 0 )
                    a[ 0 ] = null;
                return a;
            }
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            int resultIndex = 0;
            for (int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    a[ resultIndex++ ] = ( T ) vals[ i ];
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            if ( a.length > resultIndex ) {
                a[ resultIndex ] = null;
            }
            return a;
        }


        @Override
        public boolean remove( Object o ) {
            int index = valueIndex( o );
            if ( index >= 0 ) {
                removeAt( index );
                return true;
            } else {
                return false;
            }
        }


        @Override
        public boolean containsAll( @NotNull Collection<?> c ) {
            return HashBlocks.containsAll( this, c );
        }


        @Override
        public boolean removeAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( this.isEmpty() || another.isEmpty() )
                return false;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean changed = false;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && another.contains( vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        public boolean retainAll( @NotNull Collection<?> another ) {
            if ( this == another )
                throw new IllegalArgumentException();
            if ( another.isEmpty() ) {
                if ( this.isEmpty() ) {
                    return false;
                } else {
                    this.clear();
                    return true;
                }
            }
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean changed = false;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !another.contains( vals[ i ] ) ) {
                    removeAt( i );
                    mc++;
                    changed = true;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return changed;
        }


        @Override
        @NotNull
        public TIterator<V> iterator() {
            return new ValueIterator();
        }


        @Override
        public void forEach( Consumer<? super V> action ) {
            if ( isEmpty() )
                return;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    action.accept( vals[ i ] );
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
        }


        @Override
        public boolean testWhile( Predicate<? super V> predicate ) {
            if ( isEmpty() )
                return true;
            byte[] hash = states;
            V[] vals = values;
            int mc = modCount();
            boolean terminated = false;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
            if ( mc != modCount() )
                throw new ConcurrentModificationException();
            return !terminated;
        }
    }


    private class ValueIterator extends StatesIterator implements TIterator<V> {

        protected final V[] values = DHashCharObjMap.this.values;


        @Override
        public V value() {
            return values[ index ];
        }


        @Override
        public V next() {
            advance();
            return values[ index ];
        }
    }


    @Override
    @NotNull
    public HashObjSet<Map.Entry<Character, V>> entrySet() {
        return new EntryView();
    }


    private class EntryView extends CharHashBackedEntryView {

        @Override
        @NotNull
        public TIterator<Map.Entry<Character, V>> iterator() {
            return new EntryIterator();
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean remove( Object o ) {
            Map.Entry<Character, V> e = ( Map.Entry<Character, V> ) o;
            int index = index( e.getKey() );
            if ( index >= 0 ) {
                if ( nullableValuesEquals( e.getValue(), values[ index ] ) ) {
                    removeAt( index );
                    return true;
                }
            }
            return false;
        }


        @Override
        @SuppressWarnings("unchecked")
        public boolean contains( Object o ) {
            Map.Entry<Character, V> e = ( Map.Entry<Character, V> ) o;
            int index = index( e.getKey() );
            return index >= 0 &&
                    nullableValuesEquals( values[ index ], e.getValue() );
        }


        @Override
        protected Map.Entry<Character, V> entryAt( int index ) {
            return new Entry( index );
        }
    }


    private class EntryIterator extends StatesIterator
            implements TIterator<Map.Entry<Character, V>> {
        private Entry entry;

        @Override
        public Map.Entry<Character, V> next() {
            advance();
            return entry = new Entry( index );
        }


        @Override
        public Map.Entry<Character, V> value() {
            return entry;
        }
    }


    private class Entry extends CharHashEntry {

        Entry( int index ) {
            super( index );
        }


        @Override
        public V getValue() {
            return values[ index ];
        }


        @Override
        public V setValue( V newValue ) {
            V oldValue = getValue();
            values[ index ] = newValue;
            return oldValue;
        }


        @SuppressWarnings("unchecked")
        public boolean equals( Object o ) {
            if ( o == null )
                return false;
            Map.Entry e2;
            char key2;
            V value2;
            try {
                e2 = ( Map.Entry ) o;
                key2 = ( Character ) e2.getKey();
                value2 = ( V ) e2.getValue();
            } catch ( ClassCastException e ) {
                return false;
            } catch ( NullPointerException e ) {
                return false;
            }
            int i = index;
            return set[ i ] == key2 &&
                    nullableValuesEquals( values[ i ], value2 );
        }


        public int hashCode() {
            int i = index;
            return Primitives.identityHashCode( set[ i ] ) ^
                    nullableValueHashCode( values[ i ] );
        }

    }
}
