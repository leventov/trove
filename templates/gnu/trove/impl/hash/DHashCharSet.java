// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.CharIterator;
import gnu.trove.TCollection;
import gnu.trove.CharSequence;
import gnu.trove.function.*;
import gnu.trove.impl.*;
import gnu.trove.impl.Collections;
import gnu.trove.CharCollection;
import gnu.trove.set.CharSet;
import gnu.trove.set.TSet;
import gnu.trove.set.hash.HashCharSet;
import org.jetbrains.annotations.NotNull;

import java.util.*;


//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


public class DHashCharSet extends CharSetDHash<Character>
        implements HashCharSet, ReverseCharCollectionOps {


    public DHashCharSet() {
        this( DEFAULT_EXPECTED_INITIAL_SIZE );
    }


    public DHashCharSet( int expectedSize ) {
        this( expectedSize, DEFAULT_LOAD_FACTOR );
    }


    public DHashCharSet( int expectedSize, float loadFactor ) {
        setUp( loadFactor, expectedSize, true );
    }


    public DHashCharSet( CharSequence sequence ) {
        this( sequence, sequence.size(), DEFAULT_LOAD_FACTOR );
    }


    public DHashCharSet( CharSequence sequence,
            int expectedSize, float loadFactor ) {
        if ( sequence instanceof DHashCharSet ) {
            DHashCharSet c = ( DHashCharSet ) sequence;
            setUp( loadFactor, sequence.size(), false );
            byte[] hash = c.states;
            char[] set = c.set;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    insertForRehash( set[ i ] );
            }
        }
        else if ( sequence instanceof CharDHashBackedMap.KeyView ) {
            CharDHashBackedMap.KeyView c = ( CharDHashBackedMap.KeyView ) sequence;
            setUp( loadFactor, sequence.size(), false );
            byte[] hash = c.getHash().states;
            char[] set = c.getHash().set;
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    insertForRehash( set[ i ] );
            }
        }
        else {
            // just expected size because the sequence is not a set
            setUp( loadFactor, expectedSize, true );
            sequence.forEach( new CharConsumer() {
                @Override
                public void accept( char value ) {
                    DHashCharSet.this.add( value );
                }
            } );
        }
    }


    public DHashCharSet( char[] elements ) {
        this( elements.length );
        for ( char e : elements ) {
            add( e );
        }
    }


    @Override
    protected void rehash( int newCapacity ) {
        int mc = modCount();
        int oldSize = size();
        byte[] oldStates = states;
        char[] oldSet = set;

        initForRehash( newCapacity );
        mc++; // modCount is incremented in initForRehash()
        if ( oldSize == 0 )
            return;
        // Process entries from the old array, skipping free and removed slots.
        // Put the values into the appropriate place in the new array.
        for ( int i = oldStates.length; i-- > 0; ) {
            if ( isFull( oldStates[ i ] ) ) {
                insertForRehash( oldSet[ i ] );
            }
        }
        if ( mc != modCount() )
            throw new ConcurrentModificationException();
    }


    @Override
    public char getNoEntryValue() {
        return Constants.DEFAULT_CHAR_NO_ENTRY_VALUE;
    }


    public int hashCode() {
        return HashBlocks.setHashCode( this );
    }


    public boolean equals( Object obj ) {
        return Collections.setEquals( this, obj );
    }


    @Override
    public boolean contains( char value ) {
        return index( value ) >= 0;
    }


    @Override
    public char[] toCharArray() {
        return HashBlocks.toCharArray( this, set );
    }


    @Override
    public char[] toArray( char[] dest ) {
        return HashBlocks.toCharArray(
                this, set, getNoEntryValue(), dest );
    }


    @Override
    public boolean add( char value ) {
        return insert( value ) < 0;
    }


    @Override
    public boolean remove( char value ) {
        int index = index( value );
        if ( index >= 0 ) {
            removeAt( index );
            return true;
        }
        return false;
    }


    @Override
    public boolean removeIf( CharPredicate filter ) {
        return HashBlocks.removeCharsIf( this, set, filter );
    }


    @Override
    public boolean allCharsContainingIn( CharCollection collection ) {
        return HashBlocks.reverseAllCharsContainingIn( this, set, collection );
    }


    @Override
    public boolean reverseAddAllCharsTo( CharCollection collection ) {
        return HashBlocks.reverseAddAllChars( this, set, collection );
    }


    @Override
    public boolean reverseRemoveAllCharsFrom( CharSet s ) {
        return HashBlocks.reverseRemoveAllChars( this, set, s );
    }


    @Override
    public boolean contains( Object o ) {
        return contains( ( ( Character ) o ).charValue() );
    }


    @Override
    @NotNull
    public CharIterator iterator() {
        return new CharHashIterator();
    }


    @Override
    public void forEach( Consumer<? super Character> action ) {
        HashBlocks.forEachChar( this, set, action );
    }


    @Override
    public boolean testWhile( Predicate<? super Character> predicate ) {
        return HashBlocks.testCharsWhile( this, set, predicate );
    }


    @Override
    @NotNull
    public Object[] toArray() {
        return HashBlocks.toArray( this, set );
    }


    @Override
    @NotNull
    public <T> T[] toArray( @NotNull T[] a ) {
        return HashBlocks.toArray( this, set, a );
    }


    @Override
    public boolean add( Character character ) {
        return add( character.charValue() );
    }


    @Override
    public boolean remove( Object o ) {
        return remove( ( ( Character ) o ).charValue() );
    }


    @Override
    public boolean containsAll( @NotNull Collection<?> c ) {
        return HashBlocks.containsAll( this, c );
    }


    @Override
    public boolean addAll( @NotNull Collection<? extends Character> c ) {
        if ( this == c )
            throw new IllegalArgumentException();
        ensureCapacity( c.size() );
        if ( c instanceof ReverseCharCollectionOps ) {
            return ( ( ReverseCharCollectionOps ) c ).reverseAddAllCharsTo( this );
        } else if ( c instanceof ReverseCollectionOps ) {
            // noinspection unchecked
            return ( ( ReverseCollectionOps ) c ).reverseAddAllTo( this );
        } else {
            return Collections.addAll( this, c );
        }
    }


    @Override
    public boolean removeAll( @NotNull Collection<?> c ) {
        return HashBlocks.removeAllCharsFromSet( this, this, set, c );
    }


    @Override
    public boolean retainAll( @NotNull Collection<?> c ) {
        return HashBlocks.retainAllChars( this, this, set, c );
    }


    @Override
    public void forEach( CharConsumer action ) {
        HashBlocks.forEachChar( this, set, action );
    }


    @Override
    public boolean testWhile( CharPredicate predicate ) {
        return HashBlocks.testCharsWhile( this, set, predicate );
    }


    @Override
    public boolean removeIf( Predicate<? super Character> filter ) {
        return HashBlocks.removeCharsIf( this, set, filter );
    }


    @Override
    public boolean allContainingIn( TCollection<? super Character> collection ) {
        return HashBlocks.reverseAllCharsContainingIn( this, set, collection );
    }


    @Override
    public boolean reverseAddAllTo( TCollection<? super Character> collection ) {
        return HashBlocks.reverseAddAllChars( this, set, collection );
    }


    @Override
    public boolean reverseRemoveAllFrom( TSet<? super Character> s ) {
        return HashBlocks.reverseRemoveAllChars( this, set, s );
    }
}