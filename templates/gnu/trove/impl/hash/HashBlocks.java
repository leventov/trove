// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.impl.hash;

import gnu.trove.*;
import gnu.trove.function.*;
import gnu.trove.impl.Collections;
import gnu.trove.impl.*;
import gnu.trove.set.*;
import gnu.trove.set.hash.*;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Array;
import java.util.*;

import static gnu.trove.impl.hash.ObjDHash.isFull;
import static gnu.trove.impl.hash.ObjDHash.notFree;
import static gnu.trove.impl.hash.StatesDHash.isFull;

public final class HashBlocks {

    private HashBlocks() {}

    //////////////////////////////////////////
    // Test While

    public static <E> boolean testWhile(
            ObjDHash objHash, E[] vals,
            Predicate<? super E> predicate ) {
        if ( objHash.isEmpty() )
            return true;
        boolean terminated = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                    terminated = true;
                    break;
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }

/* char elem  */

    public static boolean testCharsWhile( StatesDHash primHash, char[] vals,
            CharPredicate predicate ) {
        if ( primHash.isEmpty() )
            return true;
        int mc = primHash.modCount();
        boolean terminated = false;
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }

    public static boolean testCharsWhile( StatesDHash primHash, char[] vals,
            Predicate<? super Character> predicate ) {
        if ( primHash.isEmpty() )
            return true;
        boolean terminated = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && !predicate.test( vals[ i ] ) ) {
                terminated = true;
                break;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return !terminated;
    }

/* end */


    ///////////////////////////////////////////
    // For Each

    public static <E> void forEach( ObjDHash objHash, E[] vals,
            Consumer<? super E> action ) {
        if ( objHash.isEmpty() )
            return;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) )
                    action.accept( vals[ i ] );
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    action.accept( vals[ i ] );
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
    }

/* char elem  */

    public static void forEachChar( StatesDHash primHash, char[] vals,
            CharConsumer action ) {
        if ( primHash.isEmpty() )
            return;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
    }

    public static void forEachChar( StatesDHash primHash, char[] vals,
            Consumer<? super Character> action ) {
        if ( primHash.isEmpty() )
            return;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                action.accept( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
    }

/* end */


    /////////////////////////////////////////////
    // To Array

    public static Object[] toArray( ObjDHash objHash, Object[] vals ) {
        int size = objHash.size();
        Object[] result = new Object[ size ];
        if ( size == 0 )
            return result;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                if ( notFree( hash[ i ] ) )
                    result[ resultIndex++ ] = vals[ i ];
            }
        } else {
            for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    result[ resultIndex++ ] = vals[ i ];
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return result;
    }

/* char elem  */

    public static char[] toCharArray( StatesDHash primHash, char[] vals ) {
        int size = primHash.size();
        char[] result = new char[ size ];
        if ( size == 0 )
            return result;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                result[ resultIndex++ ] = vals[ i ];
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return result;
    }

    public static Object[] toArray( StatesDHash primHash, char[] vals ) {
        int size = primHash.size();
        Object[] result = new Object[ size ];
        if ( size == 0 )
            return result;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length, resultIndex = 0; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                result[ resultIndex++ ] = vals[ i ];
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return result;
    }

/* end */


    @SuppressWarnings("unchecked")
    public static <T> T[] toArray( ObjDHash objHash, Object[] vals,
            @NotNull T[] a ) {
        int size = objHash.size();
        if ( a.length < size ) {
            Class<?> tType = a.getClass().getComponentType();
            a = ( T[] ) Array.newInstance( tType, size );
        }
        if ( size == 0 ) {
            if ( a.length > 0 )
                a[ 0 ] = null;
            return a;
        }
        int resultIndex = 0;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for (int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) )
                    a[ resultIndex++ ] = ( T ) vals[ i ];
            }
        } else {
            for (int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    a[ resultIndex++ ] = ( T ) vals[ i ];
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        if ( a.length > resultIndex ) {
            a[ resultIndex ] = null;
        }
        return a;
    }

/* char elem  */

    public static char[] toCharArray(
            StatesDHash primHash, char[] vals, char noEntryValue,
            char[] a ) {
        int size = primHash.size();
        if ( a.length < size ) {
            a = new char[ size ];
        }
        if ( size == 0 ) {
            if ( a.length > 0 )
                a[ 0 ] = noEntryValue;
            return a;
        }
        int resultIndex = 0;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for (int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                a[ resultIndex++ ] = vals[ i ];
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        if ( a.length > resultIndex ) {
            a[ resultIndex ] = noEntryValue;
        }
        return a;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] toArray( StatesDHash primHash, char[] vals,
            @NotNull T[] a ) {
        int size = primHash.size();
        if ( a.length < size ) {
            Class<?> tType = a.getClass().getComponentType();
            a = ( T[] ) Array.newInstance( tType, size );
        }
        if ( size == 0 ) {
            if ( a.length > 0 )
                a[ 0 ] = null;
            return a;
        }
        int resultIndex = 0;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for (int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                a[ resultIndex++ ] = ( T ) Character.valueOf( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        if ( a.length > resultIndex ) {
            a[ resultIndex ] = null;
        }
        return a;
    }

/* end */


    ////////////////////////////////////////////////
    // Contains All

    public static <E> boolean containsAll( TCollection<E> collection,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        if ( another instanceof ReverseCollectionOps ) {
            // noinspection unchecked
            return ( ( ReverseCollectionOps ) another ).allContainingIn( collection );
        } else {
            return Collections.containsAll( collection, another );
        }
    }

/* char elem  */

    public static boolean containsAll( CharCollection collection,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        if ( another instanceof ReverseCharCollectionOps ) {
            return ( ( ReverseCharCollectionOps ) another ).allCharsContainingIn( collection );
        } else if ( another instanceof ReverseCollectionOps ) {
            // noinspection unchecked
            return ( ( ReverseCollectionOps ) another ).allContainingIn( collection );
        } else {
            return Collections.containsAll( collection, another );
        }
    }

    public static boolean containsAll( CharSet collection,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        // char collection guarantees same equality (== operator)
        if ( another instanceof CharSet &&
                collection.size() < another.size() ) {
            return false;
        } else if ( another instanceof ReverseCharCollectionOps ) {
            return ( ( ReverseCharCollectionOps ) another ).allCharsContainingIn( collection );
        } else if ( another instanceof ReverseCollectionOps ) {
            // noinspection unchecked
            return ( ( ReverseCollectionOps ) another ).allContainingIn( collection );
        } else {
            return Collections.containsAll( collection, another );
        }
    }

/* end */


    public static <V> boolean reverseAllContainingIn( ObjDHash objHash, V[] vals,
            TCollection<? super V> collection ) {
        if ( objHash.isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                    containsAll = false;
                    break;
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }

/* char elem  */

    public static boolean reverseAllCharsContainingIn(
            StatesDHash primHash, char[] vals,
            CharCollection collection ) {
        if ( primHash.isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }

    public static boolean reverseAllCharsContainingIn(
            StatesDHash primHash, char[] vals,
            TCollection<? super Character> collection ) {
        if ( primHash.isEmpty() )
            return true;
        boolean containsAll = true;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && !collection.contains( vals[ i ] ) ) {
                containsAll = false;
                break;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return containsAll;
    }

/* end */


    //////////////////////////////////////////
    // Add All

    public static <E> boolean reverseAddAll( ObjDHash objHash, E[] vals,
            Collection<? super E> collection ) {
        if ( objHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) )
                    changed |= collection.add( vals[ i ] );
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= collection.add( vals[ i ] );
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* char elem  */

    public static boolean reverseAddAllChars( StatesDHash primHash, char[] vals,
            CharCollection collection ) {
        if ( primHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                changed |= collection.add( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

    public static boolean reverseAddAllChars( StatesDHash primHash, char[] vals,
            TCollection<? super Character> collection ) {
        if ( primHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                changed |= collection.add( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* end */


    ////////////////////////////////////////////
    // Reverse Remove All

    public static <E> boolean reverseRemoveAll(
            ObjDHash objHash, E[] vals, TSet<? super E> set ) {
        if ( objHash.isEmpty() || set.isEmpty() )
            return false;
        boolean changed = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for ( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) )
                    changed |= set.remove( vals[ i ] );
            }
        } else {
            for ( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) )
                    changed |= set.remove( vals[ i ] );
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* char elem  */

    public static boolean reverseRemoveAllChars(
            StatesDHash primHash, char[] vals, CharSet set ) {
        if ( primHash.isEmpty() || set.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                changed |= set.remove( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

    public static boolean reverseRemoveAllChars(
            StatesDHash primHash, char[] vals, TSet<? super Character> set ) {
        if ( primHash.isEmpty() || set.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) )
                changed |= set.remove( vals[ i ] );
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* end */


    /////////////////////////////////////////////////
    // Remove All

    public static boolean removeAll(
            Collection<?> collection, ObjDHash objHash, Object[] vals,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        if ( objHash.isEmpty() || another.isEmpty() )
            return false;
        boolean changed = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && another.contains( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && another.contains( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* char elem  */

    public static boolean removeAllCharsFromSet(
            HashCharSet set, StatesDHash primHash, char[] vals,
            Collection<?> another ) {
        if ( set == another )
            throw new IllegalArgumentException();
        if ( set.isEmpty() || another.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        if ( another instanceof ReverseCharCollectionOps &&
                another.size() < set.size() ) {
            return ( ( ReverseCharCollectionOps ) another ).reverseRemoveAllCharsFrom( set );
        }
        else if ( another instanceof CharCollection ) {
            CharCollection anotherChars = ( CharCollection ) another;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) &&
                        anotherChars.contains( vals[ i ] ) ) {
                    primHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && another.contains( vals[ i ] ) ) {
                    primHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* end */


    ////////////////////////////////
    // Retain All

    public static boolean retainAll(
            Collection<?> collection, ObjDHash objHash, Object[] vals,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        if ( another.isEmpty() ) {
            if ( objHash.isEmpty() ) {
                return false;
            } else {
                objHash.clear();
                return true;
            }
        }
        boolean changed = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && !another.contains( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !another.contains( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* char elem  */

    public static boolean retainAllChars(
            CharCollection collection, StatesDHash primHash, char[] vals,
            Collection<?> another ) {
        if ( collection == another )
            throw new IllegalArgumentException();
        if ( another.isEmpty() ) {
            if ( primHash.isEmpty() ) {
                return false;
            } else {
                primHash.clear();
                return true;
            }
        }
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        if ( another instanceof CharCollection ) {
            CharCollection anotherChars = ( CharCollection ) another;
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) &&
                        !anotherChars.contains( vals[ i ] ) ) {
                    primHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && !another.contains( vals[ i ] ) ) {
                    primHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* end */


    /////////////////////////////////////
    // Remove If

    public static <E> boolean removeIf( ObjDHash objHash, E[] vals,
            Predicate<? super E> filter ) {
        if ( objHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = objHash.modCount();
        Object[] hash = objHash.set;
        if ( objHash.noRemoved() ) {
            for( int i = hash.length; i-- > 0; ) {
                if ( notFree( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        } else {
            for( int i = hash.length; i-- > 0; ) {
                if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                    objHash.removeAt( i );
                    mc++;
                    changed = true;
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* char elem  */

    public static boolean removeCharsIf( StatesDHash primHash, char[] vals,
            CharPredicate filter ) {
        if ( primHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                primHash.removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

    public static boolean removeCharsIf( StatesDHash primHash, char[] vals,
            Predicate<? super Character> filter ) {
        if ( primHash.isEmpty() )
            return false;
        boolean changed = false;
        int mc = primHash.modCount();
        byte[] hash = primHash.states;
        for( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) && filter.test( vals[ i ] ) ) {
                primHash.removeAt( i );
                mc++;
                changed = true;
            }
        }
        if ( mc != primHash.modCount() )
            throw new ConcurrentModificationException();
        return changed;
    }

/* end */


    ///////////////////////////////////
    // Set Hash Code

    public static <E> int setHashCode( ObjDHash<E> objHash ) {
        if ( objHash.isEmpty() )
            return 0;
        int hashCode = 0;
        int mc = objHash.modCount();
        //noinspection unchecked
        E[] set = ( E[] ) objHash.set;
        E e;
        if ( objHash.noRemoved() ) {
            for ( int i = set.length; i-- > 0; ) {
                if ( notFree( ( e = set[ i ] ) ) && e != null ) {
                    hashCode += objHash.hashCode( e );
                }
            }
        } else {
            for ( int i = set.length; i-- > 0; ) {
                if ( isFull( ( e = set[ i ] ) ) && e != null ) {
                    hashCode += objHash.hashCode( e );
                }
            }
        }
        if ( mc != objHash.modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }

/* char elem  */

    public static int setHashCode( CharDHash charHash ) {
        if ( charHash.isEmpty() )
            return 0;
        int hashCode = 0;
        int mc = charHash.modCount();
        byte[] hash = charHash.states;
        char[] set = charHash.set;
        for ( int i = hash.length; i-- > 0; ) {
            if ( isFull( hash[ i ] ) ) {
                hashCode += Primitives.identityHashCode( set[ i ] );
            }
        }
        if ( mc != charHash.modCount() )
            throw new ConcurrentModificationException();
        return hashCode;
    }

/* end */
}
