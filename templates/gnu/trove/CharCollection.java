// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove;


//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


import gnu.trove.function.CharPredicate;


//TODO doc
public interface CharCollection
        extends TCollection<Character>, CharIterable, CharSequence {

    /**
     * Returns the value that is used to represent null. The default value
     * is generally zero, but can be changed during construction of the collection.
     *
     * @return the value that represents null
     */
    char getNoEntryValue();


    /**
     * Returns {@code true} if this collection contains at least one element
     * equals to the specified one.
     *
     * @param value element whose presence in this collection is to be tested
     * @return {@code true} if this collection contains the specified element
     * @throws IllegalArgumentException if the specified element is representing
     *         null in this collection, and it does not permit such elements
     *         (see {@link #getNoEntryValue()}; optional restriction,
     *         the implementation may also simply return {@code false})
     * @see #contains(Object)
     */
    boolean contains( char value );



    /**
     * Returns an array containing all of the elements in this collection.
     * If this collection makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the elements
     * in the same order.
     *
     * <p>The returned array will be "safe" in that no references to it
     * are maintained by this collection.  (In other words, this method must
     * allocate a new array even if this collection is backed by an array).
     * The caller is thus free to modify the returned array.
     *
     * <p>This method acts as bridge between array-based and collection-based APIs.
     *
     * @return an array containing all the elements in this collection
     * @see #toArray()
     * @see #toArray(char[])
     */
    char[] toCharArray();


    /**
     * Returns an array containing elements in this collection.
     *
     * <p>If this collection fits in the specified array with room to spare
     * (i.e., the array has more elements than this collection), the element in
     * the array immediately following the end of the collection is set
     * to the value representing null in this collection (see
     * {@link #getNoEntryValue()}).  This is useful in determining
     * the length of this collection <i>only</i> if the caller knows that this
     * collection does not contain any elements representing null.
     *
     * <p>If the native array is smaller than the collection size,
     * the array will be filled with elements in Iterator order
     * until it is full and exclude the remainder.
     *
     * <p>If this collection makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the elements
     * in the same order.
     *
     * @param dest the array into which the elements of this collection are to be
     *        stored.
     * @return an {@code char[]} containing all the elements in this collection
     * @throws NullPointerException if the specified array is {@code null}
     * @see #toCharArray()
     * @see #toArray(Object[])
     */
    char[] toArray( char[] dest );


    /**
     * Ensures that this collection contains the specified element (optional
     * operation).  Returns {@code true} if this collection changed as a
     * result of the call.  (Returns {@code false} if this collection does
     * not permit duplicates and already contains the specified element.)
     *
     * <p>Collections that support this operation may place limitations on what
     * elements may be added to this collection.
     * In particular, some collections will refuse to add elements
     * representing null (see {@link #getNoEntryValue()}).
     * Collection classes should clearly specify in their documentation any
     * restrictions on what elements may be added.
     *
     * <p>If a collection refuses to add a particular element for any reason
     * other than that it already contains the element, it <i>must</i> throw
     * an exception (rather than returning {@code false}).  This preserves
     * the invariant that a collection always contains the specified element
     * after this call returns.
     *
     * @param value element whose presence in this collection is to be ensured
     * @return {@code true} if this collection changed as a result of the call
     * @throws UnsupportedOperationException if the {@code add} operation
     *         is not supported by this collection
     * @throws IllegalArgumentException if some property of the element
     *         prevents it from being added to this collection
     * @throws IllegalStateException if the element cannot be added at this
     *         time due to insertion restrictions
     * @see #add(Object)
     */
    boolean add( char value );


    /**
     * Removes a single copy of the specified element from this
     * collection, if it is present (optional operation).
     * Returns {@code true} if this collection contained the specified element
     * (or equivalently, if this collection changed as a result of the call).
     *
     * @param value element to be removed from this collection, if present
     * @return {@code true} if an element was removed as a result of this call
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this collection
     * @throws IllegalArgumentException if the specified element represents
     *         null in this collection, and it does not permit such elements
     *         (see {@link #getNoEntryValue()}; optional restriction,
     *         the implementation may also simply return {@code false})
     * @see #remove(Object)
     */
    boolean remove( char value );


    boolean removeIf( CharPredicate filter );

} // CharCollection
