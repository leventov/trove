// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map;


//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////

import gnu.trove.function.*;
import gnu.trove.ShortCollection;
import gnu.trove.set.CharSet;
import org.jetbrains.annotations.NotNull;


/**
 * Interface for a primitive map of char keys and short values.
 */
public interface CharShortMap
        extends TMap<Character, Short>, CharShortEntrySequence {


    char getNoEntryKey();


    short getNoEntryValue();


    boolean containsKey( char key );


    boolean containsValue( short value );


    short get( char key );


    short put( char key, short value );


    short remove( char key );


    /**
     * Attempts to compute a mapping for the specified key and its current
     * mapped value (or "no entry" value if there is no current mapping).
     *
     * <p>If the function itself throws an (unchecked) exception,
     * the exception is rethrown, and the current mapping is left unchanged.
     *
     * @param key key with which the specified value is to be associated
     * @param remappingFunction the function to compute a value
     * @return the new value associated with the specified key
     * @throws NullPointerException if the remappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     */
    short compute( char key, CharShortToShortFunction remappingFunction );


    /**
     * If the specified key is not already associated with a value,
     * attempts to compute its value using the given mapping function
     * and enters it into this map.
     *
     * <p>If the function itself throws an (unchecked) exception, the
     * exception is rethrown, and no mapping is recorded.  The most
     * common usage is to construct a new object serving as an initial
     * mapped value or memoized result.
     *
     * @param key key with which the specified value is to be associated
     * @param mappingFunction the function to compute a value
     * @return the current (existing or computed) value associated with
     *         the specified key
     * @throws NullPointerException if the mappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     */
    short computeIfAbsent( char key, CharToShortFunction mappingFunction );


    /**
     * If the value for the specified key is present, attempts to
     * compute a new mapping given the key and its current mapped value.
     *
     * <p>If the function itself throws an (unchecked) exception,
     * the exception is rethrown, and the current mapping is left unchanged.
     *
     * @param key key with which the specified value is to be associated
     * @param remappingFunction the function to compute a value
     * @return the new value associated with the specified key,
     *         or "no entry" value
     * @throws NullPointerException if the remappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     */
    short computeIfPresent( char key, CharShortToShortFunction remappingFunction );


    /**
     * Performs the given action on each entry in this map until all entries
     * have been processed or the action throws an {@code Exception}.
     * Exceptions thrown by the action are relayed to the caller. The entries
     * will be processed in the same order as the entry set iterator unless that
     * order is unspecified in which case implementations may use an order which
     * differs from the entry set iterator.
     *
     * @param action The action to be performed for each entry
     * @throws NullPointerException if the specified action is null
     */
    @Override
    void forEach( CharShortConsumer action );


    @Override
    @NotNull
    CharShortMapIterator mapIterator();


    /**
     * Returns the value to which the specified key is mapped,
     * or {@code defaultValue} if this map contains no mapping for the key.
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or
     *         {@code defaultValue} if this map contains no mapping for the key
     */
    short getOrDefault( char key, short defaultValue );


    /**
     * If the specified key is not already associated with a value,
     * associates it with the given value. Otherwise, replaces the value with
     * the results of the given remapping function. This method may be of use
     * when combining multiple mapped values for a key.
     *
     * <p>If the function itself throws an (unchecked) exception,
     * the exception is rethrown, and the current mapping is left unchanged.
     *
     * @param key key with which the specified value is to be associated
     * @param value the value to use if absent
     * @param remappingOperator the function to recompute a value if present
     * @return the new value associated with the specified key
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws NullPointerException if the remappingOperator is null
     */
    short merge( char key, short value, ShortBinaryOperator remappingOperator );


    /**
     * If the specified key is not already associated with a value,
     * associates it with the given value and returns "no entry" value,
     * else returns the current value.
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with the specified key, or
     *         "no entry" value if there was no mapping for the key.
     *         (A "no entry" value return can also indicate that the map
     *         previously associated "no entry" value with the key,
     *         if the implementation supports such values.)
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws IllegalArgumentException if some property of the specified key
     *         or value prevents it from being stored in this map (optional restriction)
     */
    short putIfAbsent( char key, short value );


    /**
     * Removes the entry for the specified key only if it is currently
     * mapped to the specified value.
     *
     * @param key key with which the specified value is associated
     * @param value value expected to be associated with the specified key
     * @return {@code true} if the value was removed
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this map (optional restriction)
     */
    boolean remove( char key, short value );


    /**
     * Replaces the entry for the specified key only if it is
     * currently mapped to some value.
     *
     * @param key key with which the specified value is associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with the specified key,
     *         or "no entry" value if there was no mapping for the key.
     *         (A "no entry" value return can also indicate that the map
     *         previously associated "no entry" value with the key,
     *         if the implementation supports such values.)
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws IllegalArgumentException if some property of the specified key
     *         or value prevents it from being stored in this map
     */
    short replace( char key, short value );


    /**
     * Replaces the entry for the specified key only if currently
     * mapped to the specified value.
     *
     * @param key key with which the specified value is associated
     * @param oldValue value expected to be associated with the specified key
     * @param newValue value to be associated with the specified key
     * @return {@code true} if the value was replaced
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws IllegalArgumentException if some property of a specified key
     *         or value prevents it from being stored in this map
     */
    boolean replace( char key, short oldValue, short newValue );


    /**
     * Replaces each entry's value with the result of invoking the given
     * function on that entry, in the order entries are returned by an entry
     * set iterator, until all entries have been processed or the function
     * throws an exception.
     *
     * @param function the function to apply to each entry
     * @throws UnsupportedOperationException if the {@code set} operation
     *         is not supported by this map's entry set iterator.
     * @throws NullPointerException if the specified function is null
     * @throws IllegalArgumentException if some property of a replacement value
     *         prevents it from being stored in this map (optional restriction)
     */
    void replaceAll( CharShortToShortFunction function );


    boolean containsEntry( char key, short value );


    void justPut( char key, short value );

    @Override
    @NotNull
    CharSet keySet();


    @Override
    @NotNull ShortCollection values();


    boolean testWhile( CharShortPredicate predicate );


    boolean removeIf( CharShortPredicate filter );


    /**
     * Adjusts the primitive value mapped to the key if the key is present in the map.
     * Otherwise, the {@code putAmount} is put in the map.
     *
     * @param key the key of the value to increment
     * @param adjustAmount the amount to adjust the value by
     * @param putAmount the value put into the map if the key is not initial present
     *
     * @return the value present in the map after the adjustment or put operation
     */
    short adjustOrPutValue( char key, short adjustAmount, short putAmount );
}
