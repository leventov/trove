// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map;

//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


/**
 * Iterator for maps
 * TODO doc
 * @param <V>
 */
public interface CharKeyMapIterator<V> extends MapIterator<Character, V> {

    /**
     * Returns the key of the last entry, passed by the iterator.
     *
     * <p>Primitive specialization of {@link #key()} method.
     *
     * <p>{@code TIterator} implementations should document specific
     * type of runtime exceptions, thrown if {@code charKey()} is invoked on
     * newly-created iterator prior to advancing.
     *
     * @return the key of the last entry, passed by the iterator
     * @throws RuntimeException if the {@code charKey()} method is invoked
     *         on newly-created iterator prior to calling {@link #tryAdvance()}
     *         at least once
     * @see java.util.Map.Entry#getKey()
     */
    char charKey();
}
