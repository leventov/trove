// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map;

import gnu.trove.function.*;
import gnu.trove.set.CharSet;
import org.jetbrains.annotations.NotNull;


//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


/**
 * Interface for a primitive map of char keys and Object values.
 */
public interface CharObjMap<V>
        extends TMap<Character, V>, CharObjEntrySequence<V> {

    boolean containsKey( char key );


    V get( char key );


    V put( char key, V value );


    V remove( char key );


    /**
     * Returns the value that represents null in the {@link #keySet()}.
     * The default value is generally zero, but can be changed during
     * construction of the collection.
     *
     * @return the value that represents a null value in this collection.
     */
    char getNoEntryKey();


    /**
     * Attempts to compute a mapping for the specified key and its
     * current mapped value (or {@code null} if there is no current
     * mapping).
     *
     * <p>If the function returns {@code null}, the mapping is removed (or
     * remains absent if initially absent).  If the function itself throws an
     * (unchecked) exception, the exception is rethrown, and the current mapping
     * is left unchanged.
     *
     * <p>Any class that permits null values must document
     * whether and how this method distinguishes absence from null mappings.
     *
     * @param key key with which the specified value is to be associated
     * @param remappingFunction the function to compute a value
     * @return the new value associated with the specified key, or null if none
     * @throws NullPointerException if the remappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of the specified value
     *         prevents it from being stored in this map (optional restriction)
     */
    V compute( char key,
            CharObjFunction<? super V, ? extends V> remappingFunction );


    /**
     * If the specified key is not already associated with a value (or
     * is mapped to {@code null}), attempts to compute its value using
     * the given mapping function and enters it into this map unless
     * {@code null}.
     *
     * <p>If the function returns {@code null} no mapping is recorded. If
     * the function itself throws an (unchecked) exception, the
     * exception is rethrown, and no mapping is recorded.  The most
     * common usage is to construct a new object serving as an initial
     * mapped value or memoized result.
     *
     * <p>Any class that permits null values must document
     * whether and how this method distinguishes absence from null mappings.
     *
     * @param key key with which the specified value is to be associated
     * @param mappingFunction the function to compute a value
     * @return the current (existing or computed) value associated with
     *         the specified key, or null if the computed value is null
     * @throws NullPointerException if the mappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of the specified value
     *         prevents it from being stored in this map (optional restriction)
     */
    V computeIfAbsent( char key, CharFunction<? extends V> mappingFunction );


    /**
     * If the value for the specified key is present and non-null, attempts to
     * compute a new mapping given the key and its current mapped value.
     *
     * <p>If the function returns {@code null}, the mapping is removed.  If the
     * function itself throws an (unchecked) exception, the exception is
     * rethrown, and the current mapping is left unchanged.
     *
     * <p>Any class that permits null values must document
     * whether and how this method distinguishes absence from null mappings.
     *
     * @param key key with which the specified value is to be associated
     * @param remappingFunction the function to compute a value
     * @return the new value associated with the specified key, or null if none
     * @throws NullPointerException if the remappingFunction is null
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of the specified value
     *         prevents it from being stored in this map (optional restriction)
     */
    V computeIfPresent( char key,
            CharObjFunction<? super V, ? extends V> remappingFunction );


    /**
     * Performs the given action on each entry in this map until all entries
     * have been processed or the action throws an {@code Exception}.
     * Exceptions thrown by the action are relayed to the caller. The entries
     * will be processed in the same order as the entry set iterator unless that
     * order is unspecified in which case implementations may use an order which
     * differs from the entry set iterator.
     *
     * @param action The action to be performed for each entry
     * @throws NullPointerException if the specified action is null
     */
    @Override
    void forEach( CharObjConsumer<? super V> action );


    @Override
    @NotNull
    CharKeyMapIterator<V> mapIterator();


    /**
     *  Returns the value to which the specified key is mapped,
     *  or {@code defaultValue} if this map contains no mapping
     *  for the key.
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or
     *         {@code defaultValue} if this map contains no mapping for the key
     */
    V getOrDefault( char key, V defaultValue );


    /**
     * If the specified key is not already associated with a value or is
     * associated with null, associates it with the given value.
     * Otherwise, replaces the value with the results of the given
     * remapping function, or removes if the result is {@code null}. This
     * method may be of use when combining multiple mapped values for a key.
     *
     * <p>If the function returns {@code null}, the mapping is removed (or
     * remains absent if initially absent).  If the function itself throws an
     * (unchecked) exception, the exception is rethrown, and the current mapping
     * is left unchanged.
     *
     * <p>Any class that permits null values must document
     * whether and how this method distinguishes absence from null mappings.
     *
     * @param key key with which the specified value is to be associated
     * @param value the value to use if absent
     * @param remappingFunction the function to recompute a value if present
     * @return the new value associated with the specified key, or null if none
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of the specified value
     *         prevents it from being stored in this map (optional restriction)
     * @throws NullPointerException if the remappingFunction is null
     */
    V merge( char key, V value,
            BiFunction<? super V, ? super V, ? extends V> remappingFunction );


    /**
     * If the specified key is not already associated with a value (or is mapped
     * to {@code null}) associates it with the given value and returns
     * {@code null}, else returns the current value.
     *
     * @param key key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with the specified key, or
     *         {@code null} if there was no mapping for the key.
     *         (A {@code null} return can also indicate that the map
     *         previously associated {@code null} with the key,
     *         if the implementation supports null values.)
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws NullPointerException if the specified value is null,
     *         and this map does not permit null values (optional restriction)
     * @throws IllegalArgumentException if some property of the specified
     *         value prevents it from being stored in this map (optional restriction)
     */
    V putIfAbsent( char key, V value );


    /**
     * Removes the entry for the specified key only if it is currently
     * mapped to the specified value.
     *
     * @param key key with which the specified value is associated
     * @param value value expected to be associated with the specified key
     * @return {@code true} if the value was removed
     * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the value is of an inappropriate
     *         type for this map (optional restriction)
     * @throws NullPointerException if the specified value is null,
     *         and this map does not permit null values (optional restriction)
     */
    boolean remove( char key, Object value );


    /**
     * Replaces the entry for the specified key only if it is
     * currently mapped to some value.
     *
     * @param key key with which the specified value is associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with the specified key, or
     *         {@code null} if there was no mapping for the key.
     *         (A {@code null} return can also indicate that the map
     *         previously associated {@code null} with the key,
     *         if the implementation supports null values.)
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of the specified value
     *         prevents it from being stored in this map (optional restriction)
     * @throws NullPointerException if the specified value is null,
     *         and this map does not permit null values
     * @throws IllegalArgumentException if some property of the specified key
     *         or value prevents it from being stored in this map
     */
    V replace( char key, V value );


    /**
     * Replaces the entry for the specified key only if currently
     * mapped to the specified value.
     *
     * @param key key with which the specified value is associated
     * @param oldValue value expected to be associated with the specified key
     * @param newValue value to be associated with the specified key
     * @return {@code true} if the value was replaced
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by this map (optional restriction)
     * @throws ClassCastException if the class of a specified value
     *         prevents it from being stored in this map
     * @throws NullPointerException if a specified value is null,
     *         and this map does not permit null values
     * @throws IllegalArgumentException if some property of a specified
     *         value prevents it from being stored in this map
     */
    boolean replace( char key, V oldValue, V newValue );


    /**
     * Replaces each entry's value with the result of invoking the given
     * function on that entry, in the order entries are returned by an entry
     * set iterator, until all entries have been processed or the function
     * throws an exception.
     *
     * @param function the function to apply to each entry
     * @throws UnsupportedOperationException if the {@code set} operation
     *         is not supported by this map's entry set iterator.
     * @throws ClassCastException if the class of a replacement value
     *         prevents it from being stored in this map
     * @throws NullPointerException if the specified function is null, or the
     *         specified replacement value is null, and this map does not permit
     *         null values (optional restriction)
     * @throws IllegalArgumentException if some property of a replacement value
     *         prevents it from being stored in this map (optional restriction)
     */
    void replaceAll( CharObjFunction<? super V, ? extends V> function );


    boolean containsEntry( char key, V value );


    void justPut( char key, V value );


    boolean justRemove( char key );


    @Override
    @NotNull
    CharSet keySet();


    boolean testWhile( CharObjPredicate<? super V> predicate );


    boolean removeIf( CharObjPredicate<? super V> filter );
}
