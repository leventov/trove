// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove.map;

//////////////////////////////////////////////////
// THIS IS A GENERATED CLASS. DO NOT HAND EDIT! //
//////////////////////////////////////////////////


// TODO doc
public interface CharValueMapIterator<K> extends MapIterator<K, Character> {

    /**
     * Returns the value of the last entry, passed by the iterator.
     *
     * <p>Primitive specialization of {@link #value()} method.
     *
     * <p>{@code TIterator} implementations should document specific
     * type of runtime exceptions, thrown if {@code charValue()} is invoked on
     * newly-created iterator prior to advancing.
     *
     * @return the value of the last entry, passed by the iterator
     * @throws RuntimeException if the {@code charValue()} method is invoked on
     *         on newly-created iterator prior to calling {@link #tryAdvance()}
     *         at least once
     */
    char charValue();


    /**
     * Replace the value of the last entry, passed by the iterator, with the
     * specified value (optional operation). Writes through to the map.
     *
     * <p>Primitive specialization of {@link #setValue(Object)} method.
     *
     * <p>The behavior of this call is undefined
     * if the entry has already been removed from the map by the iterator's
     * {@link #remove()} operation, or on newly-created iterator prior
     * to {@link #tryAdvance()}, for example
     * an {@code IndexOutOfBoundsException} could be thrown.
     *
     * <p>Unlike {@link java.util.Map.Entry#setValue(Object)}, this method
     * doesn't return the previous value of the entry to allow more
     * efficient implementation. If you need the previous value,
     * call {@link #charValue()} before setting the new value.
     *
     * @param value the value to set in the current entry
     * @throws UnsupportedOperationException if the {@code put} operation
     *         is not supported by the backing map
     * @throws IllegalArgumentException if some property of this value prevents
     *         it from being stored in the backing map
     * @throws IllegalStateException implementations may, but are not required to,
     *         throw this exception if the entry has been removed from the backing map
     * @see java.util.Map.Entry#setValue(Object)
     */
    void setValue( char value );
}
