// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////

package gnu.trove;

import gnu.trove.function.CharConsumer;
import gnu.trove.function.CharPredicate;
import org.jetbrains.annotations.NotNull;


//TODO doc
public interface CharIterable extends TIterable<Character> {

    /** {@inheritDoc} */
    @NotNull
    CharIterator iterator();


    void forEach( CharConsumer action );


    /**
     * Executes the specified predicate for each element in this collection.
     *
     * <p>If this collection makes any guarantees as to what order its elements
     * are returned by its iterator, this method must apply the predicate to
     * elements in the same order.
     *
     * If the predicate returns {@code false} for some element, the process
     * terminates and {@code testWhile} call returns {@code false}.
     *
     * @param predicate predicate to be executed for each element in this collection
     * @return {@code false} if the loop over the collection terminated because
     *         the predicate returned {@code false} for some element
     */
    boolean testWhile( CharPredicate predicate );
}
