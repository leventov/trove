// ////////////////////////////////////////////////////////////////////////
// Copyright (c) 2001-2013, Trove authors. All Rights Reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// ////////////////////////////////////////////////////////////////////////
package gnu.trove.generator;

import java.io.*;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static gnu.trove.generator.Generator.Phase.*;
import static java.lang.String.format;


/**
 * <p>Generator class that builds final source files from templates. It does so by
 * replacing patterns in the template files.</p>
 *
 * <p><b>Patterns</b></p>
 *
 * TODO doc
 *
 * <p><b>Block Replication</b></p>
 *
 * TODO doc
 *
 * @author Roman Leventov
 */
public class Generator {
    //TODO refactor

    private static final String END_S = "/[\\*/]{2}/";

    private static final Pattern AS_IS_T_P =
            Pattern.compile("/[\\*/]![\\*/]/(.*?)" + END_S);

    private static final String COND_T_P_FORMAT =
            "/[\\*/] if ([a-z]+)((\\|[a-z]+)+)? %s [\\*/]/(.*?)" +
            "(/[\\*/] elif ([a-z]+)((\\|[a-z]+)+)? [\\*/]/(.*?))*" +
            "/[\\*/] endif [\\*/]/";

    private static final Pattern KEY_COND_T_P = caseInsPattern(format(COND_T_P_FORMAT, "key"));
    private static final Pattern VALUE_COND_T_P = caseInsPattern(format(COND_T_P_FORMAT, "value"));

    // set of types - group 2, substitution - group 7
    private static final Pattern COND_PART = caseInsPattern(
            "/[\\*/] (el)?if (([a-z]+)((\\|[a-z]+)+)?)( [a-z]+)? [\\*/]/" +
            "(.*?)" +
            "(?=/[\\*/] (endif|elif))");


    private static final Pattern ANY_TITLE_P =
            Pattern.compile("Double|Float|Int|Long|Byte|Short|Char");

    // dim name - group 5, options - group 1
    private static final String TEMPLATE_DIMENSION_S =
            "(([a-z]+)((\\|[a-z]+)+)?)\\s+(\\S+)";

    private static final Pattern TEMPLATE_DIMENSION_P =
            caseInsPattern(TEMPLATE_DIMENSION_S);

    private static final Pattern TEMPLATE_DESCRIPTOR_P = caseInsPattern(
            "\\s*/[\\*/]\\s*((" + TEMPLATE_DIMENSION_S + "\\s+)+)[\\*/]/\\s*");

    private static final Pattern BLOCK_END_P =
            caseInsPattern("\\s*/[\\*/]\\s*END\\s*[\\*/]/\\s*");

    static class TemplateDimension {
        final String name;
        final List<?> options;

        TemplateDimension( String name, List<?> options ) {
            this.name = name;
            this.options = options;
        }
    }

    static class Type {
        static final Type DOUBLE =
                new Type("double", "Double", "POSITIVE_INFINITY", "NEGATIVE_INFINITY", "$1.0");
        static final Type FLOAT =
                new Type("float", "Float", "POSITIVE_INFINITY", "NEGATIVE_INFINITY", "$1.0f");
        static final Type INT =
                new Type("int", "Integer", "MAX_VALUE", "MIN_VALUE", "$1");
        static final Type LONG =
                new Type("long", "Long", "MAX_VALUE", "MIN_VALUE", "$1L");
        static final Type BYTE =
                new Type("byte", "Byte", "MAX_VALUE", "MIN_VALUE", "\\( byte \\) $1");
        static final Type SHORT =
                new Type("short", "Short", "MAX_VALUE", "MIN_VALUE", "\\( short \\) $1");
        static final Type CHAR =
                new Type("char", "Character", "MAX_VALUE", "MIN_VALUE", "'\\\\u000$1'");

        static final Type OBJECT = new ObjectType();

        static Type[] PRIMITIVE_TYPES =
                new Type[] {DOUBLE, FLOAT, INT, LONG, BYTE, SHORT, CHAR};

        static Type valueOf(String t) {
            t = t.toLowerCase();
            if (t.equals("double")) return DOUBLE;
            if (t.equals("float")) return FLOAT;
            if (t.equals("int") || t.equals("integer")) return INT;
            if (t.equals("long")) return LONG;
            if (t.equals("byte")) return BYTE;
            if (t.equals("short")) return SHORT;
            if (t.equals("char") || t.equals("character")) return CHAR;
            if (t.equals("obj") || t.equals("object")) return OBJECT;
            throw new IllegalArgumentException();
        }

        PrimitiveTemplate prim;
        private SimpleTemplate maxV;
        private SimpleTemplate minV;
        private SimpleTemplate clNm;
        private SimpleTemplate keyZero;
        private SimpleTemplate valueZero;
        private SimpleTemplate elemZero;

        private Type(String primitive, String className,
                String maxValue, String minValue, String zeroLiteral) {

            prim = PrimitiveTemplate.primitiveTemplate(primitive);

            clNm = new SimpleTemplate(className,
                    Pattern.compile("(?<![A-Za-z0-9_$])" + className + "(?![A-Za-z0-9_$])"));
            maxV = new SimpleTemplate(className + "." + maxValue,
                    Pattern.compile(className + "\\s*\\.\\s*" + maxValue));
            minV = new SimpleTemplate(className + "." + minValue,
                    Pattern.compile(className + "\\s*\\.\\s*" + minValue));

            String alNumId = "[a-zA-Z\\d_$]";
            // from start to template end on the same line
            // OR without close, ex. /*zk*/0, /*z*/0.0f, any Java primitive literal
            String zeroFormat = "/[\\*/]%s[\\*/]/" +
                format("([^\\n]*?%s|\\s*+%s++(\\.%s++)?)", END_S, alNumId, alNumId);
            keyZero = new SimpleTemplate(zeroLiteral, caseInsPattern(format(zeroFormat, "k(\\d)")));
            valueZero = new SimpleTemplate(zeroLiteral, caseInsPattern(format(zeroFormat, "v(\\d)")));
            elemZero = new SimpleTemplate(zeroLiteral, caseInsPattern(format(zeroFormat, "e?(\\d)")));
        }

        private Type() {}

        String replaceMaxV(String content, SimpleTemplate newTemplate) {
            return maxV.replace(content, newTemplate);
        }
        String replaceMinV(String content, SimpleTemplate newTemplate) {
            return minV.replace(content, newTemplate);
        }
        String replaceClNm(String content, SimpleTemplate newTemplate) {
            return clNm.replace(content, newTemplate);
        }
        String replaceZero(String content, SimpleTemplate newTemplate, Phase p) {
            SimpleTemplate zero;
            if (p == K) zero = keyZero;
            else if (p == V) zero = valueZero;
            else zero = elemZero;
            return zero.replace(content, newTemplate);
        }

        static class ObjectType extends Type {
            ObjectType() {
                prim = PrimitiveTemplate.objTemplate();
            }

            String replaceMaxV( String content, SimpleTemplate newTemplate ) {
                throw new UnsupportedOperationException();
            }
            String replaceMinV( String content, SimpleTemplate newTemplate ) {
                throw new UnsupportedOperationException();
            }
            String replaceClNm( String content, SimpleTemplate newTemplate ) {
                throw new UnsupportedOperationException();
            }
            String replaceZero( String content, SimpleTemplate newTemplate, Phase p ) {
                throw new UnsupportedOperationException();
            }
        }
    }

    static enum Phase { K, V, E }

    static class PrimitiveTemplate {
        String standalone;
        Pattern standaloneP;
        String lower;
        Pattern lowerP;
        String title;
        Pattern titleP;
        String upper;
        Pattern upperP;

        static PrimitiveTemplate primitiveTemplate(String primString) {
            PrimitiveTemplate t = new PrimitiveTemplate();
            t.standalone = primString;
            t.standaloneP = Pattern.compile(
                    "(?<![A-Za-z0-9_$])" + primString + "(?![A-Za-z0-9_$])");
            t.lower = primString;
            // look around not to replace keyword substring inside words
            // Uppercase ahead is ok -- consider Integer.intValue
            // Special case - plural form (ints, chars)
            t.lowerP = Pattern.compile(
                    "(?<![A-Za-z])" + primString + "(?![a-rt-z].|s[a-z])");

            t.title = primString.substring(0, 1).toUpperCase() + primString.substring(1);
            // lookahead not to replace Int in ex. doInterrupt
            // special case - plural form: addAllInts
            t.titleP = Pattern.compile(t.title + "(?![a-rt-z].|s[a-z])");

            t.upper = primString.toUpperCase();
            // lookahead and lookbehind not to replace INT in ex. INTERLEAVE_CONSTANT
            t.upperP = Pattern.compile("(?<![A-Z])" + t.upper + "(?![A-RT-Z].|S[A-Z])");
            return t;
        }

        String standalone(Phase p) {
            return standalone;
        }

        static PrimitiveTemplate substitute(String id) {
            PrimitiveTemplate t = new PrimitiveTemplate();
            t.standalone = format("#%s.standalone#", id);
            t.standaloneP = Pattern.compile(Pattern.quote(t.standalone));
            t.lower = format("#%s.lower#", id);
            t.lowerP = Pattern.compile(Pattern.quote(t.lower));
            t.title = format("#%s.title#", id);
            t.titleP = Pattern.compile(Pattern.quote(t.title));
            t.upper = format("#%s.upper#", id);
            t.upperP = Pattern.compile(Pattern.quote(t.upper));
            return t;
        }

        public String replace(String content, PrimitiveTemplate newTemplate, Phase p) {
            String out = content;
            out = standaloneP.matcher(out).replaceAll(newTemplate.standalone(p));
            out = lowerP.matcher(out).replaceAll(newTemplate.lower);
            out = titleP.matcher(out).replaceAll(newTemplate.title);
            out = upperP.matcher(out).replaceAll(newTemplate.upper);
            return out;
        }

        static PrimitiveTemplate objTemplate() {
            return new ObjectPrimitiveTemplate();
        }

        private static class ObjectPrimitiveTemplate extends PrimitiveTemplate {
            ObjectPrimitiveTemplate() {
                lower = "obj";
                title = "Obj";
                upper = "OBJECT";
            }

            public String replace(String content, PrimitiveTemplate newTemplate, Phase p) {
                throw new UnsupportedOperationException();
            }

            @Override
            String standalone( Phase p ) {
                return p.name();
            }
        }
    }

    static class KeySub {
        static PrimitiveTemplate prim = PrimitiveTemplate.substitute("key");
        static SimpleTemplate clNm = SimpleTemplate.substitute("#key.clNm#");
        static SimpleTemplate maxV = SimpleTemplate.substitute("#key.maxV#");
        static SimpleTemplate minV = SimpleTemplate.substitute("#key.minV#");
        static SimpleTemplate zero = SimpleTemplate.substitute("#key.zero#");
    }

    static class ValSub {
        static PrimitiveTemplate prim = PrimitiveTemplate.substitute("value");
        static SimpleTemplate clNm = SimpleTemplate.substitute("#value.clNm#");
        static SimpleTemplate maxV = SimpleTemplate.substitute("#value.maxV#");
        static SimpleTemplate minV = SimpleTemplate.substitute("#value.minV#");
        static SimpleTemplate zero = SimpleTemplate.substitute("#value.zero#");
    }

    static class SimpleTemplate {
        private String s;
        private Pattern p;

        SimpleTemplate( String s, Pattern p ) {
            this.s = s;
            this.p = p;
        }

        static SimpleTemplate substitute(String id) {
            return new SimpleTemplate(id, Pattern.compile(Pattern.quote(id)));
        }

        public String replace(String content, SimpleTemplate newTemplate) {
            Matcher m = p.matcher(content);
            if (m.find()) {
                return m.replaceAll(newTemplate.s);
            } else {
                return content;
            }
        }
    }

    private static File root_output_dir;
    static boolean prefixBlocks = false;

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println(
                    "V3. Usage: Generator [-c] [-prefix_blocks] " +
                            "<input_dir> <output_dir>");
            return;
        }

        int arg_index = 0;
        boolean clean_output = false;
        if (args[0].equalsIgnoreCase("-c")) {
            clean_output = true;
            arg_index++;
        }
        if (args[arg_index].equalsIgnoreCase("-prefix_blocks")) {
            prefixBlocks = true;
            arg_index++;
        }

        File input_directory = new File(args[arg_index++]);
        File output_directory = new File(args[arg_index]);
        if (!input_directory.exists()) {
            System.err.println("Directory \"" + input_directory + "\" not found.");
            System.exit(-1);
            return;
        }
        if (!output_directory.exists()) {
            makeDirs(output_directory);
        }

        root_output_dir = output_directory;

        if (clean_output) {
            System.out.println("Removing contents of \"" + output_directory + "\"...");
            cleanDir(output_directory);
        }

        scanForFiles(input_directory, output_directory);
    }

    /**
     * Creates dirs, throw IllegalArgumentException or IllegalStateException if
     * argument is invalid or creation fails
     */
    private static void makeDirs(File directory) {
        if (directory.exists() && !directory.isDirectory())
            throw new IllegalArgumentException(directory + " not a directory");

        if (directory.exists())
            return;

        if (!directory.mkdirs())
            throw new IllegalStateException("Could not create directories " + directory);
    }

    private static void scanForFiles(File input_directory, File output_directory)
            throws IOException {

        File[] files = input_directory.listFiles();
        for (File file : files) {
            // Ignore hidden files
            if (file.isHidden()) continue;

            if (file.isDirectory()) {
                // Ignore CVS directories
                if (file.getName().equals("CVS")) continue;

                scanForFiles(file, new File(output_directory, file.getName()));
                continue;
            }

            processFile(file, output_directory);
        }
    }

    private static void processFile(File input_file, File output_directory)
            throws IOException {

        System.out.println("Process file: " + input_file);

        String content = readFile(input_file);
        if (prefixBlocks) {
            content = replaceBlocks(content);
        }

        String file_name = input_file.getName();
        file_name = file_name.replaceAll("\\.template", ".java");

        // See what kind of template markers it's using, either e or k/v. No marker
        // indicates a replication-only class.
        Matcher typeMatcher = ANY_TITLE_P.matcher(file_name);
        List<String> types = new ArrayList<>();
        while (typeMatcher.find()) {
            String part = typeMatcher.group();
            if (!types.contains(part))
                types.add( part );
        }
        if (types.size() == 2) {
            processKVFile(content, output_directory, file_name,
                    Type.valueOf(types.get(0).toUpperCase()),
                    Type.valueOf(types.get(1).toUpperCase()));
        } else if (types.size() == 1) {
            processElemFile(content, output_directory, file_name,
                    Type.valueOf(types.get(0).toUpperCase()));
        } else if (types.isEmpty()) {
            if (prefixBlocks && file_name.split("\\.")[0].endsWith("Blocks")) {
                file_name = "Generated" + file_name;
            }
            File output_file = new File(output_directory, file_name);
            if (input_file.lastModified() < output_file.lastModified()) {
                System.out.println("File " + output_file + " up to date, not processing input");
                return;
            }
            writeFile(replicateBlocks(content), output_file);
        } else {
            System.err.println("Too many types in the name of " + file_name + " file. Skipped.");
        }
    }

    // Workaround to avoid name conflict of templates and generated
    // replication classes
    private static String replaceBlocks(String content) {
        return content.replaceAll("(?<![A-Za-z0-9_$])[A-Z][A-Za-z]+Blocks", "Generated$0");
    }

    private static void processKVFile(String content, final File outputDir,
            final String fileName, final Type key, final Type value) throws IOException {
        String[] parts = content.split("\\r\\n|\\n|\\r", 2);
        String firstLine = parts[0];
        Matcher m = TEMPLATE_DESCRIPTOR_P.matcher(firstLine);
        LinkedHashMap<String, List<?>> templateDimensions = new LinkedHashMap<>();
        List<Type> keyTypes = Arrays.asList(Type.PRIMITIVE_TYPES);
        List<Type> valueTypes = Arrays.asList(Type.PRIMITIVE_TYPES);
        if (m.matches()) {
            templateDimensions = parseTemplateDescriptor(firstLine);
            List<?> kt = templateDimensions.remove("key");
            if (kt != null) keyTypes = ( List<Type> ) kt;
            List<?> vt = templateDimensions.remove("value");
            if (vt != null) valueTypes = ( List<Type> ) vt;
            content = parts[1];
        }
        templateKV(content, key, keyTypes, value, valueTypes,
                templateDimensions, new TemplateKVResultsProcessor() {
            @Override
            public void process(String result, Type nk, Type nv,
                    LinkedHashMap<String, List<?>> templateDimensions,
                    List<Object> options) throws IOException {
                String outFileName = replaceKeyValues(fileName, key, value, nk, nv);
                outFileName = replaceDimensions(outFileName, templateDimensions, options);
                writeFile(result, new File(outputDir, outFileName));
            }
        });
    }

    private static String replicateBlocks(String content) throws IOException {

        final StringBuilder result = new StringBuilder();

        // Find the replicated content blocks in the template.
        // For ease, read this line by line.
        BufferedReader reader = new BufferedReader(new StringReader(content));
        String line;
        StringBuilder blockBuffer = null;
        boolean inBlock = false;
        LinkedHashMap<String, List<?>> templateDimensions = null;
        while ((line = reader.readLine()) != null) {
            if (!inBlock) {
                Matcher m = TEMPLATE_DESCRIPTOR_P.matcher(line);
                if (m.matches()) {
                    templateDimensions = parseTemplateDescriptor(line);
                    inBlock = true;
                    blockBuffer = new StringBuilder();
                } else {
                    result.append(line).append('\n');
                }
            } else {
                if (BLOCK_END_P.matcher(line).matches()) {
                    inBlock = false;
                    template(blockBuffer.toString(), templateDimensions, new TemplateResultsProcessor() {
                        @Override
                        public void process( String repl,
                                LinkedHashMap<String, List<?>> templateDimensions,
                                List<Object> options ) throws IOException {
                            result.append(repl);
                        }
                    });
                } else {
                    blockBuffer.append(line).append('\n');
                }
            }
        }
        return result.toString();
    }

    private static LinkedHashMap<String, List<?>> parseTemplateDescriptor(String descriptor) {
        Matcher m = TEMPLATE_DIMENSION_P.matcher(descriptor);
        LinkedHashMap<String, List<?>> dimensions = new LinkedHashMap<>();
        while (m.find()) {
            String dimName = m.group(5);
            String[] options = m.group(1).split("\\|");
            if ("key".equalsIgnoreCase(dimName) ||
                    "value".equalsIgnoreCase(dimName) ||
                    "elem".equalsIgnoreCase(dimName)) {
                ArrayList<Type> types = new ArrayList<>();
                for ( String option : options ) {
                    types.add(Type.valueOf(option));
                }
                if (types.size() == 1) {
                    ArrayList<Type> primTypes = new ArrayList<>();
                    Type type = types.get(0);
                    primTypes.add(type);
                    for (Type t : Type.PRIMITIVE_TYPES) {
                        if (t != type) primTypes.add(t);
                    }
                    types = primTypes;
                }
                dimensions.put(dimName.toLowerCase(), types);
            } else {
                dimensions.put(dimName, Arrays.asList(options));
            }
        }
        return dimensions;
    }

    private static Type[] getOptionTypes(Type mainType, String optionTypes) {
        if (optionTypes == null) {
            return Type.PRIMITIVE_TYPES;
        } else {
            String[] stringTypes = optionTypes.substring(1).split("\\|");
            Type[] types = new Type[stringTypes.length + 1];
            types[0] = mainType;
            for (int i = 0; i < stringTypes.length; i++) {
            	types[i + 1] = Type.valueOf(stringTypes[i].toUpperCase());
            }
            return types;
        }
    }

    private static void template(String content,
            LinkedHashMap<String, List<?>> templateDimensions,
            final TemplateResultsProcessor processor) throws IOException {
        if (templateDimensions.containsKey("key") &&
                templateDimensions.containsKey("value")) {
            final List<Type> optionKeys = (List<Type>) templateDimensions.remove("key");
            final Type key = optionKeys.get(0);
            final List<Type> optionValues = (List<Type>) templateDimensions.remove("value");
            final Type value = optionValues.get(0);
            templateKV(content, key, optionKeys, value, optionValues, templateDimensions,
                    new TemplateKVResultsProcessor() {
                        @Override
                        public void process(
                                String result, Type nk, Type nv,
                                LinkedHashMap<String, List<?>> templateDimensions,
                                List<Object> options ) throws IOException {
                            LinkedHashMap<String, List<?>> templateDimsWithKV = new LinkedHashMap<>();
                            templateDimsWithKV.put("key", optionKeys);
                            templateDimsWithKV.put("value", optionValues);
                            templateDimsWithKV.putAll(templateDimensions);
                            ArrayList<Object> optionsWithKV = new ArrayList<>();
                            optionsWithKV.add(nk);
                            optionsWithKV.add(nv);
                            optionsWithKV.addAll(options);
                            processor.process(result, templateDimsWithKV, optionsWithKV);
                        }
                    });
            return;
        }
        ArrayList<String> interleavedParts = getQuoteInterleaves(content);
        int totalCombinations = 1;
        for ( List<?> options : templateDimensions.values() ) {
            totalCombinations *= options.size();
        }
        for (int comb = 0; comb < totalCombinations; comb++) {
            List<Object> currentOptions = new ArrayList<>();
            int combRem = comb;
            for (Map.Entry<String, List<?>> e : templateDimensions.entrySet()) {
                List<?> options = e.getValue();
                int index = combRem % options.size();
                combRem /= options.size();
                Object option = options.get(index);
                currentOptions.add(option);
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < interleavedParts.size(); i++) {
                String part = interleavedParts.get(i);
                if (i % 2 == 0) {
                    part = replaceDimensions(part, templateDimensions, currentOptions);
                } // else -- quoted part
                sb.append(part);
            }
            processor.process(sb.toString(), templateDimensions, currentOptions);
        }
    }

    private static void templateKV(String content,
            Type key, List<Type> optionKeys,
            Type value, List<Type> optionValues,
            LinkedHashMap<String, List<?>> templateDimensions,
            TemplateKVResultsProcessor processor) throws IOException {
        ArrayList<String> interleavedParts = getQuoteInterleaves(content);
        int totalCombinations = 1;
        for ( List<?> options : templateDimensions.values() ) {
            totalCombinations *= options.size();
        }
        for (Type nk : optionKeys) {
            for (Type nv : optionValues) {
                for (int comb = 0; comb < totalCombinations; comb++) {
                    List<Object> currentOptions = new ArrayList<>();
                    int combRem = comb;
                    for (Map.Entry<String, List<?>> e : templateDimensions.entrySet()) {
                        List<?> options = e.getValue();
                        int index = combRem % options.size();
                        combRem /= options.size();
                        Object option = options.get(index);
                        currentOptions.add(option);
                    }

                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < interleavedParts.size(); i++) {
                        String part = interleavedParts.get(i);
                        if (i % 2 == 0) {
                            part = replaceConditions(part, KEY_COND_T_P, nk);
                            part = replaceConditions(part, VALUE_COND_T_P, nv);
                            part = replaceKeyValues(part, key, value, nk, nv);
                            part = replaceDimensions(part, templateDimensions, currentOptions);
                        } // else -- quoted part
                        sb.append(part);
                    }
                    processor.process(sb.toString(), nk, nv,
                            templateDimensions, currentOptions);
                }
            }
        }
    }

    private static String replaceDimensions(
            String content, LinkedHashMap<String, List<?>> templateDimensions,
            List<Object> currentOptions) {
        ArrayList<String> dimNames = new ArrayList<>(templateDimensions.keySet());
        ArrayList<Object> defaultOptions = new ArrayList<>();
        for ( List<?> options : templateDimensions.values() ) {
            defaultOptions.add(options.get(0));
        }
        String out = content;
        for ( int oi = 0; oi < currentOptions.size(); oi++ ) {
            Object option = currentOptions.get(oi);
            out = replaceConditions(out,
                    caseInsPattern(String.format(COND_T_P_FORMAT, dimNames.get(oi))),
                    option);
        }
        for ( int oi = 0; oi < currentOptions.size(); oi++ ) {
            Object option = currentOptions.get(oi);
            if (option instanceof String) {
                String defaultOption =
                        (String) defaultOptions.get(oi);
                out = out.replace(defaultOption, (String) option);
            } else {
                Phase p = null;
                String dimName = dimNames.get(oi);
                if ("key".equals(dimName)) p = K;
                if ("value".equals(dimName)) p = V;
                if ("elem".equals(dimName)) p = E;
                Type defaultType = (Type) defaultOptions.get(oi);
                out = replaceElems(out, defaultType, (Type) option, p);
            }
        }
        return out;
    }

    private static interface TemplateResultsProcessor {
        void process(String result,
                LinkedHashMap<String, List<?>> templateDimensions,
                List<Object> options) throws IOException;
    }


    private static interface TemplateKVResultsProcessor {
        void process(String result,
                Type nk, Type nv,
                LinkedHashMap<String, List<?>> templateDimensions,
                List<Object> options) throws IOException;
    }

    private static String replaceKeyValues(String content,
            Type key, Type value, Type nk, Type nv) {
        String out = content;

        out = key.replaceZero(out, KeySub.zero, K);
        out = key.replaceMaxV(out, KeySub.maxV);
        out = key.replaceMinV(out, KeySub.minV);
        out = key.replaceClNm(out, KeySub.clNm);
        out = key.prim.replace(out, KeySub.prim, K);

        out = value.replaceZero(out, ValSub.zero, V);
        out = value.replaceMaxV(out, ValSub.maxV);
        out = value.replaceMinV(out, ValSub.minV);
        out = value.replaceClNm(out, ValSub.clNm);
        out = value.prim.replace(out, ValSub.prim, V);

        out = KeySub.zero.replace(out, nk.keyZero);
        out = KeySub.maxV.replace(out, nk.maxV);
        out = KeySub.minV.replace(out, nk.minV);
        out = KeySub.clNm.replace(out, nk.clNm);
        out = KeySub.prim.replace(out, nk.prim, K);

        out = ValSub.zero.replace(out, nv.valueZero);
        out = ValSub.maxV.replace(out, nv.maxV);
        out = ValSub.minV.replace(out, nv.minV);
        out = ValSub.clNm.replace(out, nv.clNm);
        out = ValSub.prim.replace(out, nv.prim, V);

        return out;
    }

    private static void processElemFile(String content, final File outputDir,
            final String fileName, final Type elem) throws IOException {
        String[] parts = content.split("\\r\\n|\\n|\\r", 2);
        String firstLine = parts[0];
        LinkedHashMap<String, List<?>> templateDimensions;
        Matcher m = TEMPLATE_DESCRIPTOR_P.matcher(firstLine);
        if (m.matches()) {
            templateDimensions = parseTemplateDescriptor(firstLine);
            content = parts[1];
        } else {
            templateDimensions = new LinkedHashMap<>();
        }
        String elemDimName = null;
        List<Type> elemOptions;
        for (String dimName : Arrays.asList("key", "value", "elem")) {
            if (templateDimensions.containsKey(dimName)) {
                elemDimName = dimName;
                elemOptions = new ArrayList<>();
                elemOptions.add(elem);
                for (Object type : templateDimensions.get(elemDimName)) {
                    if (type != elem) elemOptions.add((Type) type);
                }
                templateDimensions.put(elemDimName, elemOptions);
            }
        }
        if (elemDimName == null) {
            elemDimName = "elem";
            elemOptions = new ArrayList<>();
            elemOptions.add(elem);
            for (Type type : Arrays.asList(Type.PRIMITIVE_TYPES)) {
                if (type != elem) elemOptions.add(type);
            }
            templateDimensions.put(elemDimName, elemOptions);
        }

        template(content, templateDimensions, new TemplateResultsProcessor() {
            @Override
            public void process(
                    String result,
                    LinkedHashMap<String, List<?>> templateDimensions,
                    List<Object> options ) throws IOException {
                String outFileName = replaceDimensions(fileName, templateDimensions, options);
                writeFile(result, new File(outputDir, outFileName));
            }
        });
    }

    private static ArrayList<String> getQuoteInterleaves(String content) {
        ArrayList<String> interleaves = new ArrayList<>();
        int prevEnd = 0;
        Matcher m = AS_IS_T_P.matcher(content);
        while (m.find()) {
            interleaves.add(content.substring(prevEnd, m.start()));
            interleaves.add(m.group(1));
            prevEnd = m.end();
        }
        interleaves.add(content.substring(prevEnd));
        return interleaves;
    }


    private static String replaceElems( String content, Type elem, Type ne, Phase p ) {
        String out = content;
        out = elem.replaceZero(out, ne.elemZero, p);
        out = elem.replaceMaxV(out, ne.maxV);
        out = elem.replaceMinV(out, ne.minV);
        out = elem.replaceClNm(out, ne.clNm);
        out = elem.prim.replace(out, ne.prim, p);
        return out;
    }

    private static String replaceConditions(
            String content, Pattern condPattern, Object option) {
        Matcher matcher = condPattern.matcher(content);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, resolveCond(matcher.group(), option));
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    private static String resolveCond(String cond, Object option) {
        Matcher matcher = COND_PART.matcher(cond);
        while (matcher.find()) {
            for ( String branchOption : matcher.group(2).split("\\|") ) {
                if (option instanceof Type &&
                        Type.valueOf(branchOption.toUpperCase()) == option) {
                    return matcher.group(7);
                } else if (option instanceof String &&
                        branchOption.equalsIgnoreCase((String) option)) {
                    return matcher.group(7);
                }
            }
        }
        return "";
    }


    private static void writeFile(String content, File output_file)
            throws IOException {

        File parent = output_file.getParentFile();
        makeDirs(parent);

        // Write to a temporary file
        File temp = File.createTempFile("trove", "gentemp",
                new File(System.getProperty("java.io.tmpdir")));
        Writer writer = new BufferedWriter(new FileWriter(temp));
        writer.write(content);
        writer.close();


        // Now determine if it should be moved to the final location
        final boolean need_to_move;
        if (output_file.exists()) {
            boolean matches;
            try {
                MessageDigest digest = MessageDigest.getInstance("MD5");

                byte[] current_file = digest(output_file, digest);
                byte[] new_file = digest(temp, digest);

                matches = Arrays.equals(current_file, new_file);
            } catch (NoSuchAlgorithmException ex) {
                System.err.println(
                        "WARNING: Couldn't load digest algorithm to compare " +
                                "new and old template. Generation will be forced.");
                matches = false;
            }

            need_to_move = !matches;
        } else need_to_move = true;


        // Now move it if we need to move it
        if (need_to_move) {
            delete(output_file);
            copyFile(temp, output_file);
            System.out.println("  Wrote: " + simplifyPath(output_file));
        } else {
            System.out.println("  Skipped: " + simplifyPath(output_file));
            delete(temp);
        }
    }

    /**
     * Delete the given file, throws IllegalStateException if delete operation fails
     */
    private static void delete(File output_file) {
        if (!output_file.exists())
            return;

        if (!output_file.delete())
            throw new IllegalStateException("Could not delete " + output_file);
    }


    private static byte[] digest(File file, MessageDigest digest) throws IOException {
        digest.reset();

        byte[] buffer = new byte[1024];
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            int read = in.read(buffer);
            while (read >= 0) {
                digest.update(buffer, 0, read);

                read = in.read(buffer);
            }

            return digest.digest();
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                // ignore
            }
        }
    }

    private static String readFile(File input_file) throws IOException {
        if (!input_file.exists()) {
            throw new NullPointerException("Couldn't find: " + input_file);
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(input_file));
            StringBuilder out = new StringBuilder();

            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                out.append(line);
                out.append("\n");
            }
            return out.toString();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    // ignore
                }
            }
        }
    }


    private static String simplifyPath(File file) {
        String output_string = root_output_dir.toString();

        String file_string = file.toString();
        return file_string.substring(output_string.length() + 1);
    }


    private static void cleanDir(File directory) {
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                cleanDir(file);
                delete(file);
            }

            delete(file);
        }
    }


    private static void copyFile(File source, File dest) throws IOException {
        FileChannel srcChannel = new FileInputStream(source).getChannel();
        // Create channel on the destination
        FileChannel dstChannel = new FileOutputStream(dest).getChannel();
        // Copy file contents from source to destination
        dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
        // Close the channels
        srcChannel.close();
        dstChannel.close();
    }

    private static Pattern caseInsPattern(String regex) {
        return Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    }
}
